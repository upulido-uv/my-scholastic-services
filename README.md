# My Scholastic Service
###Application Overview

This service is a proxy service to various Scholastic internal services for use in the AEM
my profile page.  This service integrates with the below teams/applications:
 - IDAM (Identify Access Management )
 - Core Services (Digital downloads, subscriptions)
 - Bookfairs (eWallet)
 - Kong API Gateway (All APIs are managed through Kong)
 - Consul (Environment variable management)
 - AEM (Top Nav. My Profile is where all services are exposed)
 
The majority of these services are exposed publicly in a browser-to-server structure, with
 exceptions being explicitly labeled & documented.
 
#####Consul
Environment variables passed through consul that are security-sensitive such as AWS keys,
  Bearer Tokens, etc. should be encrypted using Spring Security.
  
`envconsul -consul-addr consul.service.np-us-east.consul:8500 -prefix ec/myscholastic/service/dev -consul-token bf2dd000-31fa-a731-dd27-da0912a3f6af -once mvn spring-boot:run -Djasypt.encryptor.password=pCi-2019-c440-601`
  
#####Frameworks
OpenFeign is used for most of the API templating to make this service contain less boilerplate
 code and be more declaritive in nature.
 
#####Architecture
See the entire architecture diagram for the various other PCI complient pieces that integrate
around this application. LucidChart: https://www.lucidchart.com/documents/edit/36bbc8d4-9f22-4b3e-bd8c-9eaeca4ce31a/1
 
**Note**: This application is **_NOT IN A PCI COMPLIANT VPC_**. Do not pass ANYTHING involving
 non-tokenized credit card number information through this service!
 
#####Documentation
This application is using Swagger to assist in API documentation. Please include this documentaion
 in any additional APIs that are added to this service.
 
#####Logging
 As this is a client-to-server service, **BE MINDFUL OF YOUR LOGGING!** Be sparse with how you
  info log in this service as it can very easily overload Sumologic & do NOT log anything user sensitive!
   
###Developer Runbook
To run:
```
mvn clean install
mvn spring-boot:run -Djasypt.encryptor.password=<encryptor password from Openshift>
```
Include information required to run this application.


 