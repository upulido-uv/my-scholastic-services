#!/usr/bin/env groovy
// Jenkinsfile - Book Fairs Service
// (c) Scholastic Corporation 2018

// Things that need to be configured:
//    - Maven named:       'maven-3.5.4'
//    - Artifactory named: 'Scholastic'
//    - SonarQube named:   'SonarQube Production'

node("nodejs") {
    def rtMaven = Artifactory.newMavenBuild()
    def server = Artifactory.server('Scholastic')

    stage('checkout') {
        checkout scm
    }

    stage('configure') {
        properties([buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '5')), pipelineTriggers([])])
        rtMaven.resolver server: server, releaseRepo: 'libs-releases', snapshotRepo: 'libs-snapshot'
        rtMaven.tool = 'maven-3.5.4'
        rtMaven.opts = '-Xms1024m -Xmx4096m'
    }

    stage('build and test') {
        rtMaven.run pom: 'pom.xml', goals: 'clean verify test'
    }

    stage('SonarQube') {
        def branch = ''

        if(env.BRANCH_NAME != "develop") {
            branch = "-Dsonar.branch.name=${env.BRANCH_NAME}"
        }

        try {
            withSonarQubeEnv('SonarQube Production') {
                 rtMaven.run pom: 'pom.xml', goals: 'sonar:sonar '+branch
            }
        } catch (error) {
            echo 'sonar failed, just skip it'
        }
    }

    stage('publish tests') {
        junit '**/target/surefire-reports/*.xml'
		jacoco changeBuildStatus: true, maximumLineCoverage: '80', exclusionPattern: '**/resource/**'
    }
}

