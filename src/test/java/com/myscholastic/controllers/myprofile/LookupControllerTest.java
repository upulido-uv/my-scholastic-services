package com.myscholastic.controllers.myprofile;

import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.feignclients.idam.IamSpsLookupClient;
import com.myscholastic.feignclients.idam.IamSpsOrgClient;
import com.myscholastic.models.myprofile.spslookup.KeyName;
import com.myscholastic.services.misc.LookupService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest({ LookupController.class })
public class LookupControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private IamClient idamClient;

	@MockBean
	private IamSpsLookupClient iamSpsLookupClient;

	@MockBean
	private IamSpsOrgClient iamSpsOrgClient;

	@MockBean
	private LookupService lookupService;

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testLookup() throws Exception {
		mockMvc.perform(get("/lookup").with(request -> {
			request.setParameter("types", "grade");
			return request;
		})).andExpect(status().isOk());
	}

	@Test
	public void testGetSchools() throws Exception {

		mockMvc.perform(get("/lookup/schools").with(request -> {
			request.setParameter("zipcode", "10001");
			return request;
		})).andExpect(status().isOk());
		
		mockMvc.perform(get("/lookup/schools").with(request -> {
			request.setParameter("zipcode", "10001");
			request.setParameter("forceClient", "F");
			return request;
		})).andExpect(status().isOk());
	}

	@Test
	public void testGetOrgs() throws Exception {

		mockMvc.perform(get("/lookup/orgs").with(request -> {
			request.setParameter("zipcode", "10001");
			return request;
		})).andExpect(status().isOk());
		
		mockMvc.perform(get("/lookup/orgs").with(request -> {
			request.setParameter("zipcode", "10001");
			request.setParameter("forceClient", "F");
			return request;
		})).andExpect(status().isOk());
	}

	@Test
	public void testGetStates() throws Exception {

		mockMvc.perform(get("/lookup/states")).andExpect(status().isOk());
	}

	@Test
	public void testGetCountries() throws Exception {

		mockMvc.perform(get("/lookup/countries")).andExpect(status().isOk());
	}

	@Test
	public void testGetSchoolTypes() throws Exception {

		mockMvc.perform(get("/lookup/schoolTypes").with(request -> {
			request.setParameter("locationType", "domestic");
			return request;
		})).andExpect(status().isOk());
	}

	@Test
	public void testSchoolInfo() throws Exception {

		mockMvc.perform(get("/lookup/schoolInfo").with(request -> {
			request.setParameter("spsId", "3971234");
			return request;
		})).andExpect(status().isOk());
	}

	@Test
	public void testGetRoleClassSize() throws Exception {

		mockMvc.perform(get("/lookup/roleClassSize")).andExpect(status().isOk());
	}

	@Test
	public void testGetGradesByOrdinals() throws Exception {

		mockMvc.perform(get("/lookup/gradesByOrdinals")).andExpect(status().isOk());
	}

	@Test
	public void testGetAppUserTypes() throws Exception {

		mockMvc.perform(get("/lookup/appUserTypes")).andExpect(status().isOk());
	}

	// Not sure this is where it belongs..
	@Test
	public void whenCallGetCitiesByState_thenReturnAListOfKeyName() throws Exception {
		String idgi = "IDGI";
		when(iamSpsLookupClient.getCitiesByState("NY")).thenReturn(
			Collections.singletonList(new KeyName(idgi, idgi)));

		mockMvc.perform(get("/lookup/cities").param("state", "NY"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", hasSize(1)))
			.andExpect(jsonPath("$.[0].key", equalTo(idgi)))
			.andExpect(jsonPath("$.[0].name", equalTo(idgi)));
	}
}
