package com.myscholastic.controllers.myprofile;

import com.myscholastic.configurations.AppConfig;
import com.myscholastic.feignclients.bookfair.BookfairsCPTKClient;
import com.myscholastic.feignclients.BriteEmailVerifyClient;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.myprofile.BriteVerify;
import com.myscholastic.models.myprofile.User;
import com.myscholastic.utils.EncryptionUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest({EmailValidationController.class})
public class EmailValidationControllerTest {

    public static final String ACCOUNT_TEST = "account-test";
    public static final String EMAIL = "test@scholastic.com";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BriteEmailVerifyClient briteEmailVerifyClient;

    @MockBean
    private AppConfig appConfig;

    @MockBean
    private IamClient idamClient;

    @MockBean
    private BookfairsCPTKClient bookfairsCPTKClient;

    private Cookie[] cookies = {
            new Cookie("SPS_SESSION", EncryptionUtil.encrypt("2718")),
            new Cookie("SPS_TSP", EncryptionUtil.encrypt(String.valueOf(System.currentTimeMillis() + 600000L))),
            new Cookie("SPS_UD", "testSpsUd")
    };

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testBriteEmailVerify() throws Exception {
        BriteVerify briteVerify = new BriteVerify();
        briteVerify.setAccount(ACCOUNT_TEST);

        ResponseEntity<BriteVerify> responseEntity = new ResponseEntity<BriteVerify>(briteVerify, HttpStatus.OK);

        when(briteEmailVerifyClient.verify(EMAIL)).thenReturn(responseEntity);

        mockMvc.perform(get("/verify/email/brite")
                .with(request -> {
                    request.setCookies(cookies);
                    request.setParameter("email", EMAIL);
                    return request;
                }))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.account", is("account-test")));

    }

    @Test
    public void testCheckValidEmail() throws Exception {
        User user = new User();
        User[] users = new User[1];
        users[0] = user;

        when(idamClient.checkValidEmail(EMAIL)).thenReturn(users);

        mockMvc.perform(get("/verify/email/valid")
                .with(request -> {
                    request.setCookies(cookies);
                    request.setParameter("email", EMAIL);
                    return request;
                }))
                .andExpect(status().isOk());

    }

    @Test
    public void testCheckBookFairEmail() throws Exception {
        String bookfairsXmlResponse = "<?xml version='1.0' encoding='UTF-8'?>\n" +
                "<chairperson>\n" +
                "    <status>0</status>\n" +
                "    <cpFirstname>Agatha </cpFirstname>\n" +
                "    <cpLastname>Swansong</cpLastname>\n" +
                "    <schoolInfo>\n" +
                "        <schoolUCN>992601007</schoolUCN>\n" +
                "        <bfRole>10045</bfRole>\n" +
                "    </schoolInfo>\n" +
                "    <schoolInfo>\n" +
                "        <schoolUCN>992601008</schoolUCN>\n" +
                "        <bfRole>10045</bfRole>\n" +
                "    </schoolInfo>\n" +
                "</chairperson>";

        when(bookfairsCPTKClient.checkBookFairEmail(EMAIL)).thenReturn(bookfairsXmlResponse);

        mockMvc.perform(get("/verify/email/bookFairs")
                .with(request -> {
                    request.setCookies(cookies);
                    request.setParameter("email", EMAIL);
                    return request;
                }))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cpFirstname", is("Agatha ")))
                .andExpect(jsonPath("$.status", is("0")));

    }
}
