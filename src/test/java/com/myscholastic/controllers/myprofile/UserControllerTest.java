package com.myscholastic.controllers.myprofile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.myscholastic.configurations.AppConfig;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.myprofile.*;
import com.myscholastic.models.myprofile.request.AdditionalOrg;
import com.myscholastic.models.myprofile.response.AdditionalOrgResponse;
import com.myscholastic.services.myprofile.UserService;
import com.myscholastic.utils.EncryptionUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import javax.servlet.http.Cookie;
import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest({UserController.class, UpdateUserController.class})
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AppConfig appConfig;

    @MockBean
    private IamClient idamClient;

    @MockBean
    private UserService userService;

    private Cookie[] cookies = {
        new Cookie("SPS_SESSION", EncryptionUtil.encrypt("2718")),
        new Cookie("SPS_TSP", EncryptionUtil.encrypt(String.valueOf(System.currentTimeMillis() + 600000L))),
        new Cookie("SPS_UD", "testSpsUd")
    };

    @Test
    public void testGetUser() throws Exception {
        User user = new User();
        user.setId("2718");
        when(idamClient.getUserWithSections("2718", "illusion")).thenReturn(user);
        mockMvc.perform(get("/my-profile/user")
            .with(request -> {
                request.setCookies(cookies);
                request.setParameter("sections", "illusion");
                return request;
            }))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is("2718")));
    }

    @Test
    public void testUpdateUser() throws Exception {
        String type = "USER";
        String body = "{\"firstName\":\"John\",\"lastName\":\"Doe\"}";
        BasicProfile basicProfile = new BasicProfile();
        basicProfile.setFirstName("John");
        basicProfile.setLastName("Doe");
        when(idamClient.updateUser("2718", body)).thenReturn(basicProfile);

        mockMvc.perform(post("/my-profile/user?updateType=" + type + "&index=2")
            .with(getRequestPostProcessor(body)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.basicProfile.lastName", is("Doe")));
    }

    @Test
    public void testUpdateAddress() throws Exception {
        String type = "ADDRESS";
        String body = "{ \"id\": 2, \"firstName\": \"Shiv\", \"lastName\": \"Rathore\", \"nickName\": \"Shiv-999\", \"address1\": \"123 Home Street\", \"address2\": \"\", \"city\": \"New York\", \"state\": \"NY\", \"country\": \"US\", \"postalCode\": \"10020\", \"EMAILS\": \"shivr11@gmail.com\", \"phone\": \"2012384096\" }";
        Address address0 = new Address();
        address0.setId(3141L);
        address0.setAddress1("123 Home Street");
        when(idamClient.updateAddress("2718", "2", body)).thenReturn(address0);

        mockMvc.perform(post("/my-profile/user?updateType=" + type + "&index=2")
            .with(getRequestPostProcessor(body)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.basicProfile.addresses.3141.address1", is("123 Home Street")))
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testUpdateSpecialities() throws Exception {
        String type = "SPECIALITIES";
        String body = "{\"school\":{\"specialities\":[\"elat\",\"rt\",\"mt\",\"st\"]}}";
        Educator educator = new Educator();
        School school = new School();
        school.setSpecialities(new String[]{"elat", "rt", "mt", "st"});
        educator.setSchool(school);
        when(idamClient.updateEducator("2718", body)).thenReturn(educator);

        mockMvc.perform(post("/my-profile/user?updateType=" + type + "&index=2")
            .with(getRequestPostProcessor(body)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.personas.educator.school.specialities[0]", is("elat")))
            .andExpect(jsonPath("$.personas.educator.school.specialities[1]", is("rt")))
            .andDo(MockMvcResultHandlers.print());

        type = "ROLES";
        mockMvc.perform(post("/my-profile/user?updateType=" + type + "&index=2")
            .with(getRequestPostProcessor(body)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.personas.educator.school.specialities[0]", is("elat")))
            .andExpect(jsonPath("$.personas.educator.school.specialities[1]", is("rt")))
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testUpdatePrimarySchool() throws Exception {
        String type = "PRIMARY_SCHOOL";
        String body = "{\"schoolId\": \"14903\",\"gradeRoles\": [\"FOGT\"],\"classes\": {\"FOURTH\": {\"classSize\": 15}},\"primaryGrade\": \"FOURTH\"}";
        School school = new School();
        school.setSchoolId("14903");
        school.setSpecialities(new String[]{"elat", "rt", "mt", "st"});
        when(idamClient.updateSchool("2718", body)).thenReturn(school);

        mockMvc.perform(post("/my-profile/user?updateType=" + type + "&index=2")
            .with(getRequestPostProcessor(body)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.personas.educator.school.schoolId", is("14903")));
    }

    @Test
    public void testUpdateContext() throws Exception {
        String type = "FIRST_YEAR_TEACHING";
        String body = "{\"schoolId\": \"14903\",\"gradeRoles\": [\"FOGT\"],\"classes\": {\"FOURTH\": {\"classSize\": 15}},\"primaryGrade\": \"FOURTH\"}";
        Contexts contexts = new Contexts();
        Clubs clubs = new Clubs();
        clubs.setActivationCode("YNVGD");
        contexts.setClubs(clubs);
        when(idamClient.updateContexts("2718", body)).thenReturn(contexts);

        mockMvc.perform(post("/my-profile/user?updateType=" + type + "&index=2")
            .with(getRequestPostProcessor(body)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.contexts.clubs.activationCode", is("YNVGD")));
    }

    @Test
    public void testUpdateUserAppType() throws Exception {
        String type = "MYSCHL_appUserName";
        String body = "{\"schoolId\": \"14903\",\"gradeRoles\": [\"FOGT\"],\"classes\": {\"FOURTH\": {\"classSize\": 15}},\"primaryGrade\": \"FOURTH\"}";
        User user = new User();
        when(idamClient.updateUserAppType("2718", body)).thenReturn(user);

        mockMvc.perform(post("/my-profile/user?updateType=" + type + "&index=2")
            .with(getRequestPostProcessor(body)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", is("2718")));
    }


    @Test
    public void testUpdateChild() throws Exception {
        String type = "UPDATE_CHILD";
        String body = "{\"schoolId\": \"14903\",\"gradeRoles\": [\"FOGT\"],\"classes\": {\"FOURTH\": {\"classSize\": 15}},\"primaryGrade\": \"FOURTH\"}";
        Child child = new Child();
        child.setId("3141");
        child.setLastName("Doe");
        when(idamClient.updateChild("2718", "2", body)).thenReturn(child);

        mockMvc.perform(post("/my-profile/user?updateType=" + type + "&index=2")
            .with(getRequestPostProcessor(body)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.personas.parent.children.3141.lastName", is("Doe")));

    }

    @Test
    public void testUpdateUserAddOrganization() throws Exception {
        String type = "ADDN_ORG";
        String body = "{\"id\": \"68752\",\"orgId\": \"23037836\",\"bookfairRole\":\"chairperson\",\"orgRoles\":[\"ect\",\"kt\",\"fgt\"]}";
        User user = new User();
        user.setId("68752");
        when(idamClient.updateUserChangeOrganization("2718", "2", body)).thenReturn(user);

        mockMvc.perform(post("/my-profile/user?updateType=" + type + "&index=2")
            .with(getRequestPostProcessor(body)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("id", is("2718")));
    }

    @Test
    public void testUpdateCredentials() throws Exception {
        String type = "CREDENTIALS";
        String body = "{\"firstName\":\"John\",\"lastName\":\"Doe\"}";
        Credentials credentials = new Credentials();
        credentials.setUserName("John Doe");
        when(idamClient.updateCredentials("2718", body)).thenReturn(credentials);

        mockMvc.perform(post("/my-profile/user?updateType=" + type + "&index=2")
            .with(getRequestPostProcessor(body)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("id", is("2718")))
            .andExpect(jsonPath("credentials.userName", is("John Doe")));
    }

    @Test
    public void testAdditionalOrgs() throws Exception {
        // Existing delete doesn't return anything..
        when(idamClient.deleteAdditionalOrg("2718", "1")).thenReturn(null);
        this.mockMvc.perform(delete("/my-profile/additional-orgs/1").cookie(cookies))
            .andExpect(status().isOk());
    }

    @Test
    public void deleteUserAddressTest() throws Exception {
    	String userId = "2718";
    	String addressId = "1"; 
        String uri = "/my-profile/addresses/"+ addressId;
    	ResponseEntity<Void> response = new ResponseEntity<>(HttpStatus.OK);
        when(idamClient.deleteAddress(userId, addressId)).thenReturn(response);
        
		mockMvc.perform(delete(uri)
		.with(request -> {
            request.setCookies(cookies);
            return request;
        }))
		.andExpect(status().isOk());		
    }
    
    @Test
    public void deleteChildrenTest() throws Exception {
    	String userId = "2718";
    	String childId = "1"; 
        String uri = "/my-profile/children/"+ childId;
    	ResponseEntity<Void> response = new ResponseEntity<>(HttpStatus.OK);
        when(idamClient.deleteChild(userId, childId)).thenReturn(response);

		mockMvc.perform(delete(uri)
		.with(request -> {
            request.setCookies(cookies);
            return request;
        }))
		.andExpect(status().isOk());
    }
   
    @Test
    public void deleteRolesTest() throws Exception {

    	String userId = "2718";
    	String body="";
        String uri = "/my-profile/roles/";
        User user = new User();
        user.setId(userId);
        Personas persona = new Personas();
        Educator educator = new Educator();
        School school = new School();
        
        educator.setSchool(school);
        persona.setEducator(educator);
        user.setPersonas(persona);

    	when(idamClient.getUser(userId)).thenReturn(user);    	
        when(idamClient.updateEducator(userId, body)).thenReturn(educator);

    	String role = "teacher"; 
		mockMvc.perform(delete(uri + role)
		.with(request -> {
            request.setCookies(cookies);
            return request;
        }))
		.andExpect(status().isOk());
		
    	role = "admin"; 
		mockMvc.perform(delete(uri + role)
		.with(request -> {
            request.setCookies(cookies);
            return request;
        }))
		.andExpect(status().isOk());
		
		role = "invalid_role"; 
		mockMvc.perform(delete(uri + role)
		.with(request -> {
            request.setCookies(cookies);
            return request;
        }))
		.andExpect(status().isBadRequest());
		
		
    }
    
    @Test
    public void addUserAddressTest() throws Exception {
    	String userId = "2718";
        String uri = "/my-profile/addresses";
    	Address addressRequest = new Address();
    	addressRequest.setFirstName("John");
    	addressRequest.setLastName("Doe");
    	addressRequest.setAddress1("ADD1");
    	addressRequest.setAddress2("ADD2");
    	addressRequest.setCity("CITY");
    	addressRequest.setState("STATE");
    	addressRequest.setCountry("USA");
    	addressRequest.setPostalCode("10001");
    	addressRequest.setEmail("EMAILS@EMAILS.com");
    	addressRequest.setPhone("9175551234");
    	Address addressResponse = new Address();
    	
    	String body = "{\"firstName\":\"John\",\"lastName\":\"Doe\"}";
    	
        when(idamClient.addAddress(userId, addressRequest)).thenReturn(addressResponse);
        
		mockMvc.perform(MockMvcRequestBuilders.post(uri)
		.with(request -> {
            request.setCookies(cookies);
            request.setContent(body.getBytes());
            request.setContentType("application/json");
            return request;
        }))
		.andExpect(status().isOk());
    }
    
    @Test
    public void addChildTest() throws Exception {
    	String userId = "2718";
        String uri = "/my-profile/children";
    	ChildRequest childRequest = new ChildRequest();
    	childRequest.setFirstName("John");
    	childRequest.setLastName("Doe");
    	Child child = new Child();
    	
    	String body = "{\"firstName\":\"John\",\"lastName\":\"Doe\"}";
    	
        when(idamClient.addChild(userId, childRequest)).thenReturn(child);
        
		mockMvc.perform(MockMvcRequestBuilders.post(uri)
		.with(request -> {
            request.setCookies(cookies);
            request.setContent(body.getBytes());
            request.setContentType("application/json");
            return request;
        }))
		.andExpect(status().isOk());
    }

    @Test
    public void createAdditionalOrg() throws Exception {
        String body =
            "{\n" +
            "  \"bookfairRole\": \"chairperson\",\n" +
            "  \"orgId\": \"1\",\n" +
            "  \"orgRoles\": [\"role1\", \"role2\"]\n" +
            "}";
        AdditionalOrg additionalOrg = AdditionalOrg.builder()
            .orgId("1").bookfairRole("chairperson").orgRoles(Arrays.asList("role1", "role2")).build();
        AdditionalOrgResponse additionalOrgResponse = AdditionalOrgResponse.builder().id("123").build();

        when(idamClient.createAdditionalOrg("2718", additionalOrg)).thenReturn(additionalOrgResponse);
        mockMvc.perform(post("/my-profile/additional-orgs")
                    .cookie(cookies)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(body.getBytes()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is("123")));
    }

    @Test
    public void getProfileLinks() throws Exception {
        String expected = "GetProfileLinksJSON({\"key\", \"val\"})";

        when(userService.getProfileLinks("2718")).thenReturn("{\"key\", \"val\"}");
        mockMvc.perform(get("/my-profile/profile-links").cookie(cookies))
            .andExpect(status().isOk())
            .andExpect(content().contentType("application/javascript;charset=UTF-8"))
            .andExpect(content().string(expected));
    }

    private RequestPostProcessor getRequestPostProcessor(String body) {
        return request -> {
            request.setCookies(cookies);
            request.setParameter("sections", "illusion");
            request.setContent(body.getBytes());
            request.setContentType("application/json");
            return request;
        };
    }
    
}

