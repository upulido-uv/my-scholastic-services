package com.myscholastic.controllers.misc;

import com.myscholastic.configurations.AppConfig;
import com.myscholastic.feignclients.bookfair.BookfairsCPTKClient;
import com.myscholastic.feignclients.bookfair.BookfairsCachedClient;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.myprofile.BookfairOrganizationInfo;
import com.myscholastic.services.misc.OfeBookfairService;
import com.myscholastic.utils.EncryptionUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest({OfeBookfairController.class})
public class OfeBookfairControllerTest {

    public static final String SCHOOL_UCN = "99999999";
    public static final String PUT_JSON_BODY = "{\"default\": true}";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OfeBookfairService ofeBookfairService;

    @MockBean
    private AppConfig appConfig;

    @MockBean
    private IamClient idamClient;

    @MockBean
    private BookfairsCachedClient bookfairsCachedClient;

    @MockBean
    private BookfairsCPTKClient bookfairsCPTKClient;

    private Cookie[] cookies = {
            new Cookie("SPS_SESSION", EncryptionUtil.encrypt("2718")),
            new Cookie("SPS_TSP", EncryptionUtil.encrypt(String.valueOf(System.currentTimeMillis() + 600000L))),
            new Cookie("SPS_UD", "testSpsUd")
    };

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testGetBookfairOrgAssociations() throws Exception {

        BookfairOrganizationInfo bookfairOrganizationInfo = new BookfairOrganizationInfo();
        List<BookfairOrganizationInfo> bfOrgInfo = new ArrayList<>();
        bfOrgInfo.add(bookfairOrganizationInfo);

        when(ofeBookfairService.getBookfairOrgNames("2718")).thenReturn(bfOrgInfo);

        mockMvc.perform(get("/my-bookfairs/bookfair-school-associations")
                .with(request -> {
                    request.setCookies(cookies);
                    return request;
                }))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));

    }


    @Test
    public void testGetBookfairOrgAssociationsWithSchoolUCN() throws Exception {

        BookfairOrganizationInfo bookfairOrganizationInfo = new BookfairOrganizationInfo();
        List<BookfairOrganizationInfo> bfOrgInfo = new ArrayList<>();
        bfOrgInfo.add(bookfairOrganizationInfo);


        ResponseEntity<Void> idamResponse = new ResponseEntity<>(HttpStatus.OK);

        when(bookfairsCachedClient.getBookfairOrganizationInfo(SCHOOL_UCN)).thenReturn(bfOrgInfo);

        when(idamClient.setUserBFOrgUcn("2718", SCHOOL_UCN, PUT_JSON_BODY)).thenReturn(idamResponse);

        mockMvc.perform(put("/my-bookfairs/bookfair-school-associations/" + SCHOOL_UCN)
                .with(request -> {
                    request.setCookies(cookies);
                    return request;
                }))
                .andExpect(status().isOk());

    }

    @Test
    public void testDeleteBookfairOrgAssociations() throws Exception {

        BookfairOrganizationInfo bookfairOrganizationInfo = new BookfairOrganizationInfo();
        List<BookfairOrganizationInfo> bfOrgInfo = new ArrayList<>();
        bfOrgInfo.add(bookfairOrganizationInfo);


        ResponseEntity<Void> idamResponse = new ResponseEntity<>(HttpStatus.OK);

        when(idamClient.deleteUserBFOrgUcn("2718", SCHOOL_UCN)).thenReturn(idamResponse);

        mockMvc.perform(delete("/my-bookfairs/bookfair-school-associations/" + SCHOOL_UCN)
                .with(request -> {
                    request.setCookies(cookies);
                    return request;
                }))
                .andExpect(status().isOk());

    }
}
