package com.myscholastic.controllers.ewallet;

import com.myscholastic.feignclients.bookfair.BookfairsClient;
import com.myscholastic.feignclients.idam.EWalletClient;
import com.myscholastic.models.ewallet.*;
import com.myscholastic.models.ewallet.response.DeleteVoucherResponse;
import com.myscholastic.services.ewallet.EWalletService;
import com.myscholastic.utils.EncryptionUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import javax.servlet.http.Cookie;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.iterableWithSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest({EWalletController.class})
public class EWalletControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EWalletClient ewalletClient;

    @MockBean
    private BookfairsClient bookfairsClient;

    @MockBean
    private EWalletService eWalletService;

    private Cookie[] cookies = {
        new Cookie("SPS_SESSION", EncryptionUtil.encrypt("2718")),
        new Cookie("SPS_TSP", EncryptionUtil.encrypt(String.valueOf(System.currentTimeMillis() + 600000L))),
        new Cookie("SPS_UD", "testSpsUd"),
        new Cookie("MYSCHL_EWfairID", "1111%7C2222")
    };

    private RequestPostProcessor getRequestPostProcessor(String body) {
        return request -> {
            request.setCookies(cookies);
            request.setParameter("sections", "illusion");
            request.setContent(body.getBytes());
            request.setContentType("application/json");
            return request;
        };
    }

    @Test
    public void getFairsInfoByFairId() throws Exception {
        List<Fair> fairs = new ArrayList<>();
        Fair fair0 = Fair.builder().id(123L).build();
        fairs.add(fair0);

        when(bookfairsClient.getFairsInfo(new FairIdList(new HashSet<>(Collections.singletonList(11L)))))
            .thenReturn(fairs);
        mockMvc.perform(get("/ewallet/fair-info").param("fairId", "11").cookie(cookies))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[0].id", is(123)));

        when(bookfairsClient.getFairsInfo(new FairIdList(new HashSet<>(Collections.singletonList(2222L)))))
            .thenReturn(fairs);
        mockMvc.perform(get("/ewallet/fair-info").cookie(cookies))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[0].id", is(123)));
    }

    @Test
    public void getEWalletBySpsId() throws Exception {
        EWalletId eWalletId = new EWalletId();
        eWalletId.setId(2019L);
        eWalletId.setCreatedOn("2019-03-27");
        List<EWalletId> eWalletIds = new ArrayList<>();
        eWalletIds.add(eWalletId);

        when(ewalletClient.getEWalletBySpsId("2718")).thenReturn(eWalletIds);
        mockMvc.perform(get("/ewallet/")
            .with(request -> {
                request.setCookies(cookies);
                return request;
            }))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[0].id", is(2019)))
            .andExpect(jsonPath("$.[0].createdOn", is("2019-03-27")));
    }

    @Test
    public void getFairsAndVouchersByWalletId() throws Exception {
        FairVouchersJson fairVouchersJson = new FairVouchersJson();
        List<FairVouchers> fairVouchersList = new ArrayList<>();
        FairVouchers fairVouchers0 = FairVouchers.builder().id(2019L).build();
        fairVouchersList.add(fairVouchers0);
        fairVouchersJson.setFairVouchersList(fairVouchersList);

        when(eWalletService.getFairVouchersJson("3333", 2222L)).thenReturn(fairVouchersJson);
        mockMvc.perform(get("/ewallet/fair-vouchers/3333").cookie(cookies))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.fairVouchersList", iterableWithSize(1)))
            .andExpect(jsonPath("$.fairVouchersList.[0].id", is(2019)));
    }

    @Test
    public void postVoucherTransactionbyVoucherId() throws Exception {
        when(eWalletService.hasVoucher("2718", "999")).thenReturn(true);
        mockMvc.perform(post("/ewallet/voucher/voucher-transaction?voucherId=999&amount=100&transactionType=FUNDS_ADDED")
            .with(request -> {
                request.setCookies(cookies);
                return request;
            }))
            .andExpect(status().isOk());
    }

    @Test
    public void deleteVoucherByVoucherId() throws Exception {
        DeleteVoucherResponse resp = new DeleteVoucherResponse();
        resp.setMessage("hello");
        when(eWalletService.hasVoucher("2718", "999")).thenReturn(true);
        when(ewalletClient.deleteVoucher("999")).thenReturn(resp);
        mockMvc.perform(delete("/ewallet/voucher/999")
            .with(request -> {
                request.setCookies(cookies);
                return request;
            }))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.message", is("hello")));
    }

    @Test
    public void whenVoucherIdDoesntBelongToUser_thenReturn403() throws Exception {
        when(eWalletService.hasVoucher("2718", "999")).thenReturn(false);
        mockMvc.perform(delete("/ewallet/voucher/999")
            .with(request -> {
                request.setCookies(cookies);
                return request;
            }))
            .andExpect(status().isForbidden());
        mockMvc.perform(post("/ewallet/voucher/voucher-transaction?voucherId=999&amount=100&transactionType=FUNDS_ADDED")
            .with(request -> {
                request.setCookies(cookies);
                return request;
            }))
            .andExpect(status().isForbidden());
    }
}