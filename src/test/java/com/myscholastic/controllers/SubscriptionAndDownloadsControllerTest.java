package com.myscholastic.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myscholastic.controllers.subscriptionsanddownloads.SubscriptionsAndDownloadsController;
import com.myscholastic.feignclients.digitaldownloads.DigitalDownloadsClient;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.feignclients.subscription.QtcSubscriptionHistoryClient;
import com.myscholastic.feignclients.subscription.QtcUpdateBillingClient;
import com.myscholastic.feignclients.subscription.SubscriptionHistoryClient;
import com.myscholastic.models.BaseResponse;
import com.myscholastic.models.digitaldownloads.DigitalDownloads;
import com.myscholastic.models.digitaldownloads.DigitalItem;
import com.myscholastic.models.myprofile.Identifiers;
import com.myscholastic.models.myprofile.User;
import com.myscholastic.models.subscription.*;
import com.myscholastic.models.subscription.orderdetail.OrderDetails;
import com.myscholastic.models.subscription.orderdetail.OrderTotals;
import com.myscholastic.services.myprofile.UserService;
import com.myscholastic.utils.EncryptionUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.Cookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = {SubscriptionsAndDownloadsController.class})
public class SubscriptionAndDownloadsControllerTest {

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SubscriptionHistoryClient subscriptionHistoryClient;

    @MockBean
    private DigitalDownloadsClient digitalDownloadsClient;

    @MockBean
    private QtcSubscriptionHistoryClient qtcSubscriptionHistoryClient;

    @MockBean
    private QtcUpdateBillingClient qtcUpdateBillingClient;

    @MockBean
    private IamClient iamClient;

    private String spsCookies;
    private final Cookie[] cookies = {
        new Cookie("SPS_SESSION", EncryptionUtil.encrypt("2718")),
        new Cookie("SPS_TSP", EncryptionUtil.encrypt(String.valueOf(System.currentTimeMillis() + 600000L))),
        new Cookie("SPS_UD", "testSpsUd")
    };

    @Before
    public void setUp() {
        spsCookies = getCookies(cookies);
    }

    @Test
    public void testGetDigitalDownloads() throws Exception {
        DigitalItem digitalItem = new DigitalItem();
        digitalItem.setStoreId("6001122");
        DigitalItem[] digitalItems = {digitalItem};
        DigitalDownloads digitalDownloads = new DigitalDownloads();
        digitalDownloads.setDigitalItems(digitalItems);

        when(digitalDownloadsClient.getDigitalDownloadsList(spsCookies)).thenReturn(digitalDownloads);
        mockMvc.perform(get("/subscriptions-and-downloads/digital-downloads")
                .cookie(cookies))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.digitalItems[0].storeId", is("6001122")));
    }

    private String getCookies(Cookie[] cookies) {
        return Arrays.stream(cookies)
            .map(cookie -> cookie.getName() + "=" + cookie.getValue())
            .collect(Collectors.joining(";"));
    }

    @Test
    public void getSubscriptionHistory() throws Exception {
        Subscriptions subscriptions = new Subscriptions();
        Subscription[] subscriptionArr = new Subscription[1];
        subscriptions.setSubscriptions(subscriptionArr);

        when(subscriptionHistoryClient.getSubscriptions(spsCookies)).thenReturn(subscriptions);
        mockMvc.perform(get("/subscriptions-and-downloads/subscription-history")
            .cookie(cookies))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.subscriptions", hasSize(1)));
    }

    @Test
    public void getSubscriptionDetails() throws Exception {
        OrderDetails orderDetails = new OrderDetails();
        OrderTotals orderTotals = new OrderTotals();
        orderTotals.setOrderId("2718");
        orderDetails.setOrderTotals(orderTotals);
        String spsCookies = Arrays.stream(cookies).map(cookie -> cookie.getName() + "=" + cookie.getValue()).collect(Collectors.joining(";"));

        when(subscriptionHistoryClient.getSubscriptionByOrderId("11", "22", spsCookies)).thenReturn(orderDetails);
        mockMvc.perform(get("/subscriptions-and-downloads/subscription-order-detail/11/22")
            .with(request -> {
                request.setCookies(cookies);
                return request;
            }))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.orderTotals.orderId", is("2718")));
    }

    @Test
    public void getQtcSubscriptions() throws Exception {
        QtcSubscriptions qtcSubscriptions = new QtcSubscriptions();
        QtcSubscription qtcSub = new QtcSubscription();
        qtcSub.setSubscriptionId("1234");
        List<QtcSubscription> list = Arrays.asList(qtcSub);
        qtcSubscriptions.setSubscriptions(list);

        User user = new User();
        Identifiers identifiers = new Identifiers();
        identifiers.setUcn("1352076683");
        user.setIdentifiers(identifiers);

        when(userService.getCustomerUCN("2718")).thenReturn("1352076683");
        when(qtcSubscriptionHistoryClient.getQtcSubscriptions("1352076683")).thenReturn(qtcSubscriptions);

        mockMvc.perform(get("/api/qtc-subscription-history")
                .with(request -> {
                    request.setCookies(cookies);
                    return request;
                }))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.subscriptions[0].subscriptionId", is("1234")));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void updateQtcBilling() throws Exception {
        QtcUpdateBillingRequest requestBody = QtcUpdateBillingRequest.builder()
                .subscriptionId("300000069937022")
                .subscriptionNum("CPQ-25774-47797298")
                .orderId("80027248")
                .cybersourceToken("0187758474671111")
                .cardNumberMasked("486560XXXXXX4611")
                .cardExpirationMonth(2)
                .cardExpirationYear(2020)
                .cardType("001")
                .billToFirstName("VELIMIR")
                .billToLastName("BABATOONDEY")
                .csTrxNumber("238648236482648264")
                .build();

        BaseResponse response = new BaseResponse();
        response.setMessage("SUCCESS");
        response.setStatus("200");

        when(qtcUpdateBillingClient.updateBilling(requestBody)).thenReturn(response);

        mockMvc.perform(post("/api/qtc-update-billing")
                .with(request -> {
                    request.setCookies(cookies);
                    return request;
                })
                .content(asJsonString(requestBody))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("200")))
                .andExpect(jsonPath("$.message", is("SUCCESS")));
    }

    @Test
    public void updateQtcBillingFail() throws Exception {
        QtcUpdateBillingRequest requestBody = QtcUpdateBillingRequest.builder()
                .subscriptionId("300000069937022")
                .subscriptionNum("CPQ-25774-47797298")
                .orderId("80027248")
                .cybersourceToken("0187758474671111")
                .cardNumberMasked("486560XXXXXX4611")
                .cardExpirationMonth(2)
                .cardExpirationYear(2020)
                .cardType("001")
                .billToFirstName("VELIMIR")
                .billToLastName("BABATOONDEY")
                .csTrxNumber("238648236482648264")
                .build();

        BaseResponse response = new BaseResponse();
        response.setMessage("ERROR - ucn, cybersourceToken and orderId are mandatory");
        response.setStatus("400");

        when(qtcUpdateBillingClient.updateBilling(requestBody)).thenReturn(response);

        mockMvc.perform(post("/api/qtc-update-billing")
                .cookie(cookies)
                .content(asJsonString(requestBody))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.status", is("400")))
                .andExpect(jsonPath("$.message", is("ERROR - ucn, cybersourceToken and orderId are mandatory")));
    }

    @Test
    public void updateQtcBillingUcnNotFound() throws Exception {
        QtcUpdateBillingRequest requestBody = QtcUpdateBillingRequest.builder()
                .subscriptionId("300000069937022")
                .subscriptionNum("CPQ-25774-47797298")
                .orderId("80027248")
                .cybersourceToken("0187758474671111")
                .cardNumberMasked("486560XXXXXX4611")
                .cardExpirationMonth(2)
                .cardExpirationYear(2020)
                .cardType("001")
                .billToFirstName("VELIMIR")
                .billToLastName("BABATOONDEY")
                .csTrxNumber("238648236482648264")
                .build();

        User user = new User();
        Identifiers identifiers = new Identifiers();
        identifiers.setUcn(null);
        user.setIdentifiers(identifiers);
        doThrow(new ResponseStatusException(HttpStatus.BAD_REQUEST, "CustomerUCN not found for user")).when(userService).getCustomerUCN("2718");

        mockMvc.perform(post("/api/qtc-update-billing")
                .cookie(cookies)
                .content(asJsonString(requestBody))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(status().reason("CustomerUCN not found for user"));
    }

}