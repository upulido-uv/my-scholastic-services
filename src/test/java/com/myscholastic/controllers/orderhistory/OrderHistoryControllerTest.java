package com.myscholastic.controllers.orderhistory;

import com.myscholastic.models.orderhistory.GenericOrder;
import com.myscholastic.models.orderhistory.OrderHistory;
import com.myscholastic.models.orderhistory.orderdetails.OrderDetail;
import com.myscholastic.services.orderhistory.OrderHistoryService;
import com.myscholastic.utils.EncryptionUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest({OrderHistoryController.class})
public class OrderHistoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderHistoryService orderHistoryService;

    private Cookie[] cookies = {
        new Cookie("SPS_SESSION_SECURE", EncryptionUtil.encryptSecure("2718")),
        new Cookie("SPS_TSP_SECURE", EncryptionUtil.encryptSecure(String.valueOf(System.currentTimeMillis() + 600000L))),
    };

    @Test
    public void getOrderHistory() throws Exception {
        GenericOrder order = new GenericOrder();
        OrderHistory[] histories = {new OrderHistory(), new OrderHistory()};
        order.setGenericOrder(histories);
        when(orderHistoryService.getOrderHistory("2718", "1950-01-01", "2019-04-19"))
            .thenReturn(order);

        mockMvc.perform(get("/order-history/history?fromDate=1950-01-01&toDate=2019-04-19").cookie(cookies))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.genericOrder", hasSize(2)));
    }

    @Test
    public void getOrderDetail() throws Exception {
        OrderDetail orderDetail = new OrderDetail();
        when(orderHistoryService.getOrderDetails("2718", "ooi", null, null, null, null))
            .thenReturn(orderDetail);
        mockMvc.perform(get("/order-history/detail")
                .param("storeId", "ooi")
                .cookie(cookies))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.isPcool", is(false)))
            .andExpect(jsonPath("$.isBookFair", is(false)));
    }
}