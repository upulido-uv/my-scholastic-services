package com.myscholastic.controllers.wishlist;

import com.myscholastic.models.wishlist.DeleteWishListResponse;
import com.myscholastic.services.wishlist.WishListService;
import com.myscholastic.utils.EncryptionUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import javax.servlet.http.Cookie;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest({WishlistController.class})
public class WishlistControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WishListService wishListService;

    private Cookie[] cookies = {
        new Cookie("SPS_SESSION", EncryptionUtil.encrypt("2718")),
        new Cookie("SPS_TSP", EncryptionUtil.encrypt(String.valueOf(System.currentTimeMillis() + 600000L))),
        new Cookie("SPS_UD", "testSpsUd")
    };

    private RequestPostProcessor getRequestPostProcessor() {
        return request -> {
            request.setCookies(cookies);
            return request;
        };
    }

    @Test
    public void getWishList() throws Exception {
        List<String> productIds = Arrays.asList("MDM_P_1", "MDM_P_2");
        when(wishListService.getProductIds("2718")).thenReturn(productIds);
        mockMvc.perform(get("/api/wishlist").with(getRequestPostProcessor()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.[0]", is("MDM_P_1")))
            .andExpect(jsonPath("$.[1]", is("MDM_P_2")));
    }

    @Test
    public void deleteWishList() throws Exception {
        DeleteWishListResponse deleteWishListResponse = new DeleteWishListResponse(2);
        when(wishListService.deleteAllWishList("2718")).thenReturn(deleteWishListResponse);
        mockMvc.perform(delete("/api/wishlist").with(getRequestPostProcessor()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.count", is(2)));
    }

    @Test
    public void deleteWishListByProductId() throws Exception {
        DeleteWishListResponse deleteWishListResponse = new DeleteWishListResponse(1);
        when(wishListService.deleteItemByProductId("2718", "MDM_P_1")).thenReturn(deleteWishListResponse);
        mockMvc.perform(delete("/api/wishlist/product/MDM_P_1").with(getRequestPostProcessor()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.count", is(1)));
    }
}