package com.myscholastic.controllers.register;

import com.myscholastic.exception.SpsOrgHasInvalidLocationTypeException;
import com.myscholastic.exception.SpsOrgMissingFieldException;
import com.myscholastic.models.myprofile.request.SpsOrganization;
import com.myscholastic.models.myprofile.response.SpsOrganizationResponse;
import com.myscholastic.services.spsorg.SpsOrgService;
import com.myscholastic.utils.EncryptionUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@WebMvcTest(RegistrationController.class)
public class RegistrationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SpsOrgService spsOrgService;

    private Cookie[] cookies = {
        new Cookie("SPS_SESSION_SECURE", EncryptionUtil.encryptSecure("2718")),
        new Cookie("SPS_TSP_SECURE", EncryptionUtil.encryptSecure(String.valueOf(System.currentTimeMillis() + 600000L))),
    };

    private String requestBody;

    @Before
    public void setUp() throws Exception {
         requestBody = "{" +
            "  \"address1\": \"\"," +
            "  \"address2\": \"\"," +
            "  \"city\": \"\"," +
            "  \"country\": \"\"," +
            "  \"locationType\": \"\"," +
            "  \"name\": \"\"," +
            "  \"phone\": \"\"," +
            "  \"reportingSchoolType\": \"\"," +
            "  \"state\": \"\"," +
            "  \"zipcode\": \"\"" +
            "}";
    }

    @Test
    public void submitSchool() throws Exception {
        SpsOrganization spsOrganization = new SpsOrganization();
        SpsOrganizationResponse spsOrganizationResponse = new SpsOrganizationResponse();
        spsOrganizationResponse.setName("spsOrg");
        when(spsOrgService.submitSchool(spsOrganization)).thenReturn(spsOrganizationResponse);
        mockMvc.perform(post("/register/submit-school")
                .cookie(cookies).content(requestBody).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name", is("spsOrg")));
    }

    @Test
    public void whenSubmitSchoolSpsOrgMissingFieldException_thenReturn400() throws Exception{
        when(spsOrgService.submitSchool(any())).thenThrow(SpsOrgMissingFieldException.class);
        mockMvc.perform(post("/register/submit-school")
            .cookie(cookies).content(requestBody).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void whenSubmitSchoolThrowsSpsOrgHasInvalidLocationType_thenReturn400() throws Exception {
        when(spsOrgService.submitSchool(any())).thenThrow(SpsOrgHasInvalidLocationTypeException.class);
        mockMvc.perform(post("/register/submit-school")
            .cookie(cookies).content(requestBody).contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());
    }
}