package com.myscholastic.controllers.cybersource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myscholastic.configurations.ReCaptchaClientConfig;
import com.myscholastic.feignclients.recaptcha.ReCaptchaClient;
import com.myscholastic.models.cybersource.Signature;
import com.myscholastic.models.cybersource.CyberSourceDto;
import com.myscholastic.models.cybersource.Wallet;
import com.myscholastic.models.recaptcha.ReCaptchaRequestPayload;
import com.myscholastic.models.recaptcha.ReCaptchaResponsePayload;
import com.myscholastic.models.subscription.UpdateBillingResponse;
import com.myscholastic.services.cybersource.CreditCardService;
import com.myscholastic.services.cybersource.HtmlService;
import com.myscholastic.services.cybersource.SignatureService;
import com.myscholastic.utils.EncryptionUtil;
import com.myscholastic.utils.SignatureUtil;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import javax.servlet.http.Cookie;
import java.util.Base64;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(CyberSourceController.class)
public class CyberSourceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SignatureService signatureService;

    @MockBean
    private CreditCardService creditCardService;

    @MockBean
    private HtmlService htmlService;

    @MockBean
    private ReCaptchaClient reCaptchaClient;

    private static final String EMPTY = "";
    private String[] keys = {"key0", "key1", "key2"};
    private CyberSourceDto cyberSourceDto;
    private Wallet wallet;

    private Cookie[] cookies = {
        new Cookie("SPS_SESSION", EncryptionUtil.encrypt("2718")),
        new Cookie("SPS_TSP", EncryptionUtil.encrypt(String.valueOf(System.currentTimeMillis() + 600000L))),
        new Cookie("SPS_UD", "testSpsUd")
    };

    private RequestPostProcessor getRequestWithSecretKeys() {
        return request -> {
            request.setCookies(cookies);
            request.addHeader("secret-key", "key0");
            request.addHeader("ewallet-secret-key", "key1");
            request.addHeader("update-billing-secret-key", "key2");
            request.setContentType("application/json");
            return request;
        };
    }

    @Before
    public void setUp() {
        cyberSourceDto = CyberSourceDto.builder()
            .decision(EMPTY)
            .cardType(EMPTY)
            .editPaymentToken(EMPTY)
            .paymentToken(EMPTY)
            .cardNumber(EMPTY)
            .expiryDate(EMPTY)
            .userId(EMPTY)
            .subId(EMPTY)
            .otcParam(EMPTY)
            .defaultCard(EMPTY)
            .saveToProfile(EMPTY)
            .cardAction(EMPTY)
            .ext(EMPTY)
            .ewallet(EMPTY)
            .firstName(EMPTY)
            .lastName(EMPTY)
            .address1(EMPTY)
            .address2(EMPTY)
            .city(EMPTY)
            .postalCode(EMPTY)
            .state(EMPTY)
            .country(EMPTY)
            .phone(EMPTY)
            .build();
        wallet = mock(Wallet.class);
        when(signatureService.isSignatureValid(anyMap(), Mockito.any(SignatureUtil.SecretKeyType.class))).thenReturn(true);
    }

    @Ignore
    public void whenActionIsEditAndTypeIsUpdateBilling() throws Exception {
        Signature signature = Signature.builder()
            .signature("signature")
            .build();
        when(signatureService.sign("EDIT", "UPDATE_BILLING", "asdfasdfasdfasdf"))
            .thenReturn(signature);

        String json = new ObjectMapper().writeValueAsString(signature);
        String encoded = Base64.getEncoder().encodeToString(json.getBytes());

        mockMvc.perform(get("/cybersource/sign?cardAction=EDIT&token=asdfasdfasdfasdf&type=UPDATE_BILLING")
            .with(getRequestWithSecretKeys()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.key", is(encoded)));
    }

    @Test
    public void givenAddCardRequestReturnSignature() throws Exception {
        Signature signature = Signature.builder()
            .signature("signature")
            .build();
        when(signatureService.signAddCardAuthorization())
            .thenReturn(signature);

        String json = new ObjectMapper().writeValueAsString(signature);
        String encoded = Base64.getEncoder().encodeToString(json.getBytes());
        
        mockMvc.perform(get("/api/sign/add")
            .with(getRequestWithSecretKeys()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.key", is(encoded)));
    }

    @Test
    public void givenEditCardRequestReturnSignature() throws Exception {
        Signature signature = Signature.builder()
            .signature("signature_with_token")
            .build();
        when(signatureService.signEditCardAuthorization("mytoken"))
            .thenReturn(signature);

        String json = new ObjectMapper().writeValueAsString(signature);
        String encoded = Base64.getEncoder().encodeToString(json.getBytes());
        
        mockMvc.perform(get("/api/sign/edit?token=mytoken")
            .with(getRequestWithSecretKeys()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.key", is(encoded)));
    }

    @Test
    public void givenEwalletAuthRequestReturnSignature() throws Exception {
        Signature signature = Signature.builder()
            .signature("signature_with_token")
            .build();
        when(signatureService.signEwalletTransaction(null))
            .thenReturn(signature);

        String json = new ObjectMapper().writeValueAsString(signature);
        String encoded = Base64.getEncoder().encodeToString(json.getBytes());
        
        mockMvc.perform(get("/api/sign/ewallet")
            .with(getRequestWithSecretKeys()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.key", is(encoded)));
    }

    @Test
    public void givenUpdateSubscriptionAssociationReturnSignature() throws Exception {
        Signature signature = Signature.builder()
            .signature("signature_with_token")
            .build();
        when(signatureService.signUpdateSubscriptionAuthorization("mytoken"))
            .thenReturn(signature);

        String json = new ObjectMapper().writeValueAsString(signature);
        String encoded = Base64.getEncoder().encodeToString(json.getBytes());
        
        mockMvc.perform(get("/api/sign/subscription?token=mytoken")
            .with(getRequestWithSecretKeys()))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.key", is(encoded)));
    }

    @Test
    public void whenSecretIsMissing_thenReturn400() throws Exception {
        mockMvc.perform(get("/cybersource/sign?cardAction=EDIT&token=asdfasdfasdfasdf&type=UPDATE_BILLING")
            .with(mockHttpServletRequest -> {
                mockHttpServletRequest.setCookies(cookies);
                return mockHttpServletRequest;
            }))
            .andExpect(status().isBadRequest());

        mockMvc.perform(get("/api/sign/add")
            .with(mockHttpServletRequest -> {
                mockHttpServletRequest.setCookies(cookies);
                return mockHttpServletRequest;
            }))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void whenActionIsInvalid_thenReturn400() throws Exception {
        when(signatureService.sign("INVALID", "UPDATE_BILLING", "asdfasdfasdfasdf"))
            .thenReturn(null);
        mockMvc.perform(get("/cybersource/sign?cardAction=INVALID&token=asdfasdfasdfasdf&type=UPDATE_BILLING")
            .with(getRequestWithSecretKeys()))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void whenReCaptchaIsInvalid_thenReturn400() throws Exception {
        ReCaptchaClientConfig reCaptchaClientConfig = new ReCaptchaClientConfig();
        ReCaptchaRequestPayload reCaptchaRequestPayload = ReCaptchaRequestPayload.builder().secret(reCaptchaClientConfig.getSecretKey()).response("TOKEN_FROM_CUSTOMER_BROWSER").build();

        ReCaptchaResponsePayload recaptchaResponsePayload = ReCaptchaResponsePayload.builder().success(false).build();

        when(reCaptchaClient.validateRecaptcha(reCaptchaRequestPayload))
                .thenReturn(recaptchaResponsePayload);

        mockMvc.perform(get("/api/cybersource/secure/sign?cardAction=EDIT&type=UPDATE_BILLING&captchaToken=TOKEN_FROM_CUSTOMER_BROWSER&ccToken=ccToken")
                .with(getRequestWithSecretKeys()))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenReCaptchaIsValid_thenReturn200() throws Exception {
        ReCaptchaClientConfig reCaptchaClientConfig = new ReCaptchaClientConfig();
        ReCaptchaRequestPayload reCaptchaRequestPayload = ReCaptchaRequestPayload.builder().secret(reCaptchaClientConfig.getSecretKey()).response("TOKEN_FROM_CUSTOMER_BROWSER").build();

        ReCaptchaResponsePayload recaptchaResponsePayload = ReCaptchaResponsePayload.builder().success(true).errorCodes(null).build();

        when(reCaptchaClient.validateRecaptcha(reCaptchaRequestPayload))
                .thenReturn(recaptchaResponsePayload);

        Signature signature = Signature.builder()
                .signature("signature")
                .build();
        when(signatureService.sign("EDIT", "UPDATE_BILLING", "ccToken"))
                .thenReturn(signature);

        mockMvc.perform(get("/api/cybersource/secure/sign?cardAction=EDIT&type=UPDATE_BILLING&captchaToken=TOKEN_FROM_CUSTOMER_BROWSER&ccToken=ccToken")
                .with(getRequestWithSecretKeys()))
                .andExpect(status().isOk());
    }

    /*--- PostBack Tokenized Data --*/

    private RequestPostProcessor getRequest() {
        return request -> {
            request.setCookies(cookies);
            return request;
        };
    }

    @Test
    public void whenActionIsNullOrOtcParamIsInvalid_thenReturn400() throws Exception {
        String expectedDefault = "<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\">" +
            "<head><script type=\"text/javascript\">function ready(){window.parent.processRequest(400);}</script>" +
            "</head><body onload=\"ready();\"></body>" +
            "</html>";

        String expectedEWallet = "<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\">" +
            "<head><script type=\"text/javascript\">function ready(){window.parent.processEwalletRequest(400);}</script>" +
            "</head><body onload=\"ready();\"></body>" +
            "</html>";

        String expectedSubscription = "<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\">" +
            "<head><script type=\"text/javascript\">function ready(){window.parent.processSubscriptionRequest(400);}</script>" +
            "</head><body onload=\"ready();\"></body>" +
            "</html>";

        setCyberSourceDto(EMPTY, EMPTY, EMPTY, EMPTY);
        when(htmlService.getHtml(400, HtmlService.Type.DEFAULT)).thenReturn(expectedDefault);
        mockMvc.perform(post("/bin/myschl/cspostback")
            .with(getRequest())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
            .content(getFormUrlEncoded(EMPTY, EMPTY, EMPTY, EMPTY)))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML_VALUE))
        .andExpect(content().string(expectedDefault));

        setCyberSourceDto(EMPTY, EMPTY, EMPTY, "EWALLET");
        when(htmlService.getHtml(400, HtmlService.Type.EWALLET)).thenReturn(expectedEWallet);
        mockMvc.perform(post("/bin/myschl/cspostback")
            .with(getRequest())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
            .content(getFormUrlEncoded(EMPTY, EMPTY, EMPTY, "EWALLET")))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML_VALUE))
        .andExpect(content().string(expectedEWallet));

        setCyberSourceDto(EMPTY, EMPTY, "OTC", EMPTY);
        when(htmlService.getHtml(400, HtmlService.Type.SUBSCRIPTION)).thenReturn(expectedSubscription);
        mockMvc.perform(post("/bin/myschl/cspostback")
            .with(getRequest())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
            .content(getFormUrlEncoded(EMPTY, EMPTY, "OTC", EMPTY)))
        .andExpect(status().isBadRequest())
        .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_HTML_VALUE))
        .andExpect(content().string(expectedSubscription));
    }

    @Test
    public void addCreditCardForEWallet() throws Exception {

        String expected1 = "<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\">" +
            "<head><script type=\"text/javascript\">function ready(){window.parent.processEwalletRequest(200);}</script>" +
            "</head><body onload=\"ready();\"></body>" +
            "</html>";
        String expected2 = "<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\">" +
            "<head><script type=\"text/javascript\">function ready(){window.parent.processEwalletRequest(500);}</script>" +
            "</head><body onload=\"ready();\"></body>" +
            "</html>";

        setCyberSourceDto("Add", "Accept", EMPTY, "EWALLET");
        when(htmlService.getHtml(200, cyberSourceDto, HtmlService.Type.EWALLET)).thenReturn(expected1);
        when(htmlService.getHtml(500, HtmlService.Type.EWALLET)).thenReturn(expected2);

        when(creditCardService.addCreditCardForEWallet(cyberSourceDto, "Add")).thenReturn(wallet);
        mockMvc.perform(post("/bin/myschl/cspostback")
            .with(getRequest())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
            .content(getFormUrlEncoded("Add", "Accept", EMPTY, "EWALLET")))
        .andExpect(status().isOk())
        .andExpect(content().string(expected1));

        when(creditCardService.addCreditCardForEWallet(cyberSourceDto, "Edit")).thenReturn(null);
        mockMvc.perform(post("/bin/myschl/cspostback")
            .with(getRequest())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
            .content(getFormUrlEncoded("Edit", "Accept", EMPTY, "EWALLET")))
        .andExpect(status().isInternalServerError())
        .andExpect(content().string(expected2));
    }

    @Test
    public void addOrEditCreditCard() throws Exception{
        String expected1 = "<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\">" +
            "<head><script type=\"text/javascript\">function ready(){window.parent.processRequest(200);}</script>" +
            "</head><body onload=\"ready();\"></body>" +
            "</html>";
        String expected2 = "<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\">" +
            "<head><script type=\"text/javascript\">function ready(){window.parent.processRequest(500);}</script>" +
            "</head><body onload=\"ready();\"></body>" +
            "</html>";

        setCyberSourceDto("Edit", "Accept", EMPTY, EMPTY);
        when(htmlService.getHtml(200, HtmlService.Type.DEFAULT)).thenReturn(expected1);
        when(htmlService.getHtml(500, HtmlService.Type.DEFAULT)).thenReturn(expected2);

        when(creditCardService.addOrEditCreditCard(cyberSourceDto)).thenReturn(wallet);
        mockMvc.perform(post("/bin/myschl/cspostback")
            .with(getRequest())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
            .content(getFormUrlEncoded("Edit", "Accept", EMPTY, EMPTY)))
        .andExpect(status().isOk())
        .andExpect(content().string(expected1));

        when(creditCardService.addOrEditCreditCard(cyberSourceDto)).thenReturn(null);
        mockMvc.perform(post("/bin/myschl/cspostback")
            .with(getRequest())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
            .content(getFormUrlEncoded("Edit", "Accept", EMPTY, EMPTY)))
        .andExpect(status().isInternalServerError())
        .andExpect(content().string(expected2));
    }

    @Test
    public void updateSubscription() throws Exception {
        String expected1 = "<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\">" +
            "<head><script type=\"text/javascript\">function ready(){window.parent.processSubscriptionRequest(200);}</script>" +
            "</head><body onload=\"ready();\"></body>" +
            "</html>";
        String expected2 = "<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\">" +
            "<head><script type=\"text/javascript\">function ready(){window.parent.processSubscriptionRequest(500);}</script>" +
            "</head><body onload=\"ready();\"></body>" +
            "</html>";

        setCyberSourceDto(EMPTY, "Accept", "OTC", EMPTY);
        cyberSourceDto.setUserId("2998");
        when(htmlService.getHtml(200, HtmlService.Type.SUBSCRIPTION)).thenReturn(expected1);
        when(htmlService.getHtml(500, HtmlService.Type.SUBSCRIPTION)).thenReturn(expected2);

        UpdateBillingResponse successResponse = new UpdateBillingResponse();
        successResponse.setIsSuccess("true");
        successResponse.setStatusCode("200");
        successResponse.setMessage("...");

        when(creditCardService.updateBilling(cyberSourceDto)).thenReturn(successResponse);
        String otcFormData = getFormUrlEncoded(EMPTY, "Accept", "OTC", EMPTY) + "&req_merchant_defined_data12=2998";
        mockMvc.perform(post("/bin/myschl/cspostback")
            .with(getRequest())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
            .content(otcFormData))
        .andExpect(status().isOk())
        .andExpect(content().string(expected1));


        UpdateBillingResponse failResponse = new UpdateBillingResponse();
        failResponse.setIsSuccess("false");
        failResponse.setStatusCode("400");
        failResponse.setMessage("failed");
        when(creditCardService.updateBilling(cyberSourceDto)).thenReturn(failResponse);
        mockMvc.perform(post("/bin/myschl/cspostback")
            .with(getRequest())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
            .content(otcFormData))
        .andExpect(status().isInternalServerError())
        .andExpect(content().string(expected2));
    }

    @Test
    public void givenSignaturesDontMatch_returnUnauthorized() throws Exception {
        when(signatureService.isSignatureValid(anyMap(), Mockito.any(SignatureUtil.SecretKeyType.class))).thenReturn(false);
        when(creditCardService.addOrEditCreditCard(cyberSourceDto)).thenReturn(wallet);
        mockMvc.perform(post("/bin/myschl/cspostback")
            .with(getRequest())
            .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
            .content(getFormUrlEncoded("Edit", "Accept", EMPTY, EMPTY)))
            .andExpect(status().isUnauthorized());
    }

    private void setCyberSourceDto(String cardAction, String decision, String otc, String ewallet) {
        cyberSourceDto.setCardAction(cardAction);
        cyberSourceDto.setDecision(decision);
        cyberSourceDto.setOtcParam(otc);
        cyberSourceDto.setEwallet(ewallet);
    }

    private String getFormUrlEncoded(String cardAction, String decision, String otc, String ewallet) {

        return String.format(
            "req_merchant_defined_data25=%s&decision=%s&req_merchant_defined_data20=%s&req_merchant_defined_data28=%s",
            cardAction,
            decision,
            otc,
            ewallet
        );
    }

}