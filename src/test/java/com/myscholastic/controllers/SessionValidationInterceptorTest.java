package com.myscholastic.controllers;

import com.myscholastic.controllers.ewallet.EWalletController;
import com.myscholastic.controllers.subscriptionsanddownloads.SubscriptionsAndDownloadsController;
import com.myscholastic.feignclients.bookfair.BookfairsClient;
import com.myscholastic.feignclients.egift.SVPGClient;
import com.myscholastic.feignclients.idam.IamSecretsManagerClient;
import com.myscholastic.feignclients.subscription.QtcSubscriptionHistoryClient;
import com.myscholastic.feignclients.subscription.QtcUpdateBillingClient;
import com.myscholastic.feignclients.subscription.SubscriptionHistoryClient;
import com.myscholastic.feignclients.digitaldownloads.DigitalDownloadsClient;
import com.myscholastic.feignclients.idam.EWalletClient;
import com.myscholastic.services.ewallet.EWalletService;
import com.myscholastic.services.myprofile.UserService;
import com.myscholastic.services.rotatingsecret.RotatingSecretService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest({
    HealthController.class,
    EWalletController.class,
    SubscriptionsAndDownloadsController.class
})
@MockBeans({
    @MockBean(EWalletClient.class),
    @MockBean(BookfairsClient.class),
    @MockBean(EWalletService.class),
    @MockBean(SubscriptionHistoryClient.class),
    @MockBean(DigitalDownloadsClient.class),
    @MockBean(QtcSubscriptionHistoryClient.class),
    @MockBean(QtcUpdateBillingClient.class),
    @MockBean(UserService.class),
    @MockBean(RotatingSecretService.class),
    @MockBean(SVPGClient.class)

})
@TestPropertySource(properties = "com.myscholastic.configurations.WebMvcConfig=enabled")
public class SessionValidationInterceptorTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void givenHealthIsExcluded_whenCallHealthCheck_thenReturn200() throws Exception {
        mockMvc.perform(get("/health")).andExpect(status().isOk());
    }

    @Test
    public void givenUserSessionIsNotValid_whenCallEndPoint_thenReturn401() throws Exception {
        mockMvc.perform(get("/ewallet")).andExpect(status().isUnauthorized());
    }

    @Test
    public void givenUserSessionIsNotValid_whenCallSubscriptionEndPoint_thenReturn401() throws Exception {
        mockMvc.perform(get("/subscriptions-and-downloads/subscription-history"))
            .andExpect(status().isUnauthorized());
    }
}
