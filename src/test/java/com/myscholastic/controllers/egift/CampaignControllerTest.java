package com.myscholastic.controllers.egift;

import com.myscholastic.feignclients.egift.EGiftClient;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.egift.*;
import com.myscholastic.models.myprofile.Identifiers;
import com.myscholastic.models.myprofile.User;
import com.myscholastic.utils.EncryptionUtil;
import lombok.val;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CampaignController.class)
public class CampaignControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EGiftClient eGiftClient;

    @MockBean
    private IamClient iamClient;

    private static final String SPSID = "1225";
    private static final String UCN = "UCN";
    private static final String ID_1 = "id1";
    private static final String ID_2 = "id2";
    private static final String ID_3 = "id3";
    private static final String ID_CONTRIBUTION = "id4";
    private static final String URL = "https://scholastic.com/egift";
    private Cookie[] cookies = {
            new Cookie("SPS_SESSION", EncryptionUtil.encrypt(SPSID)),
            new Cookie("SPS_TSP", EncryptionUtil.encrypt(String.valueOf(System.currentTimeMillis() + 600000L))),
            new Cookie("SPS_UD", "1225|why|concatenate|like|this")
    };

    private Campaign campaign1 = Campaign.builder()
            .campaignId(ID_1)
            .contributorsLink(new URL(URL))
            .email("myemail@email.com")
            .active(true)
            .expired(false)
            .disabled(false)
            .build();
    private Campaign campaign2 = Campaign.builder().campaignId(ID_2).build();
    private CampaignRequest post = CampaignRequest.builder()
            .disabled(false)
            .description("quick brown fox jump over")
            .end("2030-10-16T20:06:14.018Z")
            .imagePath("scholastic.com/img.gif")
            .name(new Name("John", "Doe"))
            .target(BigDecimal.valueOf(0L))
            .spsid(SPSID)
            .ucn(UCN)
            .title("A Day of Cup")
            .build();
    private CampaignRequest patch = CampaignRequest.builder()
            .disabled(false)
            .description("quick brown fox jump over")
            .end("2030-10-16T20:06:14.018Z")
            .imagePath("scholastic.com/img.gif")
            .name(new Name("John", "Doe"))
            .target(BigDecimal.valueOf(0L))
            .title("A Day of Cup")
            .build();
    private Campaign res = Campaign.builder()
            .active(true)
            .campaignId(ID_3)
            .build();
    private Contribution contribution1 = Contribution.builder()
            .contributionId(ID_CONTRIBUTION)
            .build();
    private ContributionRequest contributionRequest = ContributionRequest.builder()
            .amount(BigDecimal.valueOf(19.99))
            .anonymize(true)
            .message("my message")
            .name(new Name("John", "Doe"))
            .email("jdoe@scholastic.com")
            .build();
    private ContributionResponse contributionResponse = ContributionResponse.builder()
            .campaign(campaign1)
            .contribution(contribution1)
            .count(1)
            .totalAmount(BigDecimal.valueOf(19.99))
            .build();
    private ContributionsResponse contributionsResponse = ContributionsResponse.builder()
            .campaign(campaign1)
            .contributions(Collections.singletonList(contribution1))
            .count(1)
            .totalAmount(BigDecimal.valueOf(99L))
            .build();

    private static final String REQUEST_PAYLOAD = "{\n" +
            "  \"disabled\": false,\n" +
            "  \"description\": \"quick brown fox jump over\",\n" +
            "  \"end\": \"2030-10-16T20:06:14.018Z\",\n" +
            "  \"imagePath\": \"scholastic.com/img.gif\",\n" +
            "  \"name\": {\n" +
            "    \"first\": \"John\",\n" +
            "    \"last\": \"Doe\"\n" +
            "  },\n" +
            "  \"target\": 0,\n" +
            "  \"title\": \"A Day of Cup\"\n" +
            "}";

    public CampaignControllerTest() throws MalformedURLException {}

    @Test
    public void getCampaignsByUser() throws Exception {
        given(eGiftClient.getCampaigns(SPSID)).willReturn(Arrays.asList(campaign1, campaign2));
        mockMvc.perform(get("/api/campaigns").cookie(cookies))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].campaignId", is(ID_1)))
                .andExpect(jsonPath("$.[0].contributorsLink", is(URL)))
                .andExpect(jsonPath("$.[0].email", is("myemail@email.com")))
                .andExpect(jsonPath("$.[0].active", is(true)))
                .andExpect(jsonPath("$.[0].expired", is(false)))
                .andExpect(jsonPath("$.[0].disabled", is(false)))
                .andExpect(jsonPath("$.[1].campaignId", is(ID_2)));
    }

    @Test
    public void getCampaignById() throws Exception {
        given(eGiftClient.getCampaignById(ID_1)).willReturn(campaign1);
        mockMvc.perform(get("/api/campaigns/{campaignId}", ID_1)
                .cookie(cookies))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.campaignId", is(ID_1)));
    }

    @Test
    public void createCampaign() throws Exception {
        given(eGiftClient.postCampaign(post)).willReturn(res);
        val user = new User();
        val identifiers = new Identifiers();
        identifiers.setUcn(UCN);
        user.setIdentifiers(identifiers);

        given(iamClient.getUserWithSections(SPSID, "identifiers")).willReturn(user);
        mockMvc.perform(post("/api/campaigns")
                .contentType(MediaType.APPLICATION_JSON)
                .content(REQUEST_PAYLOAD)
                .cookie(cookies))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.campaignId", is(ID_3)));
    }

    @Test
    public void updateCampaign() throws Exception {
        mockMvc.perform(patch("/api/campaigns/{campaignId}", SPSID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(REQUEST_PAYLOAD)
                .cookie(cookies))
                .andExpect(status().isNoContent());
        verify(eGiftClient).putCampaign(SPSID, patch);
    }

    @Test
    public void getContributionsByCampaign() throws Exception {
        given(eGiftClient.getContributionsByCampaign(ID_1)).willReturn(contributionsResponse);
        mockMvc.perform(get("/api/campaigns/{campaignId}/contributions", ID_1)
                .cookie(cookies))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.count", is(1)))
                .andExpect(jsonPath("$.totalAmount", is(99)))
                .andExpect(jsonPath("$.contributions", hasSize(1)))
                .andExpect(jsonPath("$.contributions.[0].contributionId", is(ID_CONTRIBUTION)));
    }

    @Test
    public void getContribution() throws Exception {
        given(eGiftClient.getContribution(ID_1, ID_CONTRIBUTION)).willReturn(contribution1);
        mockMvc.perform(get("/api/campaigns/{campaignId}/contributions/{contributionId}", ID_1, ID_CONTRIBUTION)
                .cookie(cookies))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.contributionId", is(ID_CONTRIBUTION)));
    }

    @Test
    public void createContribution() throws Exception {
        String payload = "{\n" +
                "  \"amount\": 19.99,\n" +
                "  \"anonymize\": true,\n" +
                "  \"email\": \"jdoe@scholastic.com\",\n" +
                "  \"message\": \"my message\",\n" +
                "  \"name\": {\n" +
                "    \"first\": \"John\",\n" +
                "    \"last\": \"Doe\"\n" +
                "  }\n" +
                "}";
        given(eGiftClient.createContribution(ID_1, contributionRequest)).willReturn(contributionResponse);
        mockMvc.perform(post("/api/campaigns/{campaignId}/contributions", ID_1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload)
                .cookie(cookies))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.count", is(1)))
                .andExpect(jsonPath("$.contribution.contributionId", is(ID_CONTRIBUTION)))
                .andExpect(jsonPath("$.totalAmount", is(19.99)));
    }

    @Test
    public void updateContribution() throws Exception {
        // Need Java 13 text block asap
        String payload = "{\n" +
                "  \"amount\": 19.99,\n" +
                "  \"anonymize\": true,\n" +
                "  \"email\": \"new@scholastic.com\",\n" +
                "  \"message\": \"modified\",\n" +
                "  \"name\": {\n" +
                "    \"first\": \"John2\",\n" +
                "    \"last\": \"Doe2\"\n" +
                "  }\n" +
                "}";
        given(eGiftClient.updateContribution(ID_1, ID_CONTRIBUTION, contributionRequest)).willReturn(contribution1);
        mockMvc.perform(patch("/api/campaigns/{campaignId}/contributions/{contributionId}", ID_1, ID_CONTRIBUTION)
                .contentType(MediaType.APPLICATION_JSON)
                .content(payload)
                .cookie(cookies))
                .andExpect(status().isNoContent());
    }
}