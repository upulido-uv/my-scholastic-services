package com.myscholastic.controllers.egift;

import com.myscholastic.feignclients.egift.EGiftClient;
import com.myscholastic.feignclients.egift.SVPGClient;
import com.myscholastic.models.egift.*;
import com.myscholastic.utils.EncryptionUtil;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SvpgController.class)
public class SvpgControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SVPGClient svpgClient;

    private static final String SPSID = "1225";
    private Cookie[] cookies = {
            new Cookie("SPS_SESSION", EncryptionUtil.encrypt(SPSID)),
            new Cookie("SPS_TSP", EncryptionUtil.encrypt(String.valueOf(System.currentTimeMillis() + 600000L))),
            new Cookie("SPS_UD", "1225|why|concatenate|like|this")
    };


    public SvpgControllerTest() throws MalformedURLException {}

    @Test
    public void getTransactionHistory() throws Exception {
        TransactionsRequest param = TransactionsRequest.builder()
                .tenderType("campaign")
                .build();

        Transactions history = Transactions.builder()
                .amount(BigDecimal.TEN)
                .description("live laugh love")
                .tenderType("campaign")
                .timestamp(null)
                .build();

        given(svpgClient.getTransactionsHistory(SPSID, param)).willReturn(Arrays.asList(history));
        mockMvc.perform(get("/api/svpg/transactions-history?tenderType=campaign").cookie(cookies))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].amount", CoreMatchers.is(10)))
                .andExpect(jsonPath("$.[0].tenderType", is("campaign")));

    }

    @Test
    public void getBalances() throws Exception {
        BalancesResponse res = BalancesResponse.builder()
                .availableAmount(BigDecimal.ONE)
                .balance(BigDecimal.TEN)
                .tenderType("campaign")
                .build();



        given(svpgClient.getBalances(SPSID)).willReturn(Arrays.asList(res));
        mockMvc.perform(get("/api/svpg/balances").cookie(cookies))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].balance", CoreMatchers.is(10)))
                .andExpect(jsonPath("$.[0].availableAmount", CoreMatchers.is(1)))
                .andExpect(jsonPath("$.[0].tenderType", is("campaign")));

    }
}
