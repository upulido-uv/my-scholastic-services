package com.myscholastic.utils;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;


public class CreditCardUtilTest {

    @Test
    public void getCreditCardBrand() {
        assertThat(CreditCardUtil.mapCardType(null), is(""));
        assertThat(CreditCardUtil.mapCardType(""), is(""));
        assertThat(CreditCardUtil.mapCardType("001"), is("Visa"));
        assertThat(CreditCardUtil.mapCardType("002"), is("Mastercard"));
        assertThat(CreditCardUtil.mapCardType("003"), is("American Express"));
        assertThat(CreditCardUtil.mapCardType("004"), is("Discover"));
    }
}