package com.myscholastic.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class DateFormatUtilTest {

    @Test
    public void formatDate() {
        assertNull(DateFormatUtil.formatDate(null));
        assertNull(DateFormatUtil.formatDate("www"));

        String expected = "January 01, 2019";
        String actual = DateFormatUtil.formatDate("01/01/2019");
        assertEquals(expected, actual);
    }
}