package com.myscholastic.services.mail;

import com.myscholastic.configurations.AppConfig;
import com.myscholastic.configurations.EmailServiceConfig;
import com.myscholastic.feignclients.bookfair.BookfairsClient;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.ewallet.Fair;
import com.myscholastic.models.ewallet.FairIdList;
import com.myscholastic.models.ewallet.request.CreateVoucherRequest;
import com.myscholastic.models.ewallet.request.CreateVoucherRequestAttributes;
import com.myscholastic.models.myprofile.BasicProfile;
import com.myscholastic.models.myprofile.User;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static junit.framework.TestCase.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MailServiceTest {
	
	
	private MailService mailService;
    private AppConfig appConfig;
    private IamClient idamClient;
    private BookfairsClient bookfairsClient;

    @Before
    public void setUp()  {
    	appConfig = mock(AppConfig.class);
    	idamClient = mock(IamClient.class);
    	bookfairsClient = mock(BookfairsClient.class);
    	mailService = new MailService(appConfig,idamClient,bookfairsClient);    
    }


	@Ignore("Fix the tests, mocking SOAPUtil")
	@Test
    public void sendVoucherCreateConfirmationMailTest() throws TransformerException, ParserConfigurationException, IOException {
    	//given
    	SoapResponse response = null;
    	CreateVoucherRequest eWalletRequest = new CreateVoucherRequest();
    	String fairId = "3937263";
    	CreateVoucherRequestAttributes attributes = new CreateVoucherRequestAttributes();
    	attributes.setFairId(fairId);
    	attributes.setStudentFirstName("Tony");
    	eWalletRequest.setAttributes(attributes);
    	eWalletRequest.setAmount(new BigDecimal("25.00"));
      	Long fairIdL = Long.parseLong(eWalletRequest.getAttributes().getFairId());
    	String spsId = "1";
    	User user = new User();
    	BasicProfile basicProfile = new BasicProfile();
    	basicProfile.setEmail("email@email.com");
    	user.setBasicProfile(basicProfile);
    	EmailServiceConfig emailServiceConfig =  new EmailServiceConfig();
    	emailServiceConfig.setSessionToken("1234");
    	emailServiceConfig.setEventApiEwalletEvent("ABCD");
    	emailServiceConfig.setEventApiUser("username");
    	emailServiceConfig.setEventApiPwd("password");
    	emailServiceConfig.setEventApiUrl("http://url.com");
    	appConfig.setEmailServiceConfig(emailServiceConfig);
    	List<Fair> fairs = new ArrayList<Fair>();
     	Fair fair = Fair.builder().startDate("2019-04-12").endDate("2019-04-12").name("ASPIRE APEX ACADEMY").build();
    	fairs.add(fair);

    	//when
    	when(idamClient.getUser(spsId)).thenReturn(user);
    	when(bookfairsClient.getFairsInfo(new FairIdList(new HashSet<Long>(Arrays.asList(fairIdL))))).thenReturn(fairs);
    	when(appConfig.getEmailServiceConfig()).thenReturn(emailServiceConfig);
//    	response = mailService.sendVoucherCreateConfirmationMail(eWalletRequest, spsId);
    	
    	//then
//    	assertNotNull(response);
		assertNull(response);
    }
}
