package com.myscholastic.services.myprofile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.myscholastic.feignclients.idam.EWalletClient;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.ewallet.EWalletId;
import com.myscholastic.models.myprofile.Educator;
import com.myscholastic.models.myprofile.Identifiers;
import com.myscholastic.models.myprofile.Personas;
import com.myscholastic.models.myprofile.User;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    private UserService userService;
    private IamClient iamClient;
    private EWalletClient eWalletClient;

    @Before
    public void setUp() throws Exception {
        iamClient = mock(IamClient.class);
        eWalletClient = mock(EWalletClient.class);
        userService = new UserService(iamClient, eWalletClient);
    }

    @Test
    public void givenUserIsNotParentAndHasEWallet_thenReturnAllProfileLinks()
        throws JsonProcessingException, JSONException {
        String expected = "{" +
                "\"#my-profile\":\"/my-scholastic/profile/my-profile\"," +
                "\"#my-wishlist\":\"/my-scholastic/profile/my-wishlist\"," +
                "\"#order-history\":\"/my-scholastic/profile/order-history\"," +
                "\"#subscription-downloads\":\"/my-scholastic/profile/subscriptions-downloads\"," +
                "\"#ewallet\":\"/my-scholastic/profile/ewallet\"," +
                "\"#manage-campaigns\":\"/my-scholastic/profile/manage-campaigns\"" +
                "}";
        User user = new User();
        Personas personas = new Personas();
        personas.setEducator(new Educator());
        user.setPersonas(personas);
        when(iamClient.getUser("2718")).thenReturn(user);
        when(eWalletClient.getEWalletBySpsId("2718"))
            .thenReturn(Collections.singletonList(new EWalletId()));

        String actual = userService.getProfileLinks("2718");
        JSONAssert.assertEquals(actual, expected, false);
    }

    @Test
    public void givenUserIsParentAndHasNoEWallet_thenReturnPartialProfileLinks()
        throws JsonProcessingException, JSONException {
        String expected = "{" +
            "\"#my-profile\":\"/my-scholastic/profile/my-profile\"," +
            "\"#order-history\":\"/my-scholastic/profile/order-history\"}";
        User user = new User();
        when(iamClient.getUser("2718")).thenReturn(user);
        when(eWalletClient.getEWalletBySpsId("2718"))
            .thenReturn(Collections.emptyList());

        String actual = userService.getProfileLinks("2718");
        JSONAssert.assertEquals(actual, expected, false);
    }

    @Test
    public void getCustomerUCN() {
        User user = new User();
        Identifiers identifiers = new Identifiers();
        identifiers.setUcn("1352076683");
        user.setIdentifiers(identifiers);
        when(iamClient.getUser("2718")).thenReturn(user);

        String response = userService.getCustomerUCN("2718");
        assertEquals(response, "1352076683");
    }

    @Test(expected = ResponseStatusException.class)
    public void getCustomerUCN_Fail() {
        User user = new User();
        Identifiers identifiers = new Identifiers();
        identifiers.setUcn(null);
        user.setIdentifiers(identifiers);
        when(iamClient.getUser("2718")).thenReturn(user);

        userService.getCustomerUCN("2718");
    }
}
