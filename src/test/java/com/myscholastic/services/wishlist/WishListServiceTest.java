package com.myscholastic.services.wishlist;

import com.myscholastic.feignclients.idam.WishListClient;
import com.myscholastic.models.wishlist.DeleteWishListResponse;
import com.myscholastic.models.wishlist.Item;
import com.myscholastic.models.wishlist.WishList;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WishListServiceTest {

    private WishListService wishListService;
    private WishListClient wishListClient;

    @Before
    public void setUp() throws Exception {
        wishListClient = mock(WishListClient.class);
        wishListService = new WishListService(wishListClient);
    }

    @Test
    public void getProductIds() {
        WishList wishList = new WishList();

        Item item0 = new Item();
        Item item1 = new Item();
        Item item3 = new Item();
        Item item4 = new Item();
        item0.setProductId("MDM_P_1");
        item0.setUpdatedOn("2030-04-17T07:31:24Z");
        item1.setProductId("MDM_P_2");
        item1.setUpdatedOn("2030-04-17T06:31:24Z");
        item3.setProductId("MDM_P_3");
        item3.setUpdatedOn("ajskdlfajsdkf");
        item4.setProductId("MDM_P_4");
        item4.setUpdatedOn(null);
        List<Item> items = Arrays.asList(item0, item1, item3, item4);

        wishList.setItems(items);
        when(wishListClient.getAllWishListItems("999")).thenReturn(wishList);
        List<String> productIds = wishListService.getProductIds("999");
        assertNotNull(productIds);
        assertThat(productIds.get(0), equalTo("MDM_P_1"));
        assertThat(productIds.get(1), equalTo("MDM_P_2"));
        assertThat(productIds.get(2), equalTo("MDM_P_3"));
        assertThat(productIds.get(3), equalTo("MDM_P_4"));
    }


    @Test
    public void deleteItemByProductId() {
        DeleteWishListResponse deleteWishListResponse = new DeleteWishListResponse(1);
        when(wishListClient.deleteWishListItemByProductId("111", "MDM_P_1")).thenReturn(deleteWishListResponse);
        DeleteWishListResponse actual = wishListService.deleteItemByProductId("111", "MDM_P_1");
        assertNotNull(actual);
        assertThat(actual.getCount(), is(1));
    }

    @Test
    public void deleteAllWishList() {
        DeleteWishListResponse deleteWishListResponse = new DeleteWishListResponse(2);
        when(wishListClient.deleteAllWishListItems("111")).thenReturn(deleteWishListResponse);
        DeleteWishListResponse actual = wishListService.deleteAllWishList("111");
        assertNotNull(actual);
        assertThat(actual.getCount(), is(2));
    }
}