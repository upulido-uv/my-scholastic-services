package com.myscholastic.services.spsorg;

import com.myscholastic.exception.SpsOrgHasInvalidLocationTypeException;
import com.myscholastic.exception.SpsOrgMissingFieldException;
import com.myscholastic.feignclients.idam.IamSpsOrgClient;
import com.myscholastic.models.myprofile.request.SpsOrganization;
import com.myscholastic.models.myprofile.response.SpsOrganizationResponse;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SpsOrgServiceTest {

    private SpsOrgService spsOrgService;
    private IamSpsOrgClient iamSpsOrgClient;

    @Before
    public void setup() {
        iamSpsOrgClient = mock(IamSpsOrgClient.class);
        spsOrgService = new SpsOrgService(iamSpsOrgClient);
    }

    @Test
    public void submitSchool() throws Exception {
        SpsOrganization spsOrganization = new SpsOrganization();
        spsOrganization.setSchoolName("school1");
        spsOrganization.setAddress1("address1");
        spsOrganization.setCity("city");
        spsOrganization.setZipcode("11111");
        spsOrganization.setReportingSchoolType("HOS");
        spsOrganization.setLocationType("US");
        SpsOrganizationResponse expected = new SpsOrganizationResponse();
        expected.setName("school1");
        when(iamSpsOrgClient.submitSchool("domestic", spsOrganization)).thenReturn(expected);

        SpsOrganizationResponse actual = spsOrgService.submitSchool(spsOrganization);
        assertNotNull(actual);
        assertThat(actual.getName(), equalTo(expected.getName()));
    }
}