package com.myscholastic.services.cybersource;

import com.myscholastic.models.cybersource.CyberSourceDto;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class HtmlServiceTest {

    private HtmlService htmlService;

    @Before
    public void setUp() throws Exception {
        htmlService = new HtmlService();
    }

    @Test
    public void ewallet() {
        CyberSourceDto cyberSourceDto = CyberSourceDto.builder()
            .cardType("001").cardNumber("xxxxx123").paymentToken("2877")
            .build();
        String html = htmlService.getHtml(200, cyberSourceDto, HtmlService.Type.EWALLET);
        assertThat(html, is("<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\"><head>" +
            "<script type=\"text/javascript\">" +
            "function ready(){window.parent.processEwalletRequest(200, '001', 'xxxxx123', '2877');}" +
            "</script></head><body onload=\"ready();\"></body></html>"));
    }

    @Test
    public void subscription() {
        String html = htmlService.getHtml(200, HtmlService.Type.SUBSCRIPTION);
        assertThat(html, is("<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\"><head>" +
            "<script type=\"text/javascript\">" +
            "function ready(){window.parent.processSubscriptionRequest(200);}" +
            "</script></head><body onload=\"ready();\"></body></html>"));
    }

    @Test
    public void myProfile() {
        String html = htmlService.getHtml(200, HtmlService.Type.DEFAULT);
        assertThat(html, is("<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\"><head>" +
            "<script type=\"text/javascript\">" +
            "function ready(){window.parent.processRequest(200);}" +
            "</script></head><body onload=\"ready();\"></body></html>"));
    }
}