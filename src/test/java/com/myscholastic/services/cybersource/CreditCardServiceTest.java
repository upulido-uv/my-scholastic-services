package com.myscholastic.services.cybersource;

import com.myscholastic.feignclients.idam.CreditCardClient;
import com.myscholastic.feignclients.subscription.UpdateBillingClient;
import com.myscholastic.models.cybersource.Address;
import com.myscholastic.models.cybersource.CyberSourceDto;
import com.myscholastic.models.cybersource.Wallet;
import com.myscholastic.models.subscription.UpdateBillingResponse;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.isNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CreditCardServiceTest {

    private CreditCardClient creditCardClient;
    private UpdateBillingClient updateBillingClient;
    private CreditCardService creditCardService;

    @Before
    public void setUp() throws Exception {
        creditCardClient = mock(CreditCardClient.class);
        updateBillingClient = mock(UpdateBillingClient.class);
        creditCardService = new CreditCardService(creditCardClient, updateBillingClient);
    }

    @Test
    public void addOrEditCreditCard() {
        Wallet myWallet = Wallet.builder()
            .cbsToken("edit-token")
            .billingAddress(new Address())
            .source("customer")
            .cardType("")
            .maskedCardNumber(null)
            .primary(false)
            .phoneNumber(null)
            .phoneExtension(null)
            .expiration(null)
            .build();

        CyberSourceDto cyberSourceDto = new CyberSourceDto();
        cyberSourceDto.setUserId("2998");
        cyberSourceDto.setCardAction("EDIT");
        cyberSourceDto.setEditPaymentToken("edit-token");

        when(creditCardClient.addCreditCard("2998", "edit-token", myWallet)).thenReturn(myWallet);
        Wallet actual = creditCardService.addOrEditCreditCard(cyberSourceDto);
        assertThat(actual.getCbsToken(), is("edit-token"));
    }

    @Test
    public void addCreditCardForEWallet() {
        Wallet myWallet = Wallet.builder()
            .cbsToken(null)
            .billingAddress(new Address())
            .source("customer")
            .cardType("")
            .maskedCardNumber(null)
            .primary(false)
            .phoneNumber(null)
            .phoneExtension(null)
            .expiration(null)
            .build();

        CyberSourceDto cyberSourceDto = new CyberSourceDto();
        cyberSourceDto.setUserId("2998");
        cyberSourceDto.setCardAction("ADD");
        cyberSourceDto.setSaveToProfile("true");
        when(creditCardClient.addCreditCard("2998",null, myWallet)).thenReturn(myWallet);
        Wallet saveToProfile = creditCardService.addCreditCardForEWallet(cyberSourceDto, "Add");
        assertThat(saveToProfile.getSource(), is("customer"));

        // I don't understand the business logic behind this..
        cyberSourceDto.setSaveToProfile("false");
        Wallet notSaveToProfile = creditCardService.addCreditCardForEWallet(cyberSourceDto, "Add");
        assertNotNull(notSaveToProfile);

        // EWallet doesn't support 'Edit'..
        cyberSourceDto.setCardAction("Edit");
        Wallet actionIsEdit = creditCardService.addCreditCardForEWallet(cyberSourceDto, "Edit");
        assertNull(actionIsEdit);

    }

    @Test
    public void updateBilling() {
        UpdateBillingResponse updateBillingResponse = new UpdateBillingResponse();
        updateBillingResponse.setIsSuccess("true");

        CyberSourceDto cyberSourceDto = new CyberSourceDto();
        cyberSourceDto.setUserId("2448");
        cyberSourceDto.setSubId("1010");

        when(updateBillingClient.updateBilling("SPS_UD=2448", "1010")).thenReturn(updateBillingResponse);
        assertThat(updateBillingResponse.getIsSuccess(), is("true"));
    }
}