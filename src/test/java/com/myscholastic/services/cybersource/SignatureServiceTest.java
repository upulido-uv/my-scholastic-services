package com.myscholastic.services.cybersource;

import com.myscholastic.configurations.CyberSourceConfigProperties;
import com.myscholastic.exception.SignatureValidationException;
import com.myscholastic.models.cybersource.Signature;
import com.myscholastic.services.aws.SSMService;
import com.myscholastic.utils.SignatureUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SignatureServiceTest {

    private SignatureService signatureService;

    private static final String ACCESS_KEY = "access_key";
    private static final String PROFILE_ID = "profile_id";
    private static final String EWALLET_ACCESS_KEY = "ewallet_access_key";
    private static final String EWALLET_PROFILE_ID = "ewallet_profile_id";
    private static final String EWALLET_MERCHANT_DATA35 = "ewallet_merchant_data35";
    private static final String EWALLET_ADD_UNSIGNED = "ewallet_add_unsigned";
    private static final String ADD_SIGNED_FIELD = "add_signed_field1";
    private static final String EDIT_MERCHANT_DATA_35 = "edit_merchant_35";
    private static final String ONE_TIME_MECHANT_DATA_35 = "mechantdata35";
    private static final String ONE_TIME_UNSIGNED_FIELD = "All these names confuse me";
    private static final String UPDATE_BILLING_MERCHANT_DATA_35 = "merchant35";
    private static final String EDIT_SIGNED_FIELD_NAMES = "signed_field1,signed_field2";
    private static final String EDIT_BILLING_UNSIGNED_FIELD_NAMES = "unsigned_field1,unsigned_field2";
    private static final String EDIT_UNSIGNED_FIELD_NAMES = "edit_unsigned_field1";
    private static final String UPDATE_BILLING_ACCESS_KEY = "myAccessKey";
    private static final String UPDATE_BILLING_PROFILE_ID = "myProfileId";
    private static final String ADD_ACTION_URL = "https://scholastic.com";
    private static final String EDIT_ACTION_URL = "https://scholastic.com";
    private static final String NONDETERMINISTIC = "NONDETERMINISTIC";

    private static final String[] keys = {"mysecret0", "mysecret1", "mysecret2"};

    @Before
    public void setUp() {
        CyberSourceConfigProperties props = mock(CyberSourceConfigProperties.class);
        SignatureUtil signatureUtil = mock(SignatureUtil.class);
        SSMService ssmService = mock(SSMService.class);
        signatureService = new SignatureService(props, signatureUtil, ssmService);

        //null representing the active profile
        when(ssmService.getParameter("/MyScholastic/Cybersource/null/secretkey", true)).thenReturn(keys[0]);
        when(ssmService.getParameter("/MyScholastic/Cybersource/null/ewalletSecretKey", true)).thenReturn(keys[1]);
        when(ssmService.getParameter("/MyScholastic/Cybersource/null/updateBillingSecretKey", true)).thenReturn(keys[2]);

        // ADD_ONE_TIME
        when(props.getAddSignedFieldNames()).thenReturn(ADD_SIGNED_FIELD);
        when(props.getOneTimeMerchantData35()).thenReturn(ONE_TIME_MECHANT_DATA_35);
        when(props.getOneTimeeditBillingUnSignedFieldNames()).thenReturn(ONE_TIME_UNSIGNED_FIELD);

        // EWALLET
        when(props.getEwalletAccessKey()).thenReturn(EWALLET_ACCESS_KEY);
        when(props.getEwalletProfileId()).thenReturn(EWALLET_PROFILE_ID);
        when(props.getAddMerchantData35()).thenReturn(EWALLET_MERCHANT_DATA35);
        when(props.getAddUnSignedFieldNames()).thenReturn(EWALLET_ADD_UNSIGNED);

        // DEFAULT
        when(props.getAccessKey()).thenReturn(ACCESS_KEY);
        when(props.getProfileId()).thenReturn(PROFILE_ID);

        // EDIT, UPDATEBILLING
        when(props.getUpdateBillingMerchantData35()).thenReturn(UPDATE_BILLING_MERCHANT_DATA_35);
        when(props.getEditSignedFieldNames()).thenReturn(EDIT_SIGNED_FIELD_NAMES);
        when(props.getEditBillingUnSignedFieldNames()).thenReturn(EDIT_BILLING_UNSIGNED_FIELD_NAMES);
        when(props.getUpdateBillingAccessKey()).thenReturn(UPDATE_BILLING_ACCESS_KEY);
        when(props.getUpdateBillingProfileId()).thenReturn(UPDATE_BILLING_PROFILE_ID);

        // EDIT, DEFAULT
        when(props.getEditUnSignedFieldNames()).thenReturn(EDIT_UNSIGNED_FIELD_NAMES);
        when(props.getEditMerchantData35()).thenReturn(EDIT_MERCHANT_DATA_35);

        when(props.getEditActionUrl()).thenReturn(EDIT_ACTION_URL);
        when(props.getAddActionUrl()).thenReturn(ADD_ACTION_URL);

        when(signatureUtil.getReferenceNumber()).thenReturn(NONDETERMINISTIC);
        when(signatureUtil.getTransactionUUID()).thenReturn(NONDETERMINISTIC);
        when(signatureUtil.getSignedDateTime()).thenReturn(NONDETERMINISTIC);
    }

    /* In AEM My Scholastic, only "EDIT" and "UPDATE_BILLING" seems to be called from the front end */
    @Test
    public void whenActionIsEDITAndTypeIsUPDATE_BILLING() {
        String plainText = getPlainTextEditAndUPDATEBILLING();
        Signature expected = Signature.builder()
            .signature(SignatureUtil.getHmac(plainText, "mysecret2"))
            .accessKey(UPDATE_BILLING_ACCESS_KEY)
            .profileId(UPDATE_BILLING_PROFILE_ID)
            .currency("USD")
            .locale("en")
            .paymentMethod("card")
            .merchant_defined_data35(UPDATE_BILLING_MERCHANT_DATA_35)
            .signed_field_names(EDIT_SIGNED_FIELD_NAMES)
            .unsigned_field_names(EDIT_BILLING_UNSIGNED_FIELD_NAMES)
            .actionUrl(EDIT_ACTION_URL)
            .reference_number(NONDETERMINISTIC)
            .transaction_type("update_payment_token")
            .transaction_uuid(NONDETERMINISTIC)
            .country("US")
            .signed_date_time(NONDETERMINISTIC)
            .amount("0.00")
            .merchant_defined_data24("MyScholastic").build();

        Signature actual = signatureService.sign("EDIT", "UPDATE_BILLING", "mytoken");
        assertThat(actual, samePropertyValuesAs(expected));

        actual = signatureService.signUpdateSubscriptionAuthorization("mytoken");
        assertThat(actual, samePropertyValuesAs(expected));
    }

    @Test
    public void whenActionIsEDITAndTypeIsANY() {
        String plainText = getPlainTextWithActionEDITAndTypeANY();
        Signature expected = Signature.builder()
            .signature(SignatureUtil.getHmac(plainText, "mysecret0"))
            .accessKey(ACCESS_KEY)
            .profileId(PROFILE_ID)
            .currency("USD")
            .locale("en")
            .paymentMethod("card")
            .merchant_defined_data35(EDIT_MERCHANT_DATA_35)
            .signed_field_names(EDIT_SIGNED_FIELD_NAMES)
            .unsigned_field_names(EDIT_UNSIGNED_FIELD_NAMES)
            .actionUrl(EDIT_ACTION_URL)
            .reference_number(NONDETERMINISTIC)
            .transaction_type("update_payment_token")
            .transaction_uuid(NONDETERMINISTIC)
            .country("US")
            .signed_date_time(NONDETERMINISTIC)
            .amount("0.00")
            .merchant_defined_data24("MyScholastic").build();

        Signature actual = signatureService.sign("EDIT", "RAND", "mytoken");
        assertThat(actual, samePropertyValuesAs(expected));

        actual = signatureService.signEditCardAuthorization("mytoken");
        assertThat(actual, samePropertyValuesAs(expected));

    }

    @Test
    public void whenActionIsADDAndTypeIsADD_ONE_TIME() {
        String plainText = getPlainTextWithActionAdd();
        Signature expected = Signature.builder()
            .signature(SignatureUtil.getHmac(plainText, "mysecret2"))
            .accessKey(UPDATE_BILLING_ACCESS_KEY)
            .profileId(UPDATE_BILLING_PROFILE_ID)
            .currency("USD")
            .locale("en")
            .paymentMethod("card")
            .merchant_defined_data35(ONE_TIME_MECHANT_DATA_35)
            .signed_field_names(ADD_SIGNED_FIELD)
            .unsigned_field_names(ONE_TIME_UNSIGNED_FIELD)
            .actionUrl(ADD_ACTION_URL)
            .reference_number(NONDETERMINISTIC)
            .transaction_type("create_payment_token")
            .transaction_uuid(NONDETERMINISTIC)
            .country("US")
            .signed_date_time(NONDETERMINISTIC)
            .merchant_defined_data24("MyScholastic").build();

        Signature actual = signatureService.sign("ADD", "ADD_ONE_TIME", null);
        assertThat(actual, samePropertyValuesAs(expected));
    }

    @Test
    public void whenActionIsADDAndTypeIsEWALLET() {
        String plainText = getPlainTextWithActionAddAndTypeEWallet();
        Signature expected = Signature.builder()
            .signature(SignatureUtil.getHmac(plainText, "mysecret1"))
            .accessKey(EWALLET_ACCESS_KEY)
            .profileId(EWALLET_PROFILE_ID)
            .currency("USD")
            .locale("en")
            .paymentMethod("card")
            .merchant_defined_data35(EWALLET_MERCHANT_DATA35)
            .signed_field_names(ADD_SIGNED_FIELD)
            .unsigned_field_names(EWALLET_ADD_UNSIGNED)
            .actionUrl(ADD_ACTION_URL)
            .reference_number(NONDETERMINISTIC)
            .transaction_type("create_payment_token")
            .transaction_uuid(NONDETERMINISTIC)
            .country("US")
            .signed_date_time(NONDETERMINISTIC)
            .merchant_defined_data24("MyScholastic").build();

        Signature actual = signatureService.sign("ADD", "EWALLET", null);
        assertThat(actual, samePropertyValuesAs(expected));

        actual = signatureService.signEwalletTransaction(null);
        assertThat(actual, samePropertyValuesAs(expected));
    }



    @Test
    public void whenActionIsADDAndTypeIsANY() {
        String plainText = getPlainTextWithActionAddAndTypeANY();
        Signature expected = Signature.builder()
            .signature(SignatureUtil.getHmac(plainText, "mysecret0"))
            .accessKey(ACCESS_KEY)
            .profileId(PROFILE_ID)
            .currency("USD")
            .locale("en")
            .paymentMethod("card")
            .merchant_defined_data35(EWALLET_MERCHANT_DATA35)
            .signed_field_names(ADD_SIGNED_FIELD)
            .unsigned_field_names(EWALLET_ADD_UNSIGNED)
            .actionUrl(ADD_ACTION_URL)
            .reference_number(NONDETERMINISTIC)
            .transaction_type("create_payment_token")
            .transaction_uuid(NONDETERMINISTIC)
            .country("US")
            .signed_date_time(NONDETERMINISTIC)
            .merchant_defined_data24("MyScholastic").build();

        Signature actual = signatureService.sign("ADD", "RANDOMTYPE", null);
        assertThat(actual, samePropertyValuesAs(expected));

        actual = signatureService.signAddCardAuthorization();
        assertThat(actual, samePropertyValuesAs(expected));
    }



    private String getPlainTextEditAndUPDATEBILLING() {
        return "access_key=" + UPDATE_BILLING_ACCESS_KEY + "," +
            "profile_id=" + UPDATE_BILLING_PROFILE_ID + "," +
            "transaction_type=update_payment_token," +
            "locale=en," +
            "payment_method=card," +
            "currency=USD," +
            "amount=0.00," +
            "signed_field_names=" + EDIT_SIGNED_FIELD_NAMES +"," +
            "unsigned_field_names=" + EDIT_BILLING_UNSIGNED_FIELD_NAMES +"," +
            "transaction_uuid=NONDETERMINISTIC," +
            "reference_number=NONDETERMINISTIC," +
            "payment_token=mytoken," +
            "signed_date_time=NONDETERMINISTIC";
    }

    // {action: ADD, type: ADD_ONE_TIME}
    private String getPlainTextWithActionAdd() {
        return "access_key=" + UPDATE_BILLING_ACCESS_KEY + "," +
            "profile_id=" + UPDATE_BILLING_PROFILE_ID + "," +
            "transaction_type=create_payment_token," +
            "locale=en," +
            "payment_method=card," +
            "currency=USD," +
            "signed_field_names=" + ADD_SIGNED_FIELD + "," +
            "unsigned_field_names=" + ONE_TIME_UNSIGNED_FIELD +"," +
            "transaction_uuid=NONDETERMINISTIC," +
            "reference_number=NONDETERMINISTIC," +
            "signed_date_time=NONDETERMINISTIC";
    }

    private String getPlainTextWithActionAddAndTypeEWallet() {
        return "access_key=" + EWALLET_ACCESS_KEY + "," +
            "profile_id=" + EWALLET_PROFILE_ID + "," +
            "transaction_type=create_payment_token," +
            "locale=en," +
            "payment_method=card," +
            "currency=USD," +
            "signed_field_names=" + ADD_SIGNED_FIELD + "," +
            "unsigned_field_names=" + EWALLET_ADD_UNSIGNED + "," +
            "transaction_uuid=NONDETERMINISTIC," +
            "reference_number=NONDETERMINISTIC," +
            "signed_date_time=NONDETERMINISTIC";
    }

    private String getPlainTextWithActionAddAndTypeANY() {
        return "access_key=" + ACCESS_KEY + "," +
            "profile_id=" + PROFILE_ID + "," +
            "transaction_type=create_payment_token," +
            "locale=en," +
            "payment_method=card," +
            "currency=USD," +
            "signed_field_names=" + ADD_SIGNED_FIELD + "," +
            "unsigned_field_names=" + EWALLET_ADD_UNSIGNED + "," +
            "transaction_uuid=NONDETERMINISTIC," +
            "reference_number=NONDETERMINISTIC," +
            "signed_date_time=NONDETERMINISTIC";
    }

    private String getPlainTextWithActionEDITAndTypeANY() {
        return "access_key=" + ACCESS_KEY + "," +
            "profile_id=" + PROFILE_ID + "," +
            "transaction_type=update_payment_token," +
            "locale=en," +
            "payment_method=card," +
            "currency=USD," +
            "amount=0.00," +
            "signed_field_names=" + EDIT_SIGNED_FIELD_NAMES + "," +
            "unsigned_field_names=" + EDIT_UNSIGNED_FIELD_NAMES + "," +
            "transaction_uuid=NONDETERMINISTIC," +
            "reference_number=NONDETERMINISTIC," +
            "payment_token=mytoken," +
            "signed_date_time=NONDETERMINISTIC";
    }

    // Need JUnit 5 for better exception assertion
    @Test(expected = SignatureValidationException.class)
    public void givenSignatureOrSignedFieldNamesAreMissing() {
        String signatureFromCyberSource = SignatureUtil.getHmac("f1=v1,f2=v2,signed_field_names=f1,f2,signed_field_names", "mysecret0");
        // Need java 9+
        Map<String, String> formdataWithoutSignedFields = Stream.of(new String[][] {
            {"f1", "v1"},
            {"f2", "v2"},
            {"signature", signatureFromCyberSource}
        }).collect(Collectors.toMap(kv -> kv[0], kv -> kv[1]));

        signatureService.isSignatureValid(formdataWithoutSignedFields, SignatureUtil.SecretKeyType.EWALLET);

        Map<String, String> formDataWihtoutSignature = Stream.of(new String[][] {
            {"f1", "v1"},
            {"f2", "v2"},
            {"signed_field_names", "f1,f2,signed_field_names"},
        }).collect(Collectors.toMap(kv -> kv[0], kv -> kv[1]));

        signatureService.isSignatureValid(formDataWihtoutSignature, SignatureUtil.SecretKeyType.UPDATEBILLING);
    }

    @Test
    public void whenSignaturesMatch_thenReturnTrue() {
        String hmac = SignatureUtil.getHmac("f1=v1,f2=v2,signed_field_names=f1,f2,signed_field_names", "mysecret0");
        // Need java 9+
        Map<String, String> formdata = Stream.of(new String[][] {
            {"f1", "v1"},
            {"f2", "v2"},
            {"signed_field_names", "f1,f2,signed_field_names"},
            {"signature", hmac}
        }).collect(Collectors.toMap(kv -> kv[0], kv -> kv[1]));

        assertTrue(signatureService.isSignatureValid(formdata, SignatureUtil.SecretKeyType.DEFAULT));
    }

    @Test
    public void givenFormDataIsTampered_whenValidateSignature_thenReturnFalse() {
        String signatureFromCyberSource = SignatureUtil.getHmac("amount=1000,f1=v1,f2=v2,signed_field_names=f1,f2,signed_field_names", "mysecret0");
        Map<String, String> formdata = Stream.of(new String[][] {
            {"amount", "1000000"},
            {"f1", "v1"},
            {"f2", "v2"},
            {"signed_field_names", "f1,f2,signed_field_names"},
            {"signature", signatureFromCyberSource}
        }).collect(Collectors.toMap(kv -> kv[0], kv -> kv[1]));

        assertFalse(signatureService.isSignatureValid(formdata, SignatureUtil.SecretKeyType.DEFAULT));
    }

}
