package com.myscholastic.services.orderhistory;

import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.feignclients.orderhistory.OrderHistoryClient;
import com.myscholastic.models.myprofile.*;
import com.myscholastic.models.orderhistory.GenericOrder;
import com.myscholastic.models.orderhistory.OrderHistory;
import com.myscholastic.models.orderhistory.orderdetails.*;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OrderHistoryServiceTest {

    private OrderHistoryService orderHistoryService;
    private IamClient iamClient;
    private OrderHistoryClient orderHistoryClient;

    @Before
    public void setUp() throws Exception {
        iamClient = mock(IamClient.class);
        orderHistoryClient = mock(OrderHistoryClient.class);
        orderHistoryService = new OrderHistoryService(iamClient, orderHistoryClient);
    }

    @Test
    public void getOrderHistory() {
        User user = new User();
        Personas personas = new Personas();
        Educator educator = new Educator();
        School school = new School();

        // BCOE9
        school.setBcoe("BCOE");
        educator.setSchool(school);
        personas.setEducator(educator);
        user.setPersonas(personas);

        // UCN
        Identifiers identifiers = new Identifiers();
        identifiers.setUcn("ucn");
        user.setIdentifiers(identifiers);

        // Generic Order
        GenericOrder genericOrder = new GenericOrder();
        OrderHistory orderHistory0 = new OrderHistory();
        orderHistory0.setEventNumber("60074133");
        OrderHistory orderHistory1 = new OrderHistory();
        orderHistory1.setEventNumber("60071111");
        orderHistory1.setClickStore("dw_tso");

        OrderHistory[] orderHistories = {orderHistory0, orderHistory1};
        genericOrder.setGenericOrder(orderHistories);

        when(iamClient.getUser("2718")).thenReturn(user);
        when(orderHistoryClient.getOrderHistory("2718", "2019-04-19", "1950-01-01", "ucn", "BCOE"))
            .thenReturn(genericOrder);
        GenericOrder order = orderHistoryService.getOrderHistory("2718", "1950-01-01", "2019-04-19");
        assertNotNull(order);
        assertThat(order.getGenericOrder(), arrayWithSize(1));
        assertThat(order.getGenericOrder()[0].getEventNumber(), is("60071111"));
        assertThat(order.getGenericOrder()[0].getClickStore(), is("dw_tso"));
    }

    @Test
    public void testFieldsSetByOrderIds() throws IOException {
        when(orderHistoryClient.getOrderDetail("2718", "as400pcool", null, null, null, null))
            .thenReturn(getXml(0));
        OrderDetail actual0 = orderHistoryService.getOrderDetails("2718", "as400pcool", null, null, null, null);
        assertNotNull(actual0);
        assertTrue(actual0.isPcool());
        assertThat(actual0.getImageRootPath(), is("https://shop.scholastic.com/content/stores/"));
        assertThat(actual0.getReadingClubUrl(), is("https://clubs.scholastic.com/myreadingclubaccount?orderhistory=true"));

        when(orderHistoryClient.getOrderDetail("2718", "dw_tso", null, null, null, null))
            .thenReturn(getXml(0));
        OrderDetail actual1 = orderHistoryService.getOrderDetails("2718", "dw_tso", null, null, null, null);
        assertNotNull(actual1);
        assertTrue(actual1.isIndividualTracking());
        assertThat(actual1.getProductUrl(), is("https://shop.scholastic.com/content/teachers-ecommerce/en/search-results.html?search=1&filters=&text="));

        when(orderHistoryClient.getOrderDetail("2718", "AURORA", null, null, null, null))
            .thenReturn(getXml(0));
        OrderDetail actual2 = orderHistoryService.getOrderDetails("2718", "AURORA", null, null, null, null);
        assertNotNull(actual2);
        assertTrue(actual2.isBookFair());
        assertThat(actual2.getImageRootPath(), is("https://shop.scholastic.com/content/stores/"));
    }

    @Test
    public void getOrderDetail() throws IOException {
        when(orderHistoryClient.getOrderDetail("2718", "pcool", "eventNo", "2019", "ucn", "bcoe9"))
            .thenReturn(getXml(0));

        OrderDetail actual0 = orderHistoryService.getOrderDetails("2718", "pcool", "eventNo", "2019", "ucn", "bcoe9");


        OrderDetailsTotal orderTotals0 = actual0.getOrderTotals();
        assertNotNull(orderTotals0);
        assertThat(orderTotals0.getOrderId(), is("60074133"));
        assertThat(orderTotals0.getOrderDate(), is("03/13/2019"));
        assertThat(orderTotals0.getOrderStatus(), is("REJECTED ORDER"));
        assertNull(orderTotals0.getDetailURL());
        assertThat(orderTotals0.getStoreLabel(), is("Teacher Store"));
        assertThat(orderTotals0.getItemTotal(), is("2.99"));
        assertNull(orderTotals0.getDiscount());
        assertThat(orderTotals0.getShippingTotal(), is("2.25"));
        assertThat(orderTotals0.getTaxTotal(), is("0.36"));
        assertThat(orderTotals0.getGrandTotal(), is("5.60"));

        ShippingInfo shippingInfo0 = actual0.getShippingInfo();
        assertNotNull(shippingInfo0);
        assertThat(shippingInfo0.getName(), is("Jorey Castle"));
        assertThat(shippingInfo0.getAddress1(), is("myaddress1"));
        assertNull(shippingInfo0.getAddress2());
        assertThat(shippingInfo0.getCity(), is("mycity"));
        assertThat(shippingInfo0.getState(), is("NJ"));
        assertThat(shippingInfo0.getZip(), is("08820"));
        assertThat(shippingInfo0.getShip_to_phone(), is("0088800"));
        assertThat(shippingInfo0.getShip_type(), is("DUMMY"));

        BillingInformation billingInfo0 = actual0.getBillingInfo();
        assertNotNull(billingInfo0);
        assertThat(billingInfo0.getName(), is("Jory Castle"));
        assertThat(billingInfo0.getAddress1(), is("myaddress2"));
        assertNull(billingInfo0.getAddress2());
        assertThat(billingInfo0.getCity(), is("Irving"));
        assertThat(billingInfo0.getState(), is("TX"));
        assertThat(billingInfo0.getZip(), is("75039"));
        assertThat(billingInfo0.getBill_to_phone(), is("2144567897"));
        assertThat(billingInfo0.getBill_to_email(), is("qauser1@gmail.com"));
        assertThat(billingInfo0.getCard_brand(), is("Visa"));
        assertThat(billingInfo0.getCard_number(), is("************1111"));

        OrderItem[] orderItems = actual0.getOrderItem();
        assertThat(orderItems.length, is(1));
        OrderItem item = orderItems[0];
        assertThat(item.getOrder_item_id(), is("9780439799140-tso-us"));
        assertThat(item.getTitle(), is("Fly Guy: Hi! Fly Guy"));
        assertThat(item.getNickname(), is("SCHOOL"));
        assertThat(item.getShipping_amt(), is("2.25"));
        assertThat(item.getIsbn(), is("9780439799140"));
        assertThat(item.getPartNum(), is("NTS9780439799140"));
        assertThat(item.getQuantity(), is("1"));
        assertThat(item.getList_price(), is("2.99"));
        assertThat(item.getTax_dollar_amt(), is("0.36"));
        assertThat(item.getTotal_items_price(), is("2.99"));
        assertNull(item.getItem_adjust());
        assertThat(item.getItem_image(), is("media/products/40/9780439799140_sm.jpg"));
        assertThat(item.getProduct_type(), is("Non Digital"));
        assertNull(item.getTracking_href());
    }

    private String getXml(int num) throws IOException {
        return new String(Files.readAllBytes(
            Paths.get(String.format("src/test/resources/orderhistory/test%d.xml", num))
        ));
    }
}