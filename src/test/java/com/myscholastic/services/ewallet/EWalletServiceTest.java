package com.myscholastic.services.ewallet;

import com.myscholastic.feignclients.bookfair.BookfairsClient;
import com.myscholastic.feignclients.idam.EWalletClient;
import com.myscholastic.models.BaseIamResponseModel;
import com.myscholastic.models.ewallet.*;
import com.myscholastic.models.ewallet.request.CreateVoucherRequest;
import com.myscholastic.models.ewallet.request.CreateVoucherRequestAttributes;
import com.myscholastic.services.mail.MailService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.*;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EWalletServiceTest {

    private EWalletService eWalletService;
    private EWalletClient eWalletClient;
    private BookfairsClient bookfairsClient;
    private CreateVoucherRequest createVoucherRequest;
    private CreateVoucherRequestAttributes attributes;
    private FundingSource fundingSource;
    private BaseIamResponseModel createVoucherResponse;

    @Mock
    private MailService mailService;

    @Before
    public void setUp() throws Exception {
        eWalletClient = mock(EWalletClient.class);
        bookfairsClient = mock(BookfairsClient.class);
        eWalletService = new EWalletService(eWalletClient, bookfairsClient);
        createVoucherRequest = mock(CreateVoucherRequest.class);
        attributes = mock(CreateVoucherRequestAttributes.class);
        fundingSource = mock(FundingSource.class);
        createVoucherResponse = mock(BaseIamResponseModel.class);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void hasVoucherTest() {
        String spsId = "1729";
        String voucherId = "1234";
        List<EWalletId> eWalletIds = new ArrayList<>();
        EWalletId eWalletId0 = new EWalletId();
        eWalletId0.setId(2718L);
        eWalletIds.add(eWalletId0);

        List<Voucher> vouchers = new ArrayList<>();
        Voucher voucher = new Voucher();
        voucher.setId(1234);
        vouchers.add(voucher);

        when(eWalletClient.getEWalletBySpsId(spsId)).thenReturn(eWalletIds);
        when(eWalletClient.getVouchersByWalletId("2718")).thenReturn(vouchers);

        boolean actual = eWalletService.hasVoucher(spsId, "1234");
        assertTrue(actual);
    }

    @Test
    public void getFairVouchersList() {
        List<Voucher> vouchers = getVouchers();
        when(eWalletClient.getVouchersByWalletId("1282")).thenReturn(vouchers);
        // Sources, or FairIds are
        FairIdList fairIdList = FairIdList.builder().ids(new HashSet<>(Arrays.asList(0L, 1L))).build();
        List<Fair> fairs = getFairs();
        when(bookfairsClient.getFairsInfo(fairIdList)).thenReturn(fairs);

        FairVouchersJson fairVouchersJson0 = eWalletService.getFairVouchersJson("1282", null);

        assertNotNull(fairVouchersJson0.getFairVouchersList());
        List<FairVouchers> fairVouchersWithoutCookie = fairVouchersJson0.getFairVouchersList();
        assertThat(fairVouchersWithoutCookie, hasSize(2));

        assertThat(fairVouchersWithoutCookie.get(0).getStartDate(), is("2019-02-14"));
        assertThat(fairVouchersWithoutCookie.get(1).getStartDate(), is("2000-01-01"));

        assertThat(fairVouchersWithoutCookie.get(0).getVouchers(), hasSize(1));
        assertThat(fairVouchersWithoutCookie.get(1).getVouchers(), hasSize(1));
        assertThat(fairVouchersWithoutCookie.get(0).getVouchers().get(0).getAttributes().getStudentFirstName(), is("Alice"));
        assertThat(fairVouchersWithoutCookie.get(1).getVouchers().get(0).getAttributes().getStudentFirstName(), is("Bob"));

        assertThat(fairVouchersWithoutCookie.get(0).getVouchers().get(0).getTransactions(), hasSize(2));
        assertThat(fairVouchersWithoutCookie.get(0).getVouchers().get(0).getTransactions().get(0).getCreatedOn(),
            is("2019-03-12T16:54:10Z"));
        assertThat(fairVouchersWithoutCookie.get(0).getVouchers().get(0).getTransactions().get(1).getCreatedOn(),
            is("2019-03-12T16:52:10Z"));

        FairVouchersJson fairVouchersJson1 = eWalletService.getFairVouchersJson("1282", 1L);
        List<FairVouchers> fairVouchersWithCookie = fairVouchersJson1.getFairVouchersList();
        assertThat(fairVouchersWithCookie, hasSize(2));
        assertThat(fairVouchersWithCookie.get(0).getStartDate(), is("2000-01-01"));
        assertThat(fairVouchersWithCookie.get(1).getStartDate(), is("2019-02-14"));
    }

    @Test
    public void createEWalletAndVoucherTest() {
        String spsId = "1729";
        String fairId = "3968812";
        String maskedPan = "411111xxxxxx1111";
        Long walletId = 110763L;

        // build the fairs return value
        List<Fair> fairs = new ArrayList<>();
        String startDate = LocalDate.now().minusWeeks(1).toString();
        String endDate = LocalDate.now().plusWeeks(1).toString();
        Fair fair0 = Fair.builder().id(Long.parseLong(fairId)).startDate(startDate).endDate(endDate).build();
        fairs.add(fair0);

        // build the fairIds parameter
        Set<Long> fairIds = new HashSet<Long>();
        fairIds.add(Long.parseLong(fairId));
        FairIdList fairIdList = new FairIdList(fairIds);

        // build ewallet param
        List<EWalletId> eWalletIds = new ArrayList<>();
        EWalletId eWalletId0 = new EWalletId();
        eWalletId0.setId(walletId);
        eWalletIds.add(eWalletId0);

        when(createVoucherRequest.getAttributes()).thenReturn(attributes);
        when(attributes.getFairId()).thenReturn(fairId);
        when(createVoucherRequest.getFundingSource()).thenReturn(fundingSource);
        when(fundingSource.getMaskedPan()).thenReturn(maskedPan);
        when(bookfairsClient.getFairsInfo(fairIdList)).thenReturn(fairs);
        when(eWalletClient.getEWalletBySpsId(spsId)).thenReturn(eWalletIds);
        when(eWalletClient.createVoucherByWalletId(walletId, createVoucherRequest)).thenReturn(createVoucherResponse);
        when(createVoucherResponse.getId()).thenReturn(null);

        eWalletService.createEWalletAndVoucher(createVoucherRequest, spsId);
    }

    @Test(expected = ResponseStatusException.class)
    public void createEWalletAndVoucherOutsideOfDateTest() {
        String spsId = "1729";
        String fairId = "3968812";
        String maskedPan = "411111xxxxxx1111";
        Long walletId = 110763L;

        // build the fairs return value
        List<Fair> fairs = new ArrayList<>();
        String startDate = LocalDate.now().plusWeeks(3).toString();
        String endDate = LocalDate.now().plusWeeks(4).toString();
        Fair fair0 = Fair.builder().id(Long.parseLong(fairId)).startDate(startDate).endDate(endDate).build();
        fairs.add(fair0);

        // build the fairIds parameter
        Set<Long> fairIds = new HashSet<Long>();
        fairIds.add(Long.parseLong(fairId));
        FairIdList fairIdList = new FairIdList(fairIds);

        // build ewallet param
        List<EWalletId> eWalletIds = new ArrayList<>();
        EWalletId eWalletId0 = new EWalletId();
        eWalletId0.setId(walletId);
        eWalletIds.add(eWalletId0);

        when(createVoucherRequest.getAttributes()).thenReturn(attributes);
        when(attributes.getFairId()).thenReturn(fairId);
        when(createVoucherRequest.getFundingSource()).thenReturn(fundingSource);
        when(fundingSource.getMaskedPan()).thenReturn(maskedPan);
        when(bookfairsClient.getFairsInfo(fairIdList)).thenReturn(fairs);
        when(eWalletClient.getEWalletBySpsId(spsId)).thenReturn(eWalletIds);
        when(eWalletClient.createVoucherByWalletId(walletId, createVoucherRequest)).thenReturn(createVoucherResponse);
        when(createVoucherResponse.getId()).thenReturn(null);

        eWalletService.createEWalletAndVoucher(createVoucherRequest, spsId);
    }

    private List<Fair> getFairs() {
        List<Fair> fairs = new ArrayList<>();
        Fair fair0 = Fair.builder().id(0L).startDate("2019-02-14").build();
        Fair fair1 = Fair.builder().id(1L).startDate("2000-01-01").build();
        fairs.add(fair0);
        fairs.add(fair1);
        return fairs;
    }

    private List<Voucher> getVouchers() {
        List<Voucher> vouchers = new ArrayList<>();
        Voucher voucher0 = getNewVoucherInstance(0);
        Voucher voucher1 = getNewVoucherInstance(1);

        vouchers.add(voucher0);
        vouchers.add(voucher1);
        return vouchers;
    }

    private Voucher getNewVoucherInstance(int order) {
        Voucher voucher = new Voucher();

        // VoucherTransaction Sort
        VoucherTransaction voucherTransaction0 = new VoucherTransaction();
        voucherTransaction0.setCreatedOn("2019-03-12T16:52:10Z");
        VoucherTransaction voucherTransaction1 = new VoucherTransaction();
        voucherTransaction1.setCreatedOn("2019-03-12T16:54:10Z");
        List<VoucherTransaction> voucherTransactions = Arrays.asList(voucherTransaction0, voucherTransaction1);

        FundingSource fundingSource = new FundingSource();
        fundingSource.setCardType("00" + order);
        fundingSource.setMaskedPan("94020xxxxxxx1234");
        Attribute attribute = new Attribute();

        attribute.setStudentFirstName((order == 0 ? "Alice" : "Bob"));
        voucher.setAttributes(attribute);
        voucher.setSource(String.valueOf(order));
        voucher.setFundingSource(fundingSource);
        voucher.setTransactions(voucherTransactions);

        return voucher;
    }
}