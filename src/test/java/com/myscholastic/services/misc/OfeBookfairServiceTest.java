package com.myscholastic.services.misc;

import com.myscholastic.feignclients.bookfair.BookfairsCachedClient;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.myprofile.BookfairOrganizationInfo;
import com.myscholastic.models.myprofile.IdamBookfairOrganizations;
import lombok.val;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OfeBookfairServiceTest {

    public static final String USER_ID = "2718";
    public static final String ORG_ID = "1111";
    public static final String ORG_UCN = "1111111";

    private OfeBookfairService ofeBookfairService;

    @Before
    public void setUp() {
        val idamClient = mock(IamClient.class);
        val bookfairsCachedClient = mock(BookfairsCachedClient.class);

        ofeBookfairService = new OfeBookfairService(idamClient, bookfairsCachedClient);
        IdamBookfairOrganizations idamBookfairOrganizations = new IdamBookfairOrganizations();
        idamBookfairOrganizations.setDefaultOrg(true);
        idamBookfairOrganizations.setOrgId(ORG_ID);

        List<IdamBookfairOrganizations> expectedList = new ArrayList<>();
        expectedList.add(idamBookfairOrganizations);

        when(idamClient.getUserBFOrgUcns(USER_ID)).thenReturn(expectedList);

        BookfairOrganizationInfo bookfairOrganizationInfo = new BookfairOrganizationInfo();
        bookfairOrganizationInfo.setIdamOrgUcn(ORG_UCN);

        List<BookfairOrganizationInfo> bkOrgInfoList = new ArrayList<>();
        bkOrgInfoList.add(bookfairOrganizationInfo);

        when(bookfairsCachedClient.getBookfairOrganizationInfo(ORG_ID)).thenReturn(bkOrgInfoList);
    }

    @Test
    public void testGetBookfairOrgNames() {

        List<BookfairOrganizationInfo> results = ofeBookfairService.getBookfairOrgNames(USER_ID);

        assertNotNull(results);
        assertEquals(1, results.size());
        assertEquals(ORG_UCN, results.get(0).getIdamOrgUcn());
    }
}
