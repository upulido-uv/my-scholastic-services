package com.myscholastic.services.rotatingsecret;

import com.myscholastic.feignclients.idam.IamSecretsManagerClient;
import com.myscholastic.models.authentication.response.SecretResponse;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;

public class RotatingSecretServiceTest {

    private IamSecretsManagerClient iamSecretsManagerClient;
    private RotatingSecretService rotatingSecretService;

    @Before
    public void setUp() {
        iamSecretsManagerClient = mock(IamSecretsManagerClient.class);
        rotatingSecretService = new RotatingSecretService(iamSecretsManagerClient);
    }

    @Ignore
    @Test
    public void givenCacheDoesNotHaveKeys_thenRetrieveKeys() {
        SecretResponse secretResponse = new SecretResponse();
        secretResponse.setCurrentKey("prod-20200211");
        secretResponse.setMessage("Successfully provisioned.");
        secretResponse.setStatusCode(200);
        secretResponse.setKeys(Collections.singletonMap("prod-20200211", "abcdefghjkl"));

        when(iamSecretsManagerClient.getKeys()).thenReturn(secretResponse);
        SecretResponse actual = rotatingSecretService.getKeys();

        verify(iamSecretsManagerClient, times(1)).getKeys();
        // Check for equality
        assertEquals(secretResponse, actual);
        // Check for deep copy
        assertFalse(secretResponse == actual);

    }

    @Ignore
    @Test
    public void givenCacheDoesHaveKeys_thenReturnCachedKeys() {
        SecretResponse secretResponse = new SecretResponse();
        secretResponse.setCurrentKey("prod-20200211");
        secretResponse.setMessage("Successfully provisioned.");
        secretResponse.setStatusCode(200);
        secretResponse.setKeys(Collections.singletonMap("prod-20200211", "abcdefghjkl"));

        when(iamSecretsManagerClient.getKeys()).thenReturn(secretResponse);
        rotatingSecretService.updateKeys();

        SecretResponse actual1 = rotatingSecretService.getKeys();
        SecretResponse actual2 = rotatingSecretService.getKeys();

        verify(iamSecretsManagerClient, times(1)).getKeys();
        assertEquals(secretResponse, actual1);
        assertEquals(secretResponse, actual2);
        assertFalse(secretResponse == actual1);
        assertFalse(secretResponse == actual2);
        assertFalse(actual1 == actual2);

    }

    @Ignore
    @Test
    public void givenUpdatingCacheFails_thenDontWipeCache() {
        SecretResponse secretResponse = new SecretResponse();
        secretResponse.setCurrentKey("prod-20200211");
        secretResponse.setMessage("Successfully provisioned.");
        secretResponse.setStatusCode(200);
        secretResponse.setKeys(Collections.singletonMap("prod-20200211", "abcdefghjkl"));

        // Initialize cache value
        when(iamSecretsManagerClient.getKeys()).thenReturn(secretResponse);
        rotatingSecretService.updateKeys();

        when(iamSecretsManagerClient.getKeys()).thenThrow(new RuntimeException());
        rotatingSecretService.updateKeys();

        SecretResponse actual = rotatingSecretService.getKeys();
        verify(iamSecretsManagerClient, times(2)).getKeys();
        assertEquals(secretResponse, actual);
        assertFalse(secretResponse == actual);
    }

    @Ignore
    @Test
    public void givenCacheNeedsUpdating_thenUpdateCache() {
        SecretResponse secretResponse = new SecretResponse();
        secretResponse.setCurrentKey("prod-20200211");
        secretResponse.setMessage("Successfully provisioned.");
        secretResponse.setStatusCode(200);
        secretResponse.setKeys(Collections.singletonMap("prod-20200211", "abcdefghjkl"));

        // Initialize cache value
        when(iamSecretsManagerClient.getKeys()).thenReturn(secretResponse);
        rotatingSecretService.updateKeys();

        SecretResponse actual = rotatingSecretService.getKeys();
        verify(iamSecretsManagerClient, times(1)).getKeys();
        assertEquals(secretResponse, actual);
        assertFalse(secretResponse == actual);
    }
}
