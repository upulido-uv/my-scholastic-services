package com.myscholastic.services.aws;

import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagement;
import com.amazonaws.services.simplesystemsmanagement.model.GetParameterRequest;
import com.amazonaws.services.simplesystemsmanagement.model.GetParameterResult;
import com.amazonaws.services.simplesystemsmanagement.model.Parameter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class SSMServiceTests {

    public static final String RESULT_VALUE = "test";
    public static final String PARAMETER_STORE_KEY = "key";

    @InjectMocks
    private SSMService ssmService = new SSMService();

    @Mock
    private AWSSimpleSystemsManagement awsSimpleSystemsManagement;

    @Before
    public void set_up() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getParameterTest() {

        GetParameterRequest getparameterRequest = new GetParameterRequest().withName(PARAMETER_STORE_KEY).withWithDecryption(false);

        Parameter parameter = new Parameter();
        parameter.setValue(RESULT_VALUE);
        GetParameterResult result = new GetParameterResult().withParameter(parameter);

        when(awsSimpleSystemsManagement.getParameter(getparameterRequest)).thenReturn(result);

        String value = ssmService.getParameter(PARAMETER_STORE_KEY, false);
        assertNotNull(value);
        assertTrue(value.equalsIgnoreCase(RESULT_VALUE));
    }
}
