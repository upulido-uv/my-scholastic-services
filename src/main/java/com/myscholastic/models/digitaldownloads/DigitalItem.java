package com.myscholastic.models.digitaldownloads;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.myscholastic.utils.DateFormatUtil;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.text.ParseException;

@Getter
@Setter
@JacksonXmlRootElement(localName="digitalitem")
public class DigitalItem {

    @JacksonXmlProperty(localName="storeid")
    private String storeId;

    @JacksonXmlProperty(localName="storelabel")
    private String storeLabel;

    @JacksonXmlProperty(localName="orderid")
    private String orderId;

    @JacksonXmlProperty(localName="orderurl")
    private String orderURL;

    @Setter(AccessLevel.NONE)
    @JacksonXmlProperty(localName="orderdate")
    private String orderDate;

    @JacksonXmlProperty(localName="ordersortdate")
    private String orderSortDate;

    @JacksonXmlProperty(localName="title")
    private String title;

    @JacksonXmlProperty(localName="downloadurl")
    private String downloadURL;

    public void setOrderDate(String orderDate) {
        this.orderDate = DateFormatUtil.formatDate(orderDate);
    }

}
