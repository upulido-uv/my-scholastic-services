package com.myscholastic.models.digitaldownloads;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Getter
@Setter
@Slf4j
@JacksonXmlRootElement(localName = "digitaldownloads")
public class DigitalDownloads {

    @JacksonXmlProperty(localName = "digitalitem")
    @JacksonXmlElementWrapper(useWrapping = false)
    private DigitalItem[] digitalItems;

}
