package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Personas implements Serializable {

    private static final long serialVersionUID = 1L;

    private Consumer consumer;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Kid kid;

    private Educator educator;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Parent parent;

}