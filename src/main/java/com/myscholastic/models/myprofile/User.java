package com.myscholastic.models.myprofile;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    private Personas personas;
    private Identifiers identifiers;
    private String status;
    private String realm;
    private BasicProfile basicProfile;
    private Contexts contexts;
    private Credentials credentials;
    private boolean offlineAccountFlag;
    private Wallet wallet;

    private Preferences preferences;
    private List<Organization> additionalOrgs;

}
