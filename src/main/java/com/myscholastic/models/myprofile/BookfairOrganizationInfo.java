package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class BookfairOrganizationInfo {

    String name; //This is OrgId or OrgUcn in from IDAM, modified in by setter!

    @JsonAlias("ucn")
    String idamOrgUcn;

    @JsonAlias("id")
    String cptkSchoolId;

    Integer percentProfit;
    String city;
    String state;
    String postalCode;
    String address1;
    String address2;
    boolean active; //Always true from Bookfairs, placeholder for future if needed
    boolean defaultOrg;

    Long fairId;
    String ofeStartDate;
    String ofeEndDate;
}