package com.myscholastic.models.myprofile.response;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LookupListReponse implements Serializable {

    private static final long serialVersionUID = 1L;

	List<LookupResponse> grades;
	
	List<LookupResponse> salutations;
	
	//reading-level-systems
	List<LookupResponse> readingLevelSystems;
	
	//teacher-role-groups
	List<LookupResponse> teacherRoleGroups;
	
	//teacher-grade-roles
	List<LookupResponse> teacherGradeRoles;
	
	//teacher-specialities
	List<LookupResponse> teacherSpecialities;
	
	//teacher-titles
	List<LookupResponse> teacherTitles;
}
