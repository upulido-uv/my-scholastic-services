package com.myscholastic.models.myprofile.response;

import java.io.Serializable;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SchoolResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String city;
    private String schoolUCN;
    private String address1;
    private String groupType;
    private String zipcode;
    private String creditCardState;
    private String schoolStatus;
    private String state;
    private String name;
    private String spsId;
}
