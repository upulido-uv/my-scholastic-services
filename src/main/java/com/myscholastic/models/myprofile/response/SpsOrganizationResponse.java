package com.myscholastic.models.myprofile.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SpsOrganizationResponse {
    private String city;
    private String address1;
    private String address3;
    private String address4;
    private String address5;
    private String modifiedDate;
    private String address2;
    private String country;
    private String reportingSchoolType;
    private String groupType;
    private String phone;
    private String zipcode;
    private String uspsTypeCode;
    private String addressTypeCode;
    private String state;
    private String name;
    private String spsId;
}
