package com.myscholastic.models.myprofile.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SchoolInfoResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private String city;
	private String schoolUCN;
	private String address1;
	private String address2;
	private String address3;
	private String address4;
	private String address5;
	private String country;
	private String groupType;
	private String phone;
	private String zipcode;
	private String borderId;
	private String creditCardState;
	private String schoolStatus;
	private String publicPrivateKey;
	private String uspsTypeCode;
	private String addressTypeCode;
	private String state;
	private String name;
	private String spsId;

}
