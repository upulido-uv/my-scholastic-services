package com.myscholastic.models.myprofile.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AppUserTypeResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	private String appLabel;
	private String appId;
	

}
