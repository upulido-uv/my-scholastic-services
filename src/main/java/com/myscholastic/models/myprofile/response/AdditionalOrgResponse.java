package com.myscholastic.models.myprofile.response;

import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdditionalOrgResponse {
    private String id;
    private String orgId;
    private String orgUcn;
    private String bookfairRole;
    private List<String> orgRoles;
}
