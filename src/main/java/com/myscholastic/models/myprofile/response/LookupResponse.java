package com.myscholastic.models.myprofile.response;


import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class LookupResponse implements Serializable {


	private static final long serialVersionUID = 1L;
	
	String key;
	
	@JsonProperty("value")
	@JsonAlias("name")
	String value;



}
