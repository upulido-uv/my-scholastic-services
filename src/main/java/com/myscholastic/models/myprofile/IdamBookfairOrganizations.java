package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class IdamBookfairOrganizations {

    String orgId;

    @JsonAlias("default")
    boolean defaultOrg;

}
