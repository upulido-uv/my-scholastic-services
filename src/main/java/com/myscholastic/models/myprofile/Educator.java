package com.myscholastic.models.myprofile;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Educator implements Serializable {

    private static final long serialVersionUID = 4873290561024525128L;

    private String readingLevelType;
    private School school;
    private String l1RegDate;
    private String firstTimeLoginInSchoolYear;
    private String altTeacherFlag;
    private String corpStatus;
}