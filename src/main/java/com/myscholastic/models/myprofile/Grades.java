package com.myscholastic.models.myprofile;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Grades implements Serializable {

    String value;
    String key;
}
