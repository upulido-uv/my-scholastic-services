package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class CreditCard implements Serializable {

    private static final long serialVersionUID = -2377576193821933398L;

    private String cbsToken;

    private Address billingAddress;

    private String source;

    private String cardType;

    private String maskedCardNumber;

    private IDMDate expiration;

    private boolean primary;

    private String phoneNumber;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String phoneExtension;

    private String email="";

    private int code;
}
