package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Address implements Serializable {

    private static final long serialVersionUID = 4467504399376316804L;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private long id;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String address1;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String address2;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String address3;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String address4;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String address5;
    private String city;
    private String state;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String country;
    private String postalCode;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String poBox;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String email;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String phone;

    private String firstName;
    private String lastName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String nickName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String type;
}
