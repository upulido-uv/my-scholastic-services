package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Clubs implements Serializable {

    private static final long serialVersionUID = 2142443167017103602L;
    private String activationCode;
    private String activationCodeId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean usedClubsInPast;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean mentor;
    private Boolean newTeacherFlag;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Boolean smsOptIn;

    private Boolean dwPilotParticipantFlag;
    private Boolean loggedIntoDwPilotFlag;
    private String dwPilotParticipantLastModified;
    private String loggedIntoDwPilotLastModified;
    private Boolean dwPilotParticipantParent;
    private Boolean loggedIntoDwPilotParent;
    private String dwPilotParticipantParentLastModified;
    private String loggedIntoDwPilotParentLastModified;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String bonusPoints;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String bonusYTDPoints;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer startTeachingYear;


}
