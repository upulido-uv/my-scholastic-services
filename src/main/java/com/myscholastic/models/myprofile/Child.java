package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Child implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String firstName;
    private String lastName;
    private String gender;
    private Grades grade;
    private IDMDate dob;
    private String classRoomTeacherFlag;
    private String seqNumber;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String classId;
}