package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Data;

@JacksonXmlRootElement(localName = "schoolInfo")
@Data
public class SchoolInfo {
	
	@JacksonXmlProperty(localName = "schoolUCN")
	@JacksonXmlElementWrapper(useWrapping = false)
	private String schoolUCN;
	
	@JacksonXmlProperty(localName = "bfRole")
	@JacksonXmlElementWrapper(useWrapping = false)
	private String bfRole;

}
