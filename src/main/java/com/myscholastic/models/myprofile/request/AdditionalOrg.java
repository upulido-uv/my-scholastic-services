package com.myscholastic.models.myprofile.request;

import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdditionalOrg {
    private String orgId;
    private String bookfairRole;
    private List<String> orgRoles;
}
