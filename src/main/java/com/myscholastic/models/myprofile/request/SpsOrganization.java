package com.myscholastic.models.myprofile.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class SpsOrganization {
    private static final String EMPTY = "";

    @JsonProperty("name")
    private String schoolName = EMPTY;
    private String address1 = EMPTY;
    private String address2 = EMPTY;
    private String city = EMPTY;
    private String state = EMPTY;
    private String country = EMPTY;
    private String zipcode = EMPTY;
    private String phone = EMPTY;
    private String reportingSchoolType = EMPTY;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY) /* no serialization */
    private String locationType = EMPTY;
}
