package com.myscholastic.models.myprofile;

import lombok.Data;

@Data
public class ClassSize {

    int classSize;

}
