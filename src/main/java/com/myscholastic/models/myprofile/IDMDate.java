package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class IDMDate implements Serializable {

    private static final long serialVersionUID = 4416136007722501665L;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer dayOfMonth;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer monthOfYear;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer year;
}
