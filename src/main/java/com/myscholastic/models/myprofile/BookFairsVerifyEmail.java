package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Data;

@JacksonXmlRootElement(localName = "chairperson")
@Data
public class BookFairsVerifyEmail {

	@JacksonXmlProperty(localName = "status")
	@JacksonXmlElementWrapper(useWrapping = false)
	private String status;

	@JacksonXmlProperty(localName = "cpFirstname")
	@JacksonXmlElementWrapper(useWrapping = false)
	private String cpFirstname;

	@JacksonXmlProperty(localName = "cpLastname")
	@JacksonXmlElementWrapper(useWrapping = false)
	private String cpLastname;
	
	@JacksonXmlProperty(localName = "schoolInfo")
	@JacksonXmlElementWrapper(useWrapping = false)
	private SchoolInfo schoolInfo;

}