package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;


@Getter
@Setter
@ToString
public class Credentials implements Serializable {

    private static final long serialVersionUID = 7269289490845487101L;

    private String userName;

    @JsonIgnore
    private String password;

}
