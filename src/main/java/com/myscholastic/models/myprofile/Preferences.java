package com.myscholastic.models.myprofile;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Preferences {

    private static final long serialVersionUID = 1L;
    private Communications communications;
}
