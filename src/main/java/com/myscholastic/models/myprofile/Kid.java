package com.myscholastic.models.myprofile;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Kid implements Serializable {
    private String email;

    private String avatar;
}
