package com.myscholastic.models.myprofile;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Identifiers implements Serializable {
    private static final long serialVersionUID = -6770451479481896093L;
    private String sps;
    private String ucn;
}
