package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

@Getter
@Setter
@ToString
public class Wallet implements Serializable {

    private static final long serialVersionUID = -2377576193821933398L;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Map<String, CreditCard> creditCards;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Collection<CreditCard> cards;

    public void setCreditCards(Map<String, CreditCard> creditCards) {
        this.creditCards = creditCards;
        if (creditCards != null && !creditCards.isEmpty()) {
            setCards(creditCards.values());
        }
    }
}
