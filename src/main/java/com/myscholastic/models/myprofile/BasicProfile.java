package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Map;

@Getter
@Setter
@ToString
public class BasicProfile implements Serializable {

    private static final long serialVersionUID = 7239533127921733145L;

    private String title;
    private String firstName;
    private String lastName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private IDMDate dob;
    private String gender;

    private String privacyPolicyAcceptedVer;
    private String privacyPolicyAcceptedTime;
    private String termsAcceptedVer;
    private String termsAcceptedTime;
    private boolean poEnabled;
    private String emailChangedOn;
    private String email;
    private Map<String, Address> addresses;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String coppaConsentFlag;
    private boolean emailStmts;
    private String[] appUserType;
    private boolean userConsentedLatestPrivPolicy;
    private boolean userConsentedLatestTerms;
    private String badgeColor;
}
