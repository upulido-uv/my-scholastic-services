package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProfileLinks {
    @JsonProperty("#my-profile")
    private String myProfile;
    @JsonProperty("#my-wishlist")
    private String wishList;
    @JsonProperty("#order-history")
    private String orderHistory;
    @JsonProperty("#subscription-downloads")
    private String subscriptionsDownloads;
    @JsonProperty("#ewallet")
    private String ewallet;
    @JsonProperty("#manage-campaigns")
    private String campaigns;
}
