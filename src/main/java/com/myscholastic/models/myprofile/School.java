package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;


@Getter
@Setter
public class School implements Serializable {

    private static final long serialVersionUID = 1L;

    private String schoolId;
    private String[] gradeRoles;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String[] specialities;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Map<String, ClassSize> classes;

    private String schoolZip;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String bcoe;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String primaryGrade;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String[] positions;
}
