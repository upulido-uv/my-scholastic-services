package com.myscholastic.models.myprofile;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BriteVerify implements Serializable {

	private static final long serialVersionUID = 1L;

	private String address;
	private String account;
	private String domain;
	private String status;
	private String connected;
	private String disposable;
	private Boolean role_address;
	private String error_code;
	private String error;
	private float duration;

}