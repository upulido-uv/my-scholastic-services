package com.myscholastic.models.myprofile;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@ToString
public class BookFairs implements Serializable {

    private static final long serialVersionUID = 1L;
    private Organization[] onlineFairOrgs;
}
