package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Contexts implements Serializable {

    private static final long serialVersionUID = -1283838301810596256L;

    private Clubs clubs;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private BookFairs bookfairs;
}
