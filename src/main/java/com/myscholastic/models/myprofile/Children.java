package com.myscholastic.models.myprofile;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

@Getter
@Setter
public class Children implements Serializable {

    private static final long serialVersionUID = 1L;
    private Map<String, Child> map;
}
