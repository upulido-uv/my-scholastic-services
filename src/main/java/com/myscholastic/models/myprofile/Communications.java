package com.myscholastic.models.myprofile;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Communications {

    private static final long serialVersionUID = 1L;
    boolean textOptIn;
}
