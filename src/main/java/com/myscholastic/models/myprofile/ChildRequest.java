package com.myscholastic.models.myprofile;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChildRequest implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String firstName;
	private String lastName;
	private String gender;
	private String grade;
	private IDMDate dob;
	private String classRoomTeacherFlag;
}
