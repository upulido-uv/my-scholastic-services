package com.myscholastic.models.myprofile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
public class Organization implements Serializable {

    private static final long serialVersionUID = -4239256672826915937L;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;

    private String orgId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String orgUcn;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String bookfairRole;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<String> orgRoles;

    @JsonProperty("default")
    private boolean isDefault;
}
