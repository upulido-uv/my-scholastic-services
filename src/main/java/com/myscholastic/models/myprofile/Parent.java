package com.myscholastic.models.myprofile;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

@Getter
@Setter

public class Parent implements Serializable {

    private static final long serialVersionUID = 4873290561024525128L;
    private Map<String, Child> children;
}
