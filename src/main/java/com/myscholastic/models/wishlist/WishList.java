package com.myscholastic.models.wishlist;

import lombok.Data;

import java.util.List;

@Data
public class WishList {
    private List<Item> items;
}
