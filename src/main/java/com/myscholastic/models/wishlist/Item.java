package com.myscholastic.models.wishlist;

import lombok.Data;

@Data
public class Item {
    private String itemId;
    private String productId;
    private String listId;
    private String createdOn;
    private String updatedOn;
}
