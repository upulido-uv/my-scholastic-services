package com.myscholastic.models.recaptcha;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReCaptchaRequestPayload {

    private String secret;
    private String response; //Captcha token received from the customer's browser (what was given realtime from google recaptcha)
}
