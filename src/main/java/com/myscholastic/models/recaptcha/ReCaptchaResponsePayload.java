package com.myscholastic.models.recaptcha;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReCaptchaResponsePayload {

    private boolean success;
    private List<String> errorCodes;
}
