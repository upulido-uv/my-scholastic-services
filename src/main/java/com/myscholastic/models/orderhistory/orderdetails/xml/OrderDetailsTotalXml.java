package com.myscholastic.models.orderhistory.orderdetails.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.myscholastic.models.subscription.Attribute;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JacksonXmlRootElement(localName = "order_details")
public class OrderDetailsTotalXml {
    @JacksonXmlProperty(localName = "attribute")
    @JacksonXmlElementWrapper( useWrapping = false )
    private Attribute[] attributes;

}
