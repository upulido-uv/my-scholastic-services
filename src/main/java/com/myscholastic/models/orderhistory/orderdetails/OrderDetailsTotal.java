package com.myscholastic.models.orderhistory.orderdetails;

import lombok.Data;

@Data
public class OrderDetailsTotal {
    private String orderId;
    private String orderDate;
    private String orderStatus;
    private String detailURL;
    private String storeLabel;
    private String itemTotal;
    private String discount;
    private String shippingTotal;
    private String taxTotal;
    private String grandTotal;
}
