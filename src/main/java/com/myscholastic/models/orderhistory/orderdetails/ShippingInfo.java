package com.myscholastic.models.orderhistory.orderdetails;

import lombok.Data;

@Data
public class ShippingInfo {
    private String name;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zip;
    private String ship_to_phone;
    private String ship_type;
}
