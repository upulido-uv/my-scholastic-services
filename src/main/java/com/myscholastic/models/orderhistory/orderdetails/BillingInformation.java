package com.myscholastic.models.orderhistory.orderdetails;

import lombok.Data;

@Data
public class BillingInformation {
    private String name;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zip;
    private String bill_to_phone;
    private String bill_to_email;
    private String card_brand;
    private String card_number;
}
