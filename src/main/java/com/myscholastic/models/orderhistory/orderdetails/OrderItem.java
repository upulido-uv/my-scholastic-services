package com.myscholastic.models.orderhistory.orderdetails;

import lombok.Data;

@Data
public class OrderItem {
    private String order_item_id;
    private String title;
    private String nickname;
    private String shipping_amt;
    private String isbn;
    private String partNum;
    private String quantity;
    private String list_price;
    private String tax_dollar_amt;
    private String total_items_price;
    private String item_adjust;
    private String item_image;
    private String product_type;
    private String tracking_href;
}
