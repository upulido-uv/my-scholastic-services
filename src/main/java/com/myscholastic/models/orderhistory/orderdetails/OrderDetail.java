package com.myscholastic.models.orderhistory.orderdetails;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.fasterxml.jackson.annotation.JsonInclude.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetail {
    private OrderDetailsTotal orderTotals;
    private ShippingInfo shippingInfo;
    private BillingInformation billingInfo;
    private OrderItem[] orderItem;

    private String imageRootPath;
    private String productUrl;
    private String readingClubUrl;

    private boolean isPcool;
    private boolean individualTracking;
    private boolean isBookFair;

    @JsonProperty("isPcool")
    public boolean isPcool() {
        return isPcool;
    }

    @JsonProperty("isBookFair")
    public boolean isBookFair() {
        return isBookFair;
    }
}
