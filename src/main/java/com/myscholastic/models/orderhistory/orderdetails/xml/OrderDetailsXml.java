package com.myscholastic.models.orderhistory.orderdetails.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

@Data
@JacksonXmlRootElement(localName = "order_details")
public class OrderDetailsXml {
    @JacksonXmlProperty( localName = "order_totals" )
    @JacksonXmlElementWrapper( useWrapping = false )
    private OrderDetailsTotalXml orderTotals;

    @JacksonXmlProperty( localName = "shipping_info" )
    @JacksonXmlElementWrapper( useWrapping = false )
    private ShippingInfoXml shippingInfo;

    @JacksonXmlProperty( localName = "billing_info" )
    @JacksonXmlElementWrapper( useWrapping = false )
    private BillingInformationXml billingInfo;

    @JacksonXmlProperty( localName = "order_item" )
    @JacksonXmlElementWrapper( useWrapping = false )
    private OrderItemXml[] orderItem;
}
