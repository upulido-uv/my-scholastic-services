package com.myscholastic.models.orderhistory;

import lombok.Data;

import java.util.List;

@Data
public class GenericOrder {
    private OrderHistory[] genericOrder;
}
