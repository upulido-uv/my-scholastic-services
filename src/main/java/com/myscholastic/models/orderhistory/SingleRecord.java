package com.myscholastic.models.orderhistory;

import lombok.Data;

@Data
public class SingleRecord {
    private String orderID;
    private String studentFirstName;
    private String studentLastName;
    private String teacherSalutation;
    private String teacherFirstName;
    private String teacherLastName;
    private String orderStatus;
}
