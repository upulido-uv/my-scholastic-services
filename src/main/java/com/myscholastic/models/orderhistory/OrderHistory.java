package com.myscholastic.models.orderhistory;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrderHistory {
    private String orderReferenceLabel;
    private String eventNumber;
    private String eventNumberDetail;
    private String estimatedDeliveryDate;
    private String ignoreCarrier;
    private String eventDate;
    private String eventTotalDollars;
    private String eventTotalPoints;
    private String eventTotalItems;
    private String eventOrderTotalView;
    private String detailParameter;
    private String trackingParameter;
    private String eventStatus;
    private String subtitle;
    private String onlyEbookStatus;
    private String displaySubtitle;
    private String displayOrderStatus;
    private String storeName;
    private String store;
    private String clickStore;
    private String dateSubmittedDisplay;
    private String dateSubmitted;
    private String statusDate;
    private String status;
    private String carrierName;
    private String carrierDeliveryMethod;
    private String hasBackOrder;
    private String numberOfBoxes;
    private String trackingNumber;
    private Records[] records;
}
