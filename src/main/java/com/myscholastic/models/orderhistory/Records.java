package com.myscholastic.models.orderhistory;

import lombok.Data;

@Data
public class Records {
    private String eventNumber;
    private String eventDate;
    private String eventTotalDollars;
    private SingleRecord[] record;
}
