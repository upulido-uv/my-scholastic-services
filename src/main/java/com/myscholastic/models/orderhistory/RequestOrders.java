package com.myscholastic.models.orderhistory;

import lombok.Data;

@Data
public class RequestOrders {
    private String spsId;
    private String bcoe;
    private String toDate;
    private String fromDate;
    private String appID;
    private String eventsPosition;
    private String noOfEvents;
    private String p;
    private String ucn;
    private String appUser;
    private String bcoe9;
    private String cookieData;
}
