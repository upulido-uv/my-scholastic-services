package com.myscholastic.models.cybersource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    private String address1;
    private String address2;
    private String address3;
    private String address4;
    private String address5;
    private String city;
    private String state;
    private String country;
    private String postalCode;
    private String poBox;
    private String email;
    private String phone;
    private String firstName;
    private String lastName;
}
