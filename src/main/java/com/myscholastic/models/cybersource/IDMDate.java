package com.myscholastic.models.cybersource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IDMDate {
//    private int dayOfMonth;
    private int monthOfYear;
    private int year;
}
