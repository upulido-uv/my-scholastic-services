package com.myscholastic.models.cybersource;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CyberSourceDto {

    // Possible value: Accept
    private String decision;

    @JsonProperty("req_card_type")
    private String cardType;

    @JsonProperty("req_payment_token")
    private String editPaymentToken;
    @JsonProperty("payment_token")
    private String paymentToken;

    //Masked Card Number
    @JsonProperty("req_card_number")
    private String cardNumber;
    @JsonProperty("req_card_expiry_date")
    private String expiryDate; /* 01-2019 */

    @JsonProperty("req_merchant_defined_data12")
    private String userId;
    @JsonProperty("req_merchant_defined_data19")
    private String subId; /* subscriptionId */
    @JsonProperty("req_merchant_defined_data20")
    private String otcParam;
    @JsonProperty("req_merchant_defined_data21")
    private String defaultCard;
    @JsonProperty("req_merchant_defined_data23")
    private String saveToProfile;
    @JsonProperty("req_merchant_defined_data25")
    private String cardAction;
    @JsonProperty("req_merchant_defined_data27")
    private String ext;
    // Possible value: EWALLET
    @JsonProperty("req_merchant_defined_data28")
    private String ewallet;

    @JsonProperty("req_bill_to_forename")
    private String firstName;
    @JsonProperty("req_bill_to_surname")
    private String lastName;
    @JsonProperty("req_bill_to_address_line1")
    private String address1;
    @JsonProperty("req_bill_to_address_line2")
    private String address2;
    @JsonProperty("req_bill_to_address_city")
    private String city;
    @JsonProperty("req_bill_to_address_postal_code")
    private String postalCode;
    @JsonProperty("req_bill_to_address_state")
    private String state;
    @JsonProperty("req_bill_to_address_country")
    private String country;

    @JsonProperty("req_bill_to_phone")
    private String phone;
}
