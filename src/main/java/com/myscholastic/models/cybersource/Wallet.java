package com.myscholastic.models.cybersource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Wallet {
    private String cbsToken;
    private Address billingAddress;
    private String source;
    private String cardType;
    private String maskedCardNumber;
    private IDMDate expiration;
    private boolean primary;
    private String phoneNumber;
    private String phoneExtension;
}
