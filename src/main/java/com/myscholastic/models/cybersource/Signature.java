package com.myscholastic.models.cybersource;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

/**
 * Class represents a HMAC (signature field) and its corresponding plain text.
 */
@Data
@Builder
public class Signature {
    private String signature;
    private String accessKey;
    private String currency;
    private String locale;
    private String profileId;
    private String paymentMethod;
    private String merchant_defined_data35;
    private String signed_field_names;
    private String unsigned_field_names;
    private String actionUrl;
    private String reference_number;
    private String transaction_type;
    private String transaction_uuid;
    private String country;
    private String signed_date_time;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String amount;
    private String merchant_defined_data24;
}
