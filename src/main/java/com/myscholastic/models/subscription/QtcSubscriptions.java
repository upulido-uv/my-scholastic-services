package com.myscholastic.models.subscription;

import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
public class QtcSubscriptions {
    List<QtcSubscription> subscriptions = Collections.emptyList();
}
