package com.myscholastic.models.subscription;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.util.List;

@Slf4j
@Data
@JsonIgnoreProperties(value={"orderDate", "expirationDate", "title", "term", "storeSource"}, allowSetters=true)
public class QtcSubscription {
    private String subscriptionNum;
    private String subscriptionId;
    private String ucn;
    private String status;
    private String storeLabel;
    private String subscriptionTerm;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = ToStringSerializer.class)
    private LocalDate submittedDate;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = ToStringSerializer.class)
    private LocalDate renewalDate;

    private String orderId;
    private String storeId;
    private String creditCardStatus;
    private List<BillingHistory> billingHistory;
}
