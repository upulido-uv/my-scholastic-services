package com.myscholastic.models.subscription.orderdetail;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.myscholastic.models.subscription.Attribute;
import com.myscholastic.utils.DateFormatUtil;
import lombok.Data;

import java.text.ParseException;

@Data
@JacksonXmlRootElement( localName = "order_details" )
public class SubscriptionDetails {

    @JacksonXmlProperty( localName = "attribute", isAttribute = false )
    @JacksonXmlElementWrapper( useWrapping = false )
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Attribute[] attributes;

    private String subscriptionAccessStatus;
    private String renewalDate;
    private String subscriptionTerm;

    public void setAttributes( Attribute[] attributes ) throws ParseException {
        for ( Attribute attr : attributes ) {
            String propertyName = attr.getName();
            String propertyValue = attr.getValue();
            switch (propertyName) {
                case "subscription_access_status":
                    this.setSubscriptionAccessStatus(propertyValue);
                    break;
                case "renewal_date":
                    // this field would always be null because there is a bug from EntitlementService
                    // , and nobody seemed to complain about this issue..
                    this.setRenewalDate(DateFormatUtil.formatDate(propertyValue));
                    break;
                case "subscription_term":
                    this.setSubscriptionTerm(propertyValue);
                    break;
            }
        }
    }
}
