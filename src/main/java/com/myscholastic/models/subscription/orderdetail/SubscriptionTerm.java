package com.myscholastic.models.subscription.orderdetail;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.myscholastic.utils.DateFormatUtil;
import lombok.Data;

@Data
@JacksonXmlRootElement(localName = "subscription_term")
public class SubscriptionTerm {

    @JacksonXmlProperty(localName = "term_num", isAttribute = true)
    private String termNum;

    @JacksonXmlProperty(localName = "start_date", isAttribute = true)
    private String startDate;

    @JacksonXmlProperty(localName = "renewal_date", isAttribute = true)
    private String renewalDate;

    @JacksonXmlProperty(localName = "amount", isAttribute = true)
    private String amount;

    public void setStartDate(String startDate) {
        this.startDate = DateFormatUtil.formatDate(startDate);
    }

    public void setRenewalDate(String renewalDate) {
        this.renewalDate = DateFormatUtil.formatDate(renewalDate);
    }
}