package com.myscholastic.models.subscription.orderdetail;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.myscholastic.models.subscription.Attribute;
import lombok.Data;

@Data
@JacksonXmlRootElement( localName = "billing_info" )
public class BillingInfo {

    @JacksonXmlProperty(localName = "attribute")
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Attribute[] attributes;

    @JacksonXmlProperty(isAttribute = true)
    private String editBilling;

    public void setAttributes(Attribute[] attributes) {
        for (Attribute attr : attributes) {
            String propertyName = attr.getName();
            String propertyValue = attr.getValue();
            if (propertyName.equalsIgnoreCase("edit_billing")) {
                this.setEditBilling(propertyValue);
            }
        }
    }
}