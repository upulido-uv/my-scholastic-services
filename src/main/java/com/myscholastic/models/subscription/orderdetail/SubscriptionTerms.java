package com.myscholastic.models.subscription.orderdetail;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

@Data
@JacksonXmlRootElement( localName = "subscription_terms" )
public class SubscriptionTerms {
    @JacksonXmlProperty(localName = "subscription_term")
    @JacksonXmlElementWrapper(useWrapping = false)
    private SubscriptionTerm[] subscriptionTerm;
}