package com.myscholastic.models.subscription.orderdetail;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

@Data
@JacksonXmlRootElement(localName = "order_details")
public class OrderDetails {

    @JacksonXmlProperty(localName = "order_totals")
    @JacksonXmlElementWrapper(useWrapping = false)
    private OrderTotals orderTotals;

    @JacksonXmlProperty(localName = "billing_info")
    @JacksonXmlElementWrapper(useWrapping = false)
    private BillingInfo billingInfo;

    @JacksonXmlProperty(localName = "subscription_details")
    @JacksonXmlElementWrapper(useWrapping = false)
    private SubscriptionDetails subscriptionDetails;

    @JacksonXmlProperty(localName = "subscription_terms")
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY) /* deserialization only */
    private SubscriptionTerms subscriptionTermsXml;

    private SubscriptionTerm[] subscriptionTerms;

    public void setSubscriptionTermsXml(SubscriptionTerms subscriptionTerms) {
        this.subscriptionTerms = subscriptionTerms.getSubscriptionTerm();
    }
}
