package com.myscholastic.models.subscription.orderdetail;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.myscholastic.models.subscription.Attribute;
import com.myscholastic.utils.DateFormatUtil;
import lombok.Data;

import java.text.ParseException;

@JacksonXmlRootElement(localName = "order_totals")
@Data
public class OrderTotals {

    @JacksonXmlProperty(localName = "attribute")
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY) /* deserialization only */
    private Attribute[] attributes;

    private String orderId;
    private String subscriptionId;
    private String orderDate;
    private String orderStatus;
    private String detailURL;
    private String storeLabel;

    public void setAttributes(Attribute[] attributes) {
        for (Attribute attr : attributes) {
            String propertyName = attr.getName();
            String propertyValue = attr.getValue();
            switch (propertyName) {
                case "order_id":
                    this.setOrderId(propertyValue);
                    break;
                case "sub_id":
                    this.setSubscriptionId(propertyValue);
                    break;
                case "order_date":
                    this.setOrderDate(DateFormatUtil.formatDate(propertyValue));
                    break;
                case "order_status":
                    this.setOrderStatus(propertyValue);
                    break;
                case "detail_url":
                    this.setDetailURL(propertyValue);
                    break;
                case "store_label":
                    this.setStoreLabel(propertyValue);
                    break;
            }
        }
    }

}