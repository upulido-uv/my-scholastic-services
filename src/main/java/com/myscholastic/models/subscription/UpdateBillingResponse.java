package com.myscholastic.models.subscription;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateBillingResponse {
    private String isSuccess;
    private String statusCode;
    private String message;
}
