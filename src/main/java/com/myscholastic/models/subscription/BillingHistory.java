package com.myscholastic.models.subscription;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class BillingHistory {
    private String termNum;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = ToStringSerializer.class)
    private LocalDate submittedDate;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = ToStringSerializer.class)
    private LocalDate renewalDate;

    private String paymentMethod;
    private String ccType;
    private String ccMask;
    private BigDecimal price;
    private BigDecimal tax;
    private BigDecimal totalAmount;
}
