package com.myscholastic.models.subscription;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QtcUpdateBillingRequest {
    private String subscriptionId;
    private String subscriptionNum;
    private String orderId;
    private String ucn;
    private String cybersourceToken;
    private String cardNumberMasked;
    private int cardExpirationMonth;
    private int cardExpirationYear;
    private String cardType;
    private String billToFirstName;
    private String billToLastName;
    private String csTrxNumber;
}
