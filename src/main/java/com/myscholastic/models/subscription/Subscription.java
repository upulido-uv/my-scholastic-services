package com.myscholastic.models.subscription;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.myscholastic.utils.DateFormatUtil;
import lombok.Data;

import java.util.Arrays;

@JacksonXmlRootElement(localName = "order")
@Data
public class Subscription {
    @JacksonXmlProperty(isAttribute = true, localName = "store_id")
    private String storeId;

    @JacksonXmlProperty(localName = "attribute")
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY) /* deserialization only */
    private Attribute[] attributes;

    private String inSubscriptionHistory;
    private String supressLink;
    private String editBillingButton;
    private String editBillingMessage;
    private String subscriptionTerm;
    private String renewalDate;
    private String subscriptionAccessStatus;
    private String statusMessage;
    private String storeLabel;
    private String orderId;
    private String subscriptionId;
    private String status;
    private String submittedDate;
    private String sortDate;
    private String detailURL;

    public void setAttributes(Attribute[] attributes) {
        Arrays.stream(attributes).forEach(this::setPropertyFromAttr);
    }

    private void setPropertyFromAttr(Attribute attr) {
        String propertyName = attr.getName();
        String propertyValue = attr.getValue();
        switch (propertyName) {
            case "in_subscription_history":
                this.setInSubscriptionHistory(propertyValue); break;
            case "suppress_link":
                this.setSupressLink(propertyValue); break;
            case "edit_billing_button":
                this.setEditBillingButton(propertyValue); break;
            case "edit_billing_message":
                this.setEditBillingMessage(propertyValue); break;
            case "subscription_term":
                this.setSubscriptionTerm(propertyValue); break;
            case "renewal_date":
                this.setRenewalDate(DateFormatUtil.formatDate(propertyValue)); break;
            case "subscription_access_status":
                this.setSubscriptionAccessStatus(propertyValue); break;
            case "status_message":
                this.setStatusMessage(propertyValue); break;
            case "store_label":
                this.setStoreLabel(propertyValue); break;
            case "order_id":
                this.setOrderId(propertyValue); break;
            case "sub_id":
                this.setSubscriptionId(propertyValue); break;
            case "status":
                this.setStatus(propertyValue); break;
            case "submitted_date":
                this.setSubmittedDate(DateFormatUtil.formatDate(propertyValue)); break;
            case "sort_date":
                this.setSortDate(propertyValue); break;
            case "detail_url":
                this.setDetailURL(propertyValue); break;
        }
    }
}
