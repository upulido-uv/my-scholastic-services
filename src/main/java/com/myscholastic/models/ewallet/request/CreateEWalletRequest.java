package com.myscholastic.models.ewallet.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateEWalletRequest {

   private String customerProfileId;

}
