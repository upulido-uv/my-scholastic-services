package com.myscholastic.models.ewallet.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.myscholastic.models.ewallet.FundingSource;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class CreateVoucherRequest {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String name;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String type;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String fundType;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String source;

    private BigDecimal amount;

    private CreateVoucherRequestAttributes attributes;
    private FundingSource fundingSource;

}
