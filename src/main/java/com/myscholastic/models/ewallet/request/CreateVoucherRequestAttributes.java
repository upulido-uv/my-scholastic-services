package com.myscholastic.models.ewallet.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateVoucherRequestAttributes {

    private String studentFirstName;
    private String studentLastName;
    private String teacherFirstName;
    private String teacherLastName;
    private String parentFirstName;
    private String parentLastName;
    private String grade;
    private String displayGrade;
    private String displayGradeOrdinal;
    private String fairId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String notes;

}