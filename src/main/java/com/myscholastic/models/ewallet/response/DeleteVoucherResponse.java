package com.myscholastic.models.ewallet.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeleteVoucherResponse {
    private String message;
}
