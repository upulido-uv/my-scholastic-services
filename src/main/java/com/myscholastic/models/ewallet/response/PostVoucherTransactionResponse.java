package com.myscholastic.models.ewallet.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostVoucherTransactionResponse {
    private String message;
    long id;
}
