package com.myscholastic.models.ewallet;


import lombok.Data;

@Data
public class Attribute {
    private String studentFirstName;
    private String studentLastName;
    private String teacherFirstName;
    private String teacherLastName;
    private String parentFirstName;
    private String parentLastName;
    private String grade;
    private String notes;
    private String displayGrade;
    private String displayGradeOrdinal;
    private String fairId;
}
