package com.myscholastic.models.ewallet;

import lombok.Data;

@Data
public class EWalletId {
    private long id;
    private String createdOn;
}
