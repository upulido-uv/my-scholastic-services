package com.myscholastic.models.ewallet;

import lombok.Builder;
import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
@Builder
public class FairVouchers {
    private Long id;
    private String name;
    private String url;
    private String startDate;
    private String endDate;
    private String status;
    private String walletFlag;
    private boolean expandedView;
    private Boolean moved17DaysFlag;
    @Builder.Default
    private List<Voucher> vouchers = Collections.emptyList();
}