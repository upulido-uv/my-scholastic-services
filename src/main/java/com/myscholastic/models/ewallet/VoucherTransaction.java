package com.myscholastic.models.ewallet;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class VoucherTransaction {
    private Integer id;
    private Integer voucherId;
    private BigDecimal amount;
    private String timestamp;
    private String reference;
    private String type;
    private String createdOn;
    private String updatedOn;
}
