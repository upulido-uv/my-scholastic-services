package com.myscholastic.models.ewallet;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
@AllArgsConstructor
public class FairIdList {
    private Set<Long> ids;
}
