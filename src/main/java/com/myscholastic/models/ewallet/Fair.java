package com.myscholastic.models.ewallet;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Fair {
    private Long id;
    private String name;
    private String url;
    private String startDate;
    private String endDate;
    private String status;
    private String eWalletFlag;
    private String error;
    private Boolean moved17DaysFlag;
}