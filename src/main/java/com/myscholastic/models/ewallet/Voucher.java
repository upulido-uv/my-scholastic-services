package com.myscholastic.models.ewallet;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@Data
public class Voucher {
    private Integer id;
    private Integer walletId;
    private String type;
    private String event;
    private String name;
    private String owner;
    private String fundSource;
    private String source;
    private String fundType;
    private String authorization;
    private BigDecimal amount;
    private BigDecimal startingAmount;
    private String meta;
    private String key;
    private Attribute attributes;
    private FundingSource fundingSource;
    private String createdOn;
    private String updatedOn;
    private List<VoucherTransaction> transactions = Collections.emptyList();
    private String status;
}