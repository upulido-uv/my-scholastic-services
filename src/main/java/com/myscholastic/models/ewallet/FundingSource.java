package com.myscholastic.models.ewallet;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
public class FundingSource {

    private String subscriptionId;
    private String maskedPan;
    private String cardType;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String fundSource;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String displayCard;
}
