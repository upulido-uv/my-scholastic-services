package com.myscholastic.models.ewallet;

import lombok.Data;

import java.util.List;

@Data
public class FairVouchersJson {
    private List<FairVouchers> fairVouchersList;
}
