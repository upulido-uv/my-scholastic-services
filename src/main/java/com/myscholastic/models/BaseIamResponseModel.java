package com.myscholastic.models;

import lombok.Data;

@Data
public class BaseIamResponseModel {

    private String message;
    private Long id;

}
