package com.myscholastic.models.authentication.response;

import com.myscholastic.models.myprofile.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponse {

   private String status;
   private User user;

}
