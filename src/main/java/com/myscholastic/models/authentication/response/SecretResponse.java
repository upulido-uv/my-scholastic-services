package com.myscholastic.models.authentication.response;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SecretResponse {

   private Integer statusCode;
   private String message;
   private Map<String, String> keys;
   private String currentKey;

   public SecretResponse(SecretResponse secretResponse) {
      this.statusCode = secretResponse.getStatusCode();
      this.message = secretResponse.getMessage();
      this.currentKey = secretResponse.getCurrentKey();

      Map<String, String> keys = new HashMap();

      for (Map.Entry<String, String> entry : secretResponse.getKeys().entrySet()) {
         keys.put(entry.getKey(), entry.getValue());
      }

      this.keys = keys;
   }

   @JsonAnySetter
   public void setKeys(String key, String value) {
      this.keys.put(key, value);
   }

}
