package com.myscholastic.models.egift;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionsRequest {
    private Instant beginDate;
    private Instant endDate;
    private Integer maxResults;
    private Integer offset;
    private String tenderType;
}
