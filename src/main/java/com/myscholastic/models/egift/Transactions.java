package com.myscholastic.models.egift;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Transactions {
    private BigDecimal amount;
    private String description;
    private String tenderType;
    private Timestamp timestamp;
}
