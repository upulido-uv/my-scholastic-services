package com.myscholastic.models.egift;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.net.URL;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Campaign {
    private Boolean active;
    private Boolean disabled;
    private Boolean expired;
    private String campaignId;
    private URL contributorsLink;
    private String created;
    private String description;
    private String email;
    private String end;
    private String imagePath;
    private Name name;
    private BigDecimal target;
    private String title;
}
