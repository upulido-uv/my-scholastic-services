package com.myscholastic.models.egift;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CampaignRequest {

    private Boolean disabled;
    private String description;
    private String email;
    private String end;
    private String imagePath;
    private Name name;
    private String spsid;
    private String ucn;
    private BigDecimal target;
    private String title;
}
