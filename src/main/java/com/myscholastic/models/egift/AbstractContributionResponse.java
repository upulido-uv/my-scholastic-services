package com.myscholastic.models.egift;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
abstract class AbstractContributionResponse {
    private Integer count;
    private BigDecimal totalAmount;
    private Campaign campaign;
}
