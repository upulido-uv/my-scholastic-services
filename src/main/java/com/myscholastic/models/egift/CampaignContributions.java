package com.myscholastic.models.egift;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CampaignContributions {
    private Integer count;
    private BigDecimal totalAmount;
    private List<Contribution> contributions;
    private Campaign campaign;
}
