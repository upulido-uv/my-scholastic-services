package com.myscholastic.models.egift;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BalancesResponse {
    private BigDecimal availableAmount;
    private BigDecimal balance;
    private String label;
    private BigDecimal reservedAmount;
    private String tenderType;
}
