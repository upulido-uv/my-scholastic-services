package com.myscholastic.models.egift;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Contribution {
    private String contributionId;
    private BigDecimal amount;
    private String email;
    private String message;
    private Name name;
    private String created;
    private Boolean anonymize;
}
