package com.myscholastic.models.egift;

import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContributionsResponse extends AbstractContributionResponse {

    @Builder
    public ContributionsResponse(int count, BigDecimal totalAmount, Campaign campaign, List<Contribution> contributions) {
        super(count, totalAmount, campaign);
        this.contributions = contributions;
    }

    private List<Contribution> contributions;

}
