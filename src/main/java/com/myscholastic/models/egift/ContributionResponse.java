package com.myscholastic.models.egift;

import lombok.*;

import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContributionResponse extends AbstractContributionResponse {

    private Contribution contribution;

    @Builder
    public ContributionResponse(int count, BigDecimal totalAmount, Campaign campaign, Contribution contribution) {
        super(count, totalAmount, campaign);
        this.contribution = contribution;
    }

}

