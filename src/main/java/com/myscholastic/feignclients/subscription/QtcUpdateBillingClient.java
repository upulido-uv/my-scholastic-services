package com.myscholastic.feignclients.subscription;

import com.myscholastic.configurations.AppConfig;
import com.myscholastic.models.BaseResponse;
import com.myscholastic.models.subscription.QtcUpdateBillingRequest;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "QtcUpdateBillingClient", url = "${api-gateway.updateBillingRoute}", configuration = QtcUpdateBillingClient.Config.class)
public interface QtcUpdateBillingClient {

    /**
     * Aka Update Billing, Cybersource Postback
     */
    @PostMapping(value = "/", produces = "application/json")
    BaseResponse updateBilling(@RequestBody QtcUpdateBillingRequest requestBody);

    class Config {
        @Autowired
        private AppConfig appConfig;

        @Bean
        public RequestInterceptor TestKongClientInterceptor() {
            return template -> template.header("Authorization", "Bearer " + appConfig.getApiGatewayConfig().getUpdateBillingBearerToken());
        }
    }
}