package com.myscholastic.feignclients.subscription;

import com.myscholastic.configurations.AppConfig;
import com.myscholastic.models.subscription.QtcSubscriptions;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "QtcSubscriptionHistoryClient", url = "${api-gateway.subscriptionHistoryRoute}", configuration = QtcSubscriptionHistoryClient.Config.class)
public interface QtcSubscriptionHistoryClient {

    /**
     * Aka SubscriptionHistory
     */
    @GetMapping(value = "/?customerUCN={customerUCN}", produces = "application/json")
    QtcSubscriptions getQtcSubscriptions(@PathVariable("customerUCN") String customerUCN);

    class Config {
        @Autowired
        private AppConfig appConfig;

        @Bean
        public RequestInterceptor TestKongClientInterceptor() {
            return template -> template.header("Authorization", "Bearer " + appConfig.getApiGatewayConfig().getSubscriptionHistoryBearerToken());
        }
    }
}