package com.myscholastic.feignclients.subscription;

import com.myscholastic.configurations.AppConfig;
import com.myscholastic.models.subscription.UpdateBillingResponse;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;

// Had to separate this out because Entitlement Service doesn't handle response payload consistently.
// Need some serious refactor for above service
@FeignClient(name = "UpdateBillingClient", url = "${api-gateway.subHistoryRoute}", configuration = UpdateBillingClient.Config.class)
public interface UpdateBillingClient {

    @PutMapping(value = "/subscriptions/updateStatus/{subId}", produces = "application/json")
    UpdateBillingResponse updateBilling(@RequestHeader("Cookie") String cookie, @PathVariable("subId") String subId);

    class Config {

        @Autowired
        AppConfig appConfig;

        @Bean
        public RequestInterceptor setHeader() {
            if(appConfig.getApiGatewayConfig().getSubHistoryBearerToken() != null)
                return template -> template.header("Authorization", "Bearer " + appConfig.getApiGatewayConfig().getSubHistoryBearerToken());
            return template -> template.headers();
        }
    }
}
