package com.myscholastic.feignclients.subscription;

import com.myscholastic.configurations.feign.SubscriptionHistoryConfig;
import com.myscholastic.models.subscription.Subscriptions;
import com.myscholastic.models.subscription.UpdateBillingResponse;
import com.myscholastic.models.subscription.orderdetail.OrderDetails;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "SubscriptionHistoryClient", url = "${api-gateway.subHistoryRoute}", configuration = SubscriptionHistoryConfig.class)
public interface SubscriptionHistoryClient {

    /**
     * Aka SubscriptionHistory
     */
    @GetMapping(value = "/subscriptions")
    Subscriptions getSubscriptions(@RequestHeader("Cookie") String cookie);

    /**
     * Aka SubscriptionDetail
     * @return A XML String
     */
    @GetMapping(value = "/subscriptions/{subId}/{orderId}", produces = "application/json")
    OrderDetails getSubscriptionByOrderId(@PathVariable("subId") String subId,
                                          @PathVariable("orderId") String orderId,
                                          @RequestHeader("Cookie") String cookie);
}