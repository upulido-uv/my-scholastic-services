package com.myscholastic.feignclients.digitaldownloads;

import com.myscholastic.models.digitaldownloads.DigitalDownloads;
import feign.Logger;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "DigitalDownloadsClient", url = "${api-gateway.digitalDownloadsRoute}", configuration = DigitalDownloadsClient.Config.class)
public interface DigitalDownloadsClient {

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.TEXT_XML_VALUE)
    DigitalDownloads getDigitalDownloadsList(@RequestHeader("Cookie") String cookie);

    class Config {
        @Bean
        Logger.Level feignLoggerLevel() {
            return Logger.Level.FULL;
        }
    }
}
