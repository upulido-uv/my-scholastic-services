package com.myscholastic.feignclients.recaptcha;

import com.myscholastic.models.recaptcha.ReCaptchaRequestPayload;
import com.myscholastic.models.recaptcha.ReCaptchaResponsePayload;
import feign.Logger;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "ReCaptchaClient", url = "${recaptcha.url}", configuration = ReCaptchaClient.Config.class)
public interface ReCaptchaClient {

    @PostMapping(value = "", produces = "application/json")
    ReCaptchaResponsePayload validateRecaptcha(@RequestBody ReCaptchaRequestPayload recaptchaRequestPayload);

    class Config {

        @Bean
        Logger.Level feignLoggerLevel() {
            return Logger.Level.FULL;
        }

    }
}
