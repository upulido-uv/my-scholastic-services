package com.myscholastic.feignclients.orderhistory;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myscholastic.configurations.AppConfig;
import com.myscholastic.models.orderhistory.GenericOrder;
import feign.Logger;
import feign.RequestInterceptor;
import feign.codec.Decoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@FeignClient(name = "OrderHistoryClient", url = "${api-gateway.orderHistoryServiceRoute}", configuration = OrderHistoryClient.Config.class)
public interface OrderHistoryClient {

    @GetMapping(path = "/history?eventsPosition=0&noOfEvents=0")
    GenericOrder getOrderHistory(@RequestParam String spsId,
                                 @RequestParam String toDate,
                                 @RequestParam String fromDate,
                                 @RequestParam String ucn,
                                 @RequestParam String bcoe9);

    @GetMapping(path = "/detail", produces = "text/html")
    String getOrderDetail(@RequestParam String spsId,
                          @RequestParam String storeId,
                          @RequestParam String eventsNo,
                          @RequestParam String eventSubmittedYear,
                          @RequestParam String ucn,
                          @RequestParam String bcoe9);

    class Config {

        @Autowired
        AppConfig appConfig;
        // Response data is json and yet Content Type is 'text/html'...
        @Bean
        public Decoder jsonDecoder() {
            ObjectMapper objectMapper = new ObjectMapper();
            // AEM Generic Order model doesn't include all attributes..
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

            MappingJackson2HttpMessageConverter jacksonConverter = new MappingJackson2HttpMessageConverter(objectMapper);
            List<MediaType> mediaTypes = new ArrayList<>();
            mediaTypes.add(MediaType.TEXT_HTML);
            jacksonConverter.setSupportedMediaTypes(mediaTypes);

            ObjectFactory<HttpMessageConverters> objectFactory = () -> new HttpMessageConverters(jacksonConverter);
            return new SpringDecoder(objectFactory);
        }

        @Bean
        public RequestInterceptor setHeader() {
            return template -> template.header("Authorization", "Bearer " + appConfig.getApiGatewayConfig().getOrderHistoryServiceBearerToken());
        }

        @Bean
        Logger.Level feignLoggerLevel() {
            return Logger.Level.FULL;
        }
    }
}
