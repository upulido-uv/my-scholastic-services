package com.myscholastic.feignclients.egift;

import com.myscholastic.configurations.AppConfig;
import com.myscholastic.models.egift.*;
import feign.Logger;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "EGfitServiceClient", url = "${api-gateway.egiftServiceRoute}", configuration = EGiftClient.Config.class)
public interface EGiftClient {

    @GetMapping("/campaigns")
    List<Campaign> getCampaigns(@RequestParam String spsid);

    @GetMapping("/campaigns/{campaignId}")
    Campaign getCampaignById(@PathVariable String campaignId);

    @PostMapping("/campaigns")
    @ResponseStatus(HttpStatus.CREATED)
    Campaign postCampaign(@RequestBody CampaignRequest campaignRequest);

    @PatchMapping("/campaigns/{campaignId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void putCampaign(@PathVariable String campaignId, @RequestBody CampaignRequest campaignRequest);

    @GetMapping("/campaigns/{campaignId}/contributions")
    ContributionsResponse getContributionsByCampaign(@PathVariable String campaignId);

    @GetMapping("/campaigns/{campaignId}/contributions/{contributionId}")
    Contribution getContribution(@PathVariable String campaignId, @PathVariable String contributionId);

    @PostMapping("/campaigns/{campaignId}/contributions")
    ContributionResponse createContribution(@PathVariable String campaignId, @RequestBody ContributionRequest contributionReq);

    @PatchMapping("/campaigns/{campaignId}/contributions/{contributionId}")
    Contribution updateContribution(@PathVariable String campaignId, @PathVariable String contributionId, @RequestBody ContributionRequest contributionRequest);

    // TODO => TO BE DELETED
    class Config {

        @Autowired
        private AppConfig appConfig;

        @Bean
        public RequestInterceptor iamRequestInterceptor() {
            return template -> template.header("Authorization", "Bearer " + appConfig.getApiGatewayConfig().getEgiftServiceBearerToken());
        }

        @Bean
        Logger.Level feignLoggerLevel() {
            return Logger.Level.FULL;
        }

    }
}