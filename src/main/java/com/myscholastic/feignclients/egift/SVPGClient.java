package com.myscholastic.feignclients.egift;

import com.myscholastic.configurations.AppConfig;
import com.myscholastic.models.egift.*;
import feign.Logger;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.SpringQueryMap;

import java.util.List;

@FeignClient(name = "SVPGServiceClient", url = "${api-gateway.svpgServiceRoute}", configuration = SVPGClient.Config.class)
public interface SVPGClient {

    @GetMapping("/api/users/{id}/transactions")
    List<Transactions> getTransactionsHistory(@PathVariable String id, @SpringQueryMap TransactionsRequest request);

    @GetMapping("/api/users/{id}/balances")
    List<BalancesResponse> getBalances(@PathVariable String id);

    class Config {

        @Autowired
        private AppConfig appConfig;

        @Bean
        public RequestInterceptor iamRequestInterceptor() {
            return template -> template.header("Authorization", "Bearer " + appConfig.getApiGatewayConfig().getSvpgServiceBearerToken());
        }

        @Bean
        Logger.Level feignLoggerLevel() {
            return Logger.Level.FULL;
        }

    }
}


