package com.myscholastic.feignclients.idam;

import com.myscholastic.configurations.AppConfig;
import com.myscholastic.models.myprofile.request.SpsOrganization;
import com.myscholastic.models.myprofile.response.SchoolInfoResponse;
import com.myscholastic.models.myprofile.response.SchoolResponse;
import com.myscholastic.models.myprofile.response.SpsOrganizationResponse;
import feign.Logger;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "IamSpsOrgClient", url = "${api-gateway.iamSpsOrgRoute}", configuration = IamSpsOrgClient.IamSpsOrgConfig.class)
public interface IamSpsOrgClient {

    @GetMapping(value = "?clientId=NonSPSClient&spsId={spsId}", consumes = "application/json")
	List<SchoolInfoResponse> getschoolInfo(@PathVariable("spsId") String spsId);

    @GetMapping(value = "?clientId=NonSPSClient&zipcode={zipcode}&country={country}&groupType={groupType}", consumes = "application/json")
	List<SchoolResponse> getSchools(@PathVariable("zipcode") String zipcode, @PathVariable("country") String country, @PathVariable("groupType") String groupType);

    @PostMapping(value = "/{locationType}/org?clientId=NonSPSClient", consumes = "application/json", produces = "application/json")
    SpsOrganizationResponse submitSchool(@PathVariable String locationType, @RequestBody SpsOrganization spsOrganization);

    class IamSpsOrgConfig {

        @Autowired
        private AppConfig appConfig;

        @Bean
        public RequestInterceptor setHeader() {
            return template -> template.header("Authorization", "Bearer " + appConfig.getApiGatewayConfig().getIamSpsOrgBearerToken());
        }
        @Bean
        Logger.Level feignLoggerLevel() {
            return Logger.Level.FULL;
        }

    }
}

