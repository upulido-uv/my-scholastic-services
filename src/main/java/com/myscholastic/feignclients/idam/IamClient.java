package com.myscholastic.feignclients.idam;

import com.myscholastic.configurations.feign.IamClientConfig;
import com.myscholastic.models.authentication.request.LoginRequest;
import com.myscholastic.models.authentication.response.LoginResponse;
import com.myscholastic.models.myprofile.*;
import com.myscholastic.models.myprofile.request.AdditionalOrg;
import com.myscholastic.models.myprofile.response.AdditionalOrgResponse;
import com.myscholastic.models.myprofile.response.AppUserTypeResponse;
import com.myscholastic.models.myprofile.response.LookupListReponse;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "IamClient", url = "${api-gateway.iamRoute}", configuration = IamClientConfig.class)
public interface IamClient {

    @PostMapping(value = "/authentication/?sections=identifiers,basicprofile", consumes = "application/json")
    LoginResponse authenticate(@RequestBody LoginRequest loginRequest);

    @GetMapping(value = "/users/{userId}", consumes = "application/json")
    User getUser(@PathVariable("userId") String userId);

    @GetMapping(value = "/users/{userId}?sections={sections}", consumes = "application/json")
    User getUserWithSections(@PathVariable("userId") String userId, @PathVariable("sections") String sections);

    @GetMapping(value = "/users/{userId}/contexts/bookfairs/online-fair-orgs", consumes = "application/json")
    List<IdamBookfairOrganizations> getUserBFOrgUcns(@PathVariable("userId") String userId);

    @PutMapping(value = "/users/{userId}/contexts/bookfairs/online-fair-orgs/{schoolUcn}", consumes = "application/json")
    ResponseEntity<Void> setUserBFOrgUcn(@PathVariable String userId, @PathVariable String schoolUcn, @RequestBody String Body);

    @DeleteMapping(value = "/users/{userId}/contexts/bookfairs/online-fair-orgs/{schoolUcn}", consumes = "application/json")
    ResponseEntity<Void> deleteUserBFOrgUcn(@PathVariable("userId") String userId, @PathVariable("schoolUcn") String schoolUcn);

    @PutMapping(value = "/users/{userId}", consumes = "application/json")
    User updateUserAppType(@PathVariable("userId") String userId, @RequestBody String body);

    @PutMapping(value = "/users/{userId}/additionalorgs/{index}", consumes = "application/json")
    User updateUserChangeOrganization(@PathVariable("userId") String userId, @PathVariable("index") String index, @RequestBody String body);

    @PutMapping(value = "/users/{userId}/profile", consumes = "application/json")
    BasicProfile updateUser(@PathVariable("userId") String userId, @RequestBody String body);

    @PostMapping(value = "/users/{userId}/addresses", consumes = "application/json")
	Address addAddress(@PathVariable("userId") String userId, @RequestBody Address requestAddress);
    
    @PutMapping(value = "/users/{userId}/addresses/{index}", consumes = "application/json")
    Address updateAddress(@PathVariable("userId") String userId, @PathVariable("index") String index, @RequestBody String body);
    
    @PostMapping(value = "/users/{userId}/personas/parent/children", consumes = "application/json")
	Child addChild(@PathVariable("userId") String userId, ChildRequest child);

    @PutMapping(value = "/users/{userId}/personas/parent/children/{index}", consumes = "application/json")
    Child updateChild(@PathVariable("userId") String userId, @PathVariable("index") String index, @RequestBody String body);

    @PutMapping(value = "/users/{userId}/credentials", consumes = "application/json")
    Credentials updateCredentials(@PathVariable("userId") String userId, @RequestBody String body);

    @PutMapping(value = "/users/{userId}/personas/educator", consumes = "application/json")
    Educator updateEducator(@PathVariable("userId") String userId, @RequestBody String body);

    @PutMapping(value = "/users/{userId}/change-school", consumes = "application/json")
    School updateSchool(@PathVariable("userId") String userId, @RequestBody String body);

    @PutMapping(value = "/users/{userId}/contexts", consumes = "application/json")
    Contexts updateContexts(@PathVariable("userId") String userId, @RequestBody String body);
    
    @DeleteMapping(value = "/users/{userId}/wallet/credit-cards/{cardId}", consumes = "application/json")
    Wallet deleteCreditCard(@PathVariable("userId") String userId, @PathVariable("cardId") String cardId);

    @DeleteMapping(value = "/users/{userId}/addresses/{index}")
    ResponseEntity<Void> deleteAddress(@PathVariable("userId") String userId, @PathVariable("index") String index);

    @DeleteMapping(value = "/users/{userId}/personas/parent/children/{index}", consumes = "application/json")
    ResponseEntity<Void> deleteChild(@PathVariable("userId") String userId, @PathVariable("index") String index);

    @GetMapping(value = "/users", consumes = "application/json")
	User[] checkValidEmail(@RequestParam String email);
    
    @GetMapping(value = "/lookups?types={types}", consumes = "application/json")
    LookupListReponse lookUp(@PathVariable("types") String types);

    @GetMapping(value = "/lookups/app-user-types", consumes = "application/json")
	List<AppUserTypeResponse> getAppUserTypes();

    @DeleteMapping(value = "/users/{userId}/additionalorgs/{index}")
    String deleteAdditionalOrg(@PathVariable String userId, @PathVariable String index);

    @PostMapping(value = "/users/{userId}/additionalorgs")
    AdditionalOrgResponse createAdditionalOrg(@PathVariable String userId, @RequestBody AdditionalOrg additionalOrg);

}

