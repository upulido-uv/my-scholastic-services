package com.myscholastic.feignclients.idam;

import com.myscholastic.configurations.feign.IamClientConfig;
import com.myscholastic.models.cybersource.Wallet;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "IamCreditCardClient", url = "${api-gateway.iamRoute}", configuration = IamClientConfig.class)
public interface CreditCardClient {

    // https://nonprod.api.scholastic.com/qa1/app/iam-svcs/1.0/users/{0}/wallet/credit-cards/{1}
    @PutMapping(value = "/users/{userId}/wallet/credit-cards/{cbsToken}", consumes = "application/json", produces = "application/json")
    Wallet addCreditCard(@PathVariable String userId, @PathVariable String cbsToken,
                         @RequestBody Wallet wallet);

}
