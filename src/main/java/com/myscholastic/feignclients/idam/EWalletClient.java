package com.myscholastic.feignclients.idam;

import com.myscholastic.configurations.AppConfig;
import com.myscholastic.models.BaseIamResponseModel;
import com.myscholastic.models.ewallet.EWalletId;
import com.myscholastic.models.ewallet.Voucher;
import com.myscholastic.models.ewallet.VoucherTransaction;
import com.myscholastic.models.ewallet.request.CreateEWalletRequest;
import com.myscholastic.models.ewallet.request.CreateVoucherRequest;
import com.myscholastic.models.ewallet.response.DeleteVoucherResponse;
import com.myscholastic.models.ewallet.response.PostVoucherTransactionResponse;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "EWalletClient", url = "${api-gateway.eWalletServicsRoute}", configuration = EWalletClient.EWalletConfig.class )
public interface EWalletClient {

    @GetMapping(value = "/wallets/{walletId}/vouchers?transactions=true&status=all", produces = "application/json")
    List<Voucher> getVouchersByWalletId(@PathVariable(value = "walletId") String walletId);

    @GetMapping(value = "/wallets", produces = "application/json")
    List<EWalletId> getEWalletBySpsId(@RequestParam(value = "customerProfileId") String customerProfileId);

    @PostMapping(value = "/wallets", produces = "application/json")
    BaseIamResponseModel createEWalletBySpsId(@RequestBody CreateEWalletRequest eWalletRequest);

    @PostMapping(value = "/wallets/{walletId}/vouchers", produces = "application/json")
    BaseIamResponseModel createVoucherByWalletId(@PathVariable Long walletId, @RequestBody CreateVoucherRequest eWalletRequest);

    @PostMapping(value = "/vouchers/{voucherId}/transactions", consumes = "application/json", produces = "application/json")
    PostVoucherTransactionResponse postVoucherTransaction(@PathVariable String voucherId, @RequestBody VoucherTransaction voucherTransaction);

    @DeleteMapping(value = "/vouchers/{voucherId}", produces = "application/json")
    DeleteVoucherResponse deleteVoucher(@PathVariable String voucherId);

    class EWalletConfig {

        @Autowired
        private AppConfig appConfig;

        @Bean
        public RequestInterceptor iamRequestInterceptor() {
            return template -> template.header("Authorization", "Bearer " + appConfig.getApiGatewayConfig().getEWalletServicsBearerToken());
        }

    }
}
