package com.myscholastic.feignclients.idam;

import com.myscholastic.configurations.feign.IamSpsLookupConfig;
import com.myscholastic.models.myprofile.response.LookupResponse;
import com.myscholastic.models.myprofile.response.SchoolResponse;
import com.myscholastic.models.myprofile.spslookup.KeyName;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "IamSpsLookupClient", url = "${api-gateway.iamSpsLookupRoute}", configuration = IamSpsLookupConfig.class)
public interface IamSpsLookupClient {

    @GetMapping(value = "/zipcode/{zipcode}/school?clientId=NonSPSClient&forceClient")
	List<SchoolResponse> getSchools(@PathVariable("zipcode") String zipcode);

	@GetMapping(value = "/zipcode/{zipcode}/school?clientId=NonSPSClient&forceClient={forceClient}")
	List<SchoolResponse> getSchools(@PathVariable("zipcode") String zipcode, @PathVariable("forceClient") String forceClient);

    @GetMapping(value = "/zipcode/{zipcode}/school?clientId=NonSPSClient&SchoolType=org&forceClient")
	List<SchoolResponse> getOrgs(@PathVariable("zipcode") String zipcode);

    @GetMapping(value = "/zipcode/{zipcode}/school?clientId=NonSPSClient&SchoolType=org&forceClient={forceClient}")
	List<SchoolResponse> getOrgs(@PathVariable("zipcode") String zipcode, @PathVariable("forceClient")  String forceClient);

    @GetMapping(value = "/{lookupType}?clientId=NonSPSClient")
	List<LookupResponse> lookUp(@PathVariable("lookupType") String lookupType);

    @GetMapping(value = "/school-types?clientId=NonSPSClient&locationType={locationType}")
	List<LookupResponse> getschoolTypes(@PathVariable("locationType") String locationType);

    @GetMapping(value = "/states/{state}/cities?clientId=NonSPSClient", produces = "application/json")
	List<KeyName> getCitiesByState(@PathVariable String state);
}

