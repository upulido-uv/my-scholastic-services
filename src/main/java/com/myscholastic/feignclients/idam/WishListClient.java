package com.myscholastic.feignclients.idam;

import com.myscholastic.configurations.AppConfig;
import com.myscholastic.feignclients.orderhistory.OrderHistoryClient;
import com.myscholastic.models.wishlist.DeleteWishListResponse;
import com.myscholastic.models.wishlist.WishList;
import feign.Logger;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "WishListClient", url = "${api-gateway.wishlistServiceRoute}", configuration = WishListClient.Config.class)
public interface WishListClient {

    @GetMapping(path = "/users/{userId}/lists/wishlist/items", produces = {"application/json"})
    WishList getAllWishListItems(@PathVariable String userId);

    @DeleteMapping(path = "/users/{userId}/lists/wishlist", produces = {"application/json"})
    DeleteWishListResponse deleteAllWishListItems(@PathVariable String userId);

    @DeleteMapping(path = "/users/{userId}/lists/wishlist/items/product/{productId}", produces = {"application/json"})
    DeleteWishListResponse deleteWishListItemByProductId(@PathVariable String userId, @PathVariable String productId);


    class Config {

        @Autowired
        private AppConfig appConfig;

        @Bean
        public RequestInterceptor setHeader() {
            return template -> template.header("Authorization", "Bearer " + appConfig.getApiGatewayConfig().getWishlistServiceBearerToken());
        }
        @Bean
        Logger.Level feignLoggerLevel() {
            return Logger.Level.FULL;
        }

    }


}
