package com.myscholastic.feignclients.idam;

import com.myscholastic.configurations.AppConfig;

import com.myscholastic.models.authentication.response.SecretResponse;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "IamSecretsManagerClient", url = "${api-gateway.iamSecretsManagerRoute}", configuration = IamSecretsManagerClient.Config.class)
public interface IamSecretsManagerClient {

    @GetMapping(value = "/keys", consumes = "application/json")
    SecretResponse getKeys();

    class Config {
        @Autowired
        private AppConfig appConfig;

        @Bean
        public RequestInterceptor SecretManagerClientInterceptor() {
            return template -> template.header("Authorization", "Bearer " + appConfig.getApiGatewayConfig().getIamSecretsManagerBearerToken());
        }
    }
}

