package com.myscholastic.feignclients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import com.myscholastic.configurations.feign.BriteEmailVerifyConfig;
import com.myscholastic.models.myprofile.BriteVerify;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "BriteEmailVerifyClient", url = "${api-gateway.briteVerifyEmailRoute}", configuration = BriteEmailVerifyConfig.class)
public interface BriteEmailVerifyClient {

	@GetMapping(value = "/", consumes = "application/json")
	ResponseEntity<BriteVerify> verify(@RequestParam String address);

}