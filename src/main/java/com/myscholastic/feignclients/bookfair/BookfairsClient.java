package com.myscholastic.feignclients.bookfair;

import com.myscholastic.configurations.feign.BookfairServicesConfig;
import com.myscholastic.models.ewallet.Fair;
import com.myscholastic.models.ewallet.FairIdList;
import com.myscholastic.models.myprofile.BookfairOrganizationInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "BookfairsClient", url = "${api-gateway.bookfairsServiceRoute}", configuration = BookfairServicesConfig.class )
public interface BookfairsClient {

    @PostMapping(value = "/", produces = "application/json")
    List<Fair> getFairsInfo(@RequestBody FairIdList fairIdList);

}
