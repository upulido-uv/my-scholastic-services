package com.myscholastic.feignclients.bookfair;

import com.myscholastic.configurations.feign.BookfairServicesConfig;
import com.myscholastic.models.ewallet.Fair;
import com.myscholastic.models.ewallet.FairIdList;
import com.myscholastic.models.myprofile.BookfairOrganizationInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "BookfairsCachedClient", url = "${api-gateway.bookfairsServiceCachedRoute}", configuration = BookfairServicesConfig.class )
public interface BookfairsCachedClient {

    /**
     * Gets bookfair school information based on idamOrgUcn(schoolUcn in CPTK)
     * @param schoolUcn Limit of 10 schoolUcn Ids MAX each API call
     * @return
     */
    @GetMapping(value = "/ofe/school/{schoolUcn}", consumes = "application/json")
    List<BookfairOrganizationInfo> getBookfairOrganizationInfo(@PathVariable String schoolUcn);

}
