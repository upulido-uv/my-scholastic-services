package com.myscholastic.feignclients.bookfair;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.myscholastic.configurations.feign.BookfairsCPTKServicesConfig;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "BookfairsCPTKClient", url = "${api-gateway.bookfairsCPTKRoute}", configuration = BookfairsCPTKServicesConfig.class)
public interface BookfairsCPTKClient {

	@GetMapping(value = "/CPTSPSServlet")
	String checkBookFairEmail(@RequestParam String CPEmail);

}
