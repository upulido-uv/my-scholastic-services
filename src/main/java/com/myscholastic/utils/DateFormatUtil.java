package com.myscholastic.utils;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Slf4j
public class DateFormatUtil {

    private final static DateTimeFormatter DATE_PATTERN_0 = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    private final static DateTimeFormatter DATE_PATTERN_1 = DateTimeFormatter.ofPattern("MMMM dd, yyyy");

    public static String formatDate(String date) {
    //TODO: This returned EMPTY_STRING in the old MyScholstic Code. We may have to change it back, keep a note of it.
        if (date == null) return null;
        LocalDate localDate;
        try {
            localDate = LocalDate.parse(date, DATE_PATTERN_0);
        } catch (DateTimeParseException e) {
            log.warn("{} couldn't be parsed", date);
            return null;
        }
        return localDate.format(DATE_PATTERN_1);
    }
}
