package com.myscholastic.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myscholastic.exception.MyScholasticException;
import com.myscholastic.models.cookies.CookieData;
import com.myscholastic.models.myprofile.User;
import com.myscholastic.models.authentication.response.SecretResponse;
import com.myscholastic.services.rotatingsecret.RotatingSecretService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.myscholastic.utils.EncryptionUtil.*;

@Slf4j
@Component
public class CookieUtil {

    private static final String MYSCHL = "MYSCHL_";
    private static final String MYSCHL_REFERRER = "MYSCHL_referrerUrl";
    private static final String SCHOLASTIC_DOMAIN = "scholastic.com";
    private static final String COOKIE_SPS_SESSION = "SPS_SESSION";
    private static final String COOKIE_SPS_SESSION_SECURE = "SPS_SESSION_SECURE";
    private static final String COOKIE_SPS_TSP = "SPS_TSP";
    private static final String COOKIE_SPS_TSP_SECURE = "SPS_TSP_SECURE";
    private static final String COOKIE_SCHL = "SCHL";
    private static final String COOKIE_SPS_UD = "SPS_UD";

    private static final String COOKIE_ISSUER = "MyScholastic";
    private static final String SLASH = "/";
    private static final String EQUALS = "=";
    private static final String SEMICOLON = "; ";
    private static final String COLON = ":";
    private static final int TWENTY_FOUR = 24;
    private static final int THIRTY = 30;
    private static final int SIXTY = 60;

    private static RotatingSecretService rotatingSecretService;

    @Autowired
    private RotatingSecretService rotatingSecretServicePostConstruct;

    @PostConstruct
    private void initStaticRotatingKeyService() {
        rotatingSecretService = rotatingSecretServicePostConstruct;
    }

    //TODO: Are these cookies required? MYSCHL_BookFair_Email, MYSCHL_BookFair_sUCN, MYSCHL_persona, MYSCHL_role
    public static final List<String> SPSCOOKIES_NAMES = Collections.unmodifiableList(
//        Arrays.asList(COOKIE_SCHL, COOKIE_SPS_SESSION, COOKIE_SPS_SESSION_SECURE, COOKIE_SPS_TSP, COOKIE_SPS_TSP_SECURE, COOKIE_SPS_UD)
        Arrays.asList(COOKIE_SPS_SESSION, COOKIE_SPS_SESSION_SECURE, COOKIE_SPS_TSP, COOKIE_SPS_TSP_SECURE, COOKIE_SPS_UD)
    );
    // EWallet
    private static final String EWALLET_COOKIE_ID = "MYSCHL_EWfairID";

    /**
     * Method to creates SPS_XUS Cookie.
     *
     * @param spsID    the sps ID
     * @param response the response
     * @throws MyScholasticException the my scholastic exception
     */
    public static void createSpsXusCookie(String spsID, HttpServletResponse response) {
        Cookie SPS_XUS = new Cookie("XUS_EI", encrypt(spsID));
        SPS_XUS.setDomain(SCHOLASTIC_DOMAIN);
        SPS_XUS.setPath("/");
        response.addCookie(SPS_XUS);
    }

    /**
     * Method to creates SPS_SESSION Cookie.
     *
     * @param spsID    the sps ID
     * @param response the response
     */
    public static void createSpsSessionCookie(String spsID, HttpServletResponse response) {
        createCookie(response, COOKIE_SPS_SESSION, encrypt(spsID), false);
    }

    public static void createSCHLCookie(String spsID, HttpServletResponse response, long sessionTimeout) {
        SecretResponse secretResponse = rotatingSecretService.getKeys();

        Optional.ofNullable(secretResponse)
                .map(SecretResponse::getKeys)
                .map(keys -> keys.get(secretResponse.getCurrentKey()))
                .map(secret -> {
                    String timeStamp = String.valueOf(System.currentTimeMillis() + (sessionTimeout));
                    StringBuilder cookiePayload = new StringBuilder();

                    CookieData payload = new CookieData(spsID, timeStamp, COOKIE_ISSUER);

                    cookiePayload.append(secretResponse.getCurrentKey());
                    cookiePayload.append(COLON);
                    cookiePayload.append(encryptWithSecret(payload.toString(), secret));

                    createCookie(response, COOKIE_SCHL, Base64.encodeBase64String(cookiePayload.toString().getBytes()), false);
                    log.debug("SCHL cookie has been created for user {}", spsID);
                    return null; // A return value is needed
                });
    }

    private static void createCookie(HttpServletResponse response, String name, String value, boolean isSecure) {
        log.debug("Cookie: Name: {}, Value: {}", name, value);
        Cookie cookie = new Cookie(name, value);
        cookie.setDomain(SCHOLASTIC_DOMAIN);
        cookie.setPath("/");
        cookie.setMaxAge(THIRTY * TWENTY_FOUR * SIXTY * SIXTY);
        cookie.setSecure(isSecure);
        response.addCookie(cookie);
    }

    /**
     * Method to creates SPS_SESSION_SECURE Cookie.
     *
     * @param spsID    the sps ID
     * @param response the response
     */
    public static void createSpsSessionSecureCookie(String spsID, HttpServletResponse response) {
        createCookie(response, COOKIE_SPS_SESSION_SECURE, encryptSecure(spsID), true);
    }

    /**
     * Method to create SPS_TSP Cookie.
     *
     * @param response       the response
     * @param sessionTimeout the session time out
     */
    public static void createSpsTspCookie(HttpServletResponse response, long sessionTimeout) {
        String timeStamp = String.valueOf(System.currentTimeMillis() + (sessionTimeout));
        createCookie(response, COOKIE_SPS_TSP, encrypt(timeStamp), false);
    }

    /**
     * Method to create SPS_TSP_SECURE Cookie.
     *
     * @param response       the response
     * @param sessionTimeout the session time out
     */
    public static void createSpsTspSecureCookie(HttpServletResponse response, long sessionTimeout) {
        String timeStamp = String.valueOf(System.currentTimeMillis() + sessionTimeout);
        createCookie(response, COOKIE_SPS_TSP_SECURE, encryptSecure(timeStamp), true);
    }

    /**
     * Method to create SPS_UD Cookie.
     *
     * @param spsID     the sps ID
     * @param userEmail the user name
     * @param firstName the first name
     * @param lastName  the last name
     * @param response  the response
     */
    public static void createSpsUdCookie(String spsID, String userEmail, String firstName, String lastName,
                                         HttpServletResponse response) throws UnsupportedEncodingException {
        String value = spsID + "|" + userEmail + "|" + URLEncoder.encode(firstName.trim(), "UTF-8") + "|" + URLEncoder.encode(lastName.trim(), "UTF-8") + "|null";
        createCookie(response, COOKIE_SPS_UD, value, false);
    }

    /**
     * Creates the cookie.
     *
     * @param userObject     the user object
     * @param response       the response
     * @param sessionTimeout the session time out
     */
    public static void createCookie(User userObject, HttpServletResponse response, long sessionTimeout) {

        try {
            String spsID;
            String userEmail;
            String firstName;
            String lastName;

            if (null != userObject) {
                spsID = userObject.getIdentifiers().getSps();
                userEmail = userObject.getBasicProfile().getEmail();
                firstName = userObject.getBasicProfile().getFirstName();
                lastName = userObject.getBasicProfile().getLastName();

                // creating SPS Cookies for the User
                createSpsXusCookie(spsID, response);
                createSpsSessionCookie(spsID, response);
                createSpsSessionSecureCookie(spsID, response);
                createSpsTspCookie(response, sessionTimeout);
                createSpsTspSecureCookie(response, sessionTimeout);
                createSpsUdCookie(spsID, userEmail, firstName, lastName, response);

                // create cookie with rotating key
//                createSCHLCookie(spsID, response, sessionTimeout);
            }
        } catch (Exception exception) {
            log.error("***** Error creating cookies", exception);
        }
    }

    /**
     * Method to readCookies with the name provided.
     *
     * @param request the request
     * @param pName   the name
     * @return the string
     */
    public static String getCookieValue(final HttpServletRequest request, final String pName) {
        Cookie cookie = getCookie(request, pName);
        if (null != cookie) {
            return cookie.getValue();
        }
        return null;
    }

    /**
     * Method to retrieve cookie with the provided name.
     *
     * @param request the request
     * @param pName   the name
     * @return the cookie
     */
    public static Cookie getCookie(final HttpServletRequest request, final String pName) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            return Arrays.stream(cookies)
                .filter(cookie -> pName.equalsIgnoreCase(cookie.getName()))
                .findFirst()
                .orElse(null);
        }
        return null;
    }

    /**
     * Method to readCookies with the name provided and returns a String with
     * name and value.
     *
     * @param request the request
     * @param pName   the name
     * @return the string
     */
    public static String readCookieWithNameString(final HttpServletRequest request, final String pName) {
        Cookie cookie = getCookie(request, pName);
        if (cookie != null) {
            return cookie.getName() + EQUALS + cookie.getValue() + SEMICOLON;
        }
        return null;
    }

    /**
     * Reads all the session related cookies and returns a string.
     *
     * @param request the request
     * @return the session cookies
     */
    public static String getSessionCookies(final HttpServletRequest request) {
        StringBuilder sessionCookiesString = new StringBuilder();
        sessionCookiesString.append(readCookieWithNameString(request, COOKIE_SPS_SESSION));
        String spsSessionSecure = readCookieWithNameString(request, COOKIE_SPS_SESSION_SECURE);
        if (!StringUtils.isEmpty(spsSessionSecure)) {
            sessionCookiesString.append(spsSessionSecure);
        }
        sessionCookiesString.append(readCookieWithNameString(request, COOKIE_SPS_TSP));
        String spsTSPSessionSecure = readCookieWithNameString(request, COOKIE_SPS_TSP_SECURE);
        if (!StringUtils.isEmpty(spsTSPSessionSecure)) {
            sessionCookiesString.append(spsTSPSessionSecure);
        }
        String spsSCHLSessionString = readCookieWithNameString(request, COOKIE_SCHL);
        if (!StringUtils.isEmpty(spsSCHLSessionString)) {
            sessionCookiesString.append(spsSCHLSessionString);
        }
        return sessionCookiesString.toString();
    }

    /**
     * Reads all the cookies and returns a string.
     *
     * @param request the request
     * @return the cookies
     */
    public static String getCookies(final HttpServletRequest request) {
        StringBuilder sessionCookiesString = new StringBuilder();
        sessionCookiesString.append(readCookieWithNameString(request, COOKIE_SPS_SESSION));
        String spsSessionSecure = readCookieWithNameString(request, COOKIE_SPS_SESSION_SECURE);
        if (!StringUtils.isEmpty(spsSessionSecure)) {
            sessionCookiesString.append(spsSessionSecure);
        }
        sessionCookiesString.append(readCookieWithNameString(request, COOKIE_SPS_TSP));
        String spsTSPSessionSecure = readCookieWithNameString(request, COOKIE_SPS_TSP_SECURE);
        if (!StringUtils.isEmpty(spsTSPSessionSecure)) {
            sessionCookiesString.append(spsTSPSessionSecure);
        }
        String spsSCHLSessionString = readCookieWithNameString(request, COOKIE_SCHL);
        if (!StringUtils.isEmpty(spsSCHLSessionString)) {
            sessionCookiesString.append(spsSCHLSessionString);
        }
        sessionCookiesString.append(readCookieWithNameString(request, COOKIE_SPS_UD));
        return sessionCookiesString.toString();
    }

    /**
     * Reads the spsId / userId from the cookie information if session secure is
     * present reads from the session secure cookie if session secure is not
     * available reads from the session cookie.
     *
     * @param request the request
     * @return the sps id from cookie
     */
    public static String getSpsIdFromCookie(final HttpServletRequest request) {
//        String schlCookie = getCookieValue(request, COOKIE_SCHL);
        String secureSessionCookie = getCookieValue(request, COOKIE_SPS_SESSION_SECURE);
        String sessionCookie = getCookieValue(request, COOKIE_SPS_SESSION);

//        if (!StringUtils.isEmpty(schlCookie)) {
//            return getSpsIdFromValidSCHLCookie(request);
//        }
//
//        log.debug("Couldn't grab spsId from SCHL cookie, trying with legacy session cookies");
        if (!StringUtils.isEmpty(secureSessionCookie)) {
            return decryptSecure(secureSessionCookie);
        } else if (!StringUtils.isEmpty(sessionCookie)) {
            return decrypt(sessionCookie);
        }

        return null;
    }

    /**
     * Reads the spsId / userId from the SCHL cookie
     * @param request the request
     * @return the sps id from the cookie
     */
    public static String getSpsIdFromValidSCHLCookie(HttpServletRequest request) {
        String schlCookie = new String(Base64.decodeBase64(getCookieValue(request, COOKIE_SCHL)));
        String[] parsedCookie = schlCookie.split(COLON);

        if (parsedCookie.length != 2) {
            log.error("SCHL cookie did not have two parts");
            return "";
        }

        String keyID = parsedCookie[0];
        String signature =  parsedCookie[1];
        SecretResponse secretResponse = rotatingSecretService.getKeys();
        String cookieSecret = secretResponse.getKeys().get(keyID);

        if (cookieSecret == null) {
            log.error("Couldn't find cookie key ID in key list");
            return "";
        }

        String jsonString;
        CookieData cookieObject = null;
        try {
            jsonString = decryptWithSecret(signature, cookieSecret);

            ObjectMapper mapper = new ObjectMapper();
            cookieObject = mapper.readValue(jsonString, CookieData.class);
        } catch (Exception e) {
            log.error("Couldn't decrypt or unmarshall cookie json");
            return "";
        }

        String timeStampValue = cookieObject.getTimestamp();
        if (!StringUtils.isEmpty(timeStampValue)) {
            long cookieTimeStamp = Long.parseLong(timeStampValue);
            long currentTSP = System.currentTimeMillis();
            long timeDiffInMillis = cookieTimeStamp - currentTSP;
            double timeDiff = TimeUnit.MILLISECONDS.toMinutes(timeDiffInMillis);
            log.debug("timeStampValue : {}; currentTSP : {}; timeDiff : {};", timeStampValue, currentTSP, timeDiff);

            if (timeDiff <= 0) {
                log.error("Cookie is expired for user {}", cookieObject.getUserID());
                return "";
            }
        }

        return cookieObject.getUserID();
    }

    /**
     * Reads the secret ID and signature from SCHL cookkie information, attempts to
     * decrypt the cookie using that secret and verifies that the timestamp is older than
     * thirty minutes. If the cookie fails to decrypt, the session is invalidated
     *
     * @param request
     * @param response
     * @return boolean representing whether the user session is valid
     */
    public static boolean hasValidSCHLSession(final HttpServletRequest request, HttpServletResponse response) {
        String spsId = getSpsIdFromValidSCHLCookie(request);

        log.debug("Checking session for user {}", spsId);
        boolean isValidSession = !StringUtils.isEmpty(spsId);

        if (!isValidSession) {
            log.debug("No valid session, clearing session cookies");
            clearSessionCookies(request, response);
        }

        return isValidSession;
    }

    /**
     * Gets the TSP Cookie if secure tsp is present reads from the secure cookie
     * if secure tsp is not available reads from the tsp cookie.
     *
     * @param request the request
     * @return true, if successful
     */
    public static boolean hasTSPCookies(final HttpServletRequest request) {
        if (getCookie(request, COOKIE_SPS_TSP_SECURE) != null) {
            return true;
        } else return getCookie(request, COOKIE_SPS_TSP) != null;
    }

    /**
     * Gets the Session Cookie if secure tsp is present reads from the secure
     * cookie if secure tsp is not available reads from the tsp cookie.
     *
     * @param request the request
     * @return true, if successful
     */
    public static boolean hasSessionCookies(final HttpServletRequest request) {
        if (getCookie(request, COOKIE_SPS_SESSION_SECURE) != null) {
            return true;
        } else return getCookie(request, COOKIE_SPS_SESSION) != null;
    }

    /**
     * Gets the SCHL cookie if it exist
     *
     * @param request
     * @return
     */
    public static boolean hasSCHLCookie(final HttpServletRequest request) {
        if (getCookie(request, COOKIE_SCHL) != null) {
            return true;
        } else return getCookie(request, COOKIE_SCHL) != null;
    }

    /**
     * Deletes all the User Session Cookies.
     *
     * @param request  the request
     * @param response the response
     */
    public static void clearSessionCookies(final HttpServletRequest request, final HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (isSessionCookie(cookie.getName())) {
                cookie.setMaxAge(0);
                cookie.setValue(null);
                cookie.setDomain(SCHOLASTIC_DOMAIN);
                cookie.setPath("/");
                response.addCookie(cookie);
            }
        }
    }

    /**
     * Checks if is session cookie.
     *
     * @param cookieName the cookie name
     * @return true, if is session cookie
     */
    private static boolean isSessionCookie(final String cookieName) {
        return cookieName.equalsIgnoreCase(COOKIE_SPS_SESSION)
            || cookieName.equalsIgnoreCase(COOKIE_SPS_SESSION_SECURE)
            || cookieName.equalsIgnoreCase(COOKIE_SPS_TSP)
            || cookieName.equalsIgnoreCase(COOKIE_SPS_TSP_SECURE)
            || cookieName.equalsIgnoreCase(COOKIE_SCHL);
    }

    /**
     * Updates the Time Stamp for TSP and TSP Secure Cookies.
     *
     * @param request        the request
     * @param response       the response
     * @param sessionTimeout the session time out
     */
    public static void updateTSPCookies(final HttpServletRequest request, final HttpServletResponse response,
                                        long sessionTimeout) {
        log.debug("Updating the timestamp for the Users session");
        createSpsTspCookie(response, sessionTimeout);
        createSpsTspSecureCookie(response, sessionTimeout);
    }

    public static void updateSCHLCookie(final HttpServletRequest request, final HttpServletResponse response,
                                        long sessionTimeout) {
        log.debug("create new SCHL cookie with SPS ID and timestamp");
        String spsId = getSpsIdFromCookie(request);
        createSCHLCookie(spsId, response, sessionTimeout);
    }

    /**
     * Checks if is valid session.
     *
     * @param request        the request
     * @param response       the response
     * @param sessionTimeout the session time out
     * @return true, if is valid session
     */
    public static boolean isValidSession(HttpServletRequest request, HttpServletResponse response, long sessionTimeout) {
        boolean validSession = true;

//        if (hasSCHLCookie(request)) {
//            log.debug("Has SCHL session cookie");
//            validSession = hasValidSCHLSession(request, response);
//        } else
        if (hasSessionCookies(request) && hasTSPCookies(request)) {
            log.debug("Has all the session cookies");
            String timeStampValue = getCookieTimeStamp(request);
            if (!StringUtils.isEmpty(timeStampValue)) {
                long cookieTimeStamp = Long.parseLong(timeStampValue);
                long currentTSP = System.currentTimeMillis();
                long timeDiffInMillis = cookieTimeStamp - currentTSP;
                double timeDiff = TimeUnit.MILLISECONDS.toMinutes(timeDiffInMillis);
                log.debug("timeStampValue : {}; currentTSP : {}; timeDiff : {};", timeStampValue, currentTSP, timeDiff);
                if (timeDiff <= 0) {
                    clearSessionCookies(request, response);
                    validSession = false;
                }
            }
        } else {
            validSession = false;
            log.debug("No session cookies present, validSession=false");
        }
        if (validSession) {
//            updateSCHLCookie(request, response, sessionTimeout);
            updateTSPCookies(request, response, sessionTimeout);
        }
        return validSession;
    }

    /**
     * Gets the cookie time stamp.
     *
     * @param request the request
     * @return the cookie time stamp
     */
    public static String getCookieTimeStamp(final HttpServletRequest request) {
        Cookie cookie = getCookie(request, COOKIE_SPS_TSP_SECURE);
        if (cookie != null) {
            return decryptSecure(cookie.getValue());
        } else {
            cookie = getCookie(request, COOKIE_SPS_TSP);
            if (cookie != null) {
                return decrypt(cookie.getValue());
            }
        }
        return null;
    }

    /**
     * Removes the myscholastic custom created cookie.
     *
     * @param request  the request
     * @param response the response
     * @return the sling http servlet response
     */
    public static void removeMyscholasticCustomCreatedCookie(HttpServletRequest request, HttpServletResponse response) {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (StringUtils.startsWithIgnoreCase(cookie.getName(), MYSCHL)
                && !MYSCHL_REFERRER.equalsIgnoreCase(cookie.getName())) {
                cookie.setMaxAge(0);
                cookie.setValue("");
                cookie.setPath(SLASH);
                response.addCookie(cookie);
            }
        }
    }

    public static String getSpsRelatedCookies(HttpServletRequest request) {
        if (request.getCookies() == null) {
            return null;
        }

        String spsCookies = Arrays.stream(request.getCookies())
            .filter(CookieUtil::isSpsCookie)
            .map(cookie -> cookie.getName() + "=" + cookie.getValue())
            .collect(Collectors.joining(";"));

        if (!StringUtils.isEmpty(spsCookies)) {
            return spsCookies;
        } else {
            return null;
        }
    }

    private static boolean isSpsCookie(Cookie cookie) {
        return SPSCOOKIES_NAMES.contains(cookie.getName());
    }

    public static Long getBookFairId(HttpServletRequest request) {
        // Example: MYSCHL_EWfairID=992601009%7C323429
        String fairCookieValue = getCookieValue(request, EWALLET_COOKIE_ID);
        if (null != fairCookieValue) {
            String fairStr = fairCookieValue.split("%7C")[1];
            if (!StringUtils.isEmpty(fairStr)) {
                return Long.parseLong(fairStr);
            }
        }
        return null;
    }
}
