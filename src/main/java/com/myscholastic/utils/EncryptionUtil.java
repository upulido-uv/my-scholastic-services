package com.myscholastic.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Slf4j
public class EncryptionUtil {

    private static final String BLOWFISH_SECRET_KEY = "Blowfish";

    private static final String BLOWFISH_CIPHER = "Blowfish/ECB/PKCS5Padding";

    
    public static String decrypt(String key) {
        byte[] hasil = {0};
        try {
            Cipher cipher = getCipher("scholastic_key", Cipher.DECRYPT_MODE);
            hasil = cipher.doFinal(Base64.decodeBase64(key));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
            | IllegalBlockSizeException | BadPaddingException e) {
            log.error(e.getMessage());
            log.debug("Trace : ", e);
        }
        return new String(hasil);
    }

    public static String decryptSecure(String key) {
        byte[] hasil = {0};
        try {
            Cipher cipher = getCipher("scscholastic_key", Cipher.DECRYPT_MODE);
            hasil = cipher.doFinal(Base64.decodeBase64(key));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
            | IllegalBlockSizeException | BadPaddingException e) {
            log.error(e.getMessage());
            log.debug("Trace : ", e);
        }
        return new String(hasil);
    }

    public static String decryptWithSecret(String payload, String secret) {
        byte[] hasil = {0};
        try {
            Cipher cipher = getCipher(secret, Cipher.DECRYPT_MODE);
            hasil = cipher.doFinal(Base64.decodeBase64(payload));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
            | IllegalBlockSizeException | BadPaddingException e) {
            log.error(e.getMessage());
            log.debug("Trace : ", e);
            return "";
        }
        return new String(hasil);
    }

    public static String encrypt(String data) {
        byte[] hasil = {0};
        try {
            Cipher cipher = getCipher("scholastic_key", Cipher.ENCRYPT_MODE);
            hasil = cipher.doFinal(data.getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
            | IllegalBlockSizeException | BadPaddingException e) {
            log.error(e.getMessage());
            log.debug("Trace : ", e);
        }
        return new Base64().encodeToString(hasil);
    }

    public static String encryptSecure(String data) {
        byte[] hasil = {0};
        try {
            Cipher cipher = getCipher("scscholastic_key", Cipher.ENCRYPT_MODE);
            hasil = cipher.doFinal(data.getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
            | IllegalBlockSizeException | BadPaddingException e) {
            log.error(e.getMessage());
            log.debug("Trace : ", e);
        }
        return new Base64().encodeToString(hasil);
    }

    public static String encryptWithSecret(String payload, String secret) {
        byte[] hasil = {0};
        try {
            Cipher cipher = getCipher(secret, Cipher.ENCRYPT_MODE);
            hasil = cipher.doFinal(payload.getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
            | IllegalBlockSizeException | BadPaddingException e) {
            log.error("Error encrypting rotating key: {}", e.getMessage());
            log.error("Trace : ", e);
        }
        return new Base64().encodeToString(hasil);
    }

    private static Cipher getCipher(String key, int mode) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        byte[] keyData = (key).getBytes(StandardCharsets.UTF_8);
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, BLOWFISH_SECRET_KEY);
        Cipher cipher = Cipher.getInstance(BLOWFISH_CIPHER);
        cipher.init(mode, secretKeySpec);
        return cipher;
    }
}
