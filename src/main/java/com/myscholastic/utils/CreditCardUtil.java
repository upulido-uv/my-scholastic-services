package com.myscholastic.utils;

public final class CreditCardUtil {

    private static final String EMPTY = "";

    private CreditCardUtil(){}

    // I was misled by EWalletService#mapCardType()
    public static String mapCardType(String cardType) {
        if (cardType == null) return EMPTY;
        switch (cardType) {
            case "001":
                return "Visa";
            case "002":
                return "Mastercard";
            case "003":
                return "American Express";
            case "004":
                return "Discover";
            default:
                return "";
        }
    }
}
