package com.myscholastic.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.UUID;

@Component
@Slf4j
public class SignatureUtil {

    private static final String HMAC_SHA_256 = "HmacSHA256";

    public enum SecretKeyType {
        DEFAULT, EWALLET, UPDATEBILLING
    }

    /* Collection of non-determinstic methods */

    public String getTransactionUUID() {
        return UUID.randomUUID().toString();
    }

    public String getReferenceNumber() {
        return "MYSCHL_" + Instant.now().toEpochMilli();
    }

    public String getSignedDateTime() {
        return Instant.now().truncatedTo(ChronoUnit.SECONDS).toString();
    }

    public static String getHmac(String plainText, String secretKey) {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA_256);
            Mac mac = Mac.getInstance(HMAC_SHA_256);
            mac.init(secretKeySpec);

            return new String(Base64.getEncoder().encode(mac.doFinal(plainText.getBytes())));
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            log.error("Error occured while signing plain text", e);
            return null;
        }
    }
}
