package com.myscholastic.controllers.authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.myscholastic.configurations.WebMvcConfig;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.authentication.request.LoginRequest;
import com.myscholastic.models.authentication.response.LoginResponse;
import com.myscholastic.utils.CookieUtil;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping({"/api"})
public class AuthenticationController {

    @Autowired
    private IamClient iamClient;

    @Autowired
    private WebMvcConfig webMvcConfig;

    @ApiOperation(value = "")
    @PostMapping(value={"/login"}, produces = "application/json")
    public LoginResponse login(HttpServletRequest request, HttpServletResponse response, @RequestBody LoginRequest loginRequest) {
        LoginResponse loginResponse = iamClient.authenticate(loginRequest);

        if (loginResponse.getStatus().equals("success")) {
            CookieUtil.createCookie(loginResponse.getUser(), response, webMvcConfig.getSessionTimeout());
        }
        return loginResponse;
    }
}