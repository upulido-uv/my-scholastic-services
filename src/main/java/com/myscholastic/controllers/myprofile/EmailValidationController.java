package com.myscholastic.controllers.myprofile;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.myscholastic.exception.InvalidResponseException;
import com.myscholastic.exception.InvalidSessionException;
import com.myscholastic.feignclients.bookfair.BookfairsCPTKClient;
import com.myscholastic.feignclients.BriteEmailVerifyClient;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.myprofile.BookFairsVerifyEmail;
import com.myscholastic.models.myprofile.BriteVerify;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/verify")
public class EmailValidationController {

	@Autowired
	private BriteEmailVerifyClient briteEmailVerifyClient;

	@Autowired
	private IamClient idamClient;

	@Autowired
	private BookfairsCPTKClient bookfairsCPTKClient;

	@ApiOperation(value = "Brite Email Verification")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully verify email"),
			@ApiResponse(code = 400, message = "Error in getting service response", response = InvalidResponseException.class),
			@ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class) })
	@GetMapping(value = "/email/brite", produces = "application/json")
	public BriteVerify briteEmailVerify(@RequestParam String email, HttpServletRequest request,
			HttpServletResponse response) throws InvalidSessionException {

		ResponseEntity<BriteVerify> resp = briteEmailVerifyClient.verify(email);
		return resp.getBody();
	}

	@ApiOperation(value = "Valid Email Verification")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully verify email"),
			@ApiResponse(code = 400, message = "Error in getting service response", response = InvalidResponseException.class),
			@ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class) })
	@GetMapping(value = "/email/valid", produces = "application/json")
	public ResponseEntity<Void> checkValidEmail(@RequestParam String email, HttpServletRequest request, HttpServletResponse response)
			throws InvalidSessionException {

		if(idamClient.checkValidEmail(email).length>0) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		
	}

	@ApiOperation(value = "Book Fair Email Verification")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully verify email"),
			@ApiResponse(code = 400, message = "Error in getting service response", response = InvalidResponseException.class),
			@ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class) })
	@GetMapping(value = "/email/bookFairs", produces = "application/json")
	public BookFairsVerifyEmail checkBookFairEmail(@RequestParam String email, HttpServletRequest request,
			HttpServletResponse response)
			throws InvalidSessionException, JsonParseException, JsonMappingException, IOException {

		String resp = bookfairsCPTKClient.checkBookFairEmail(email).trim();
		XmlMapper xmlMapper = new XmlMapper();
		return xmlMapper.readValue(resp, BookFairsVerifyEmail.class);
	}

}
