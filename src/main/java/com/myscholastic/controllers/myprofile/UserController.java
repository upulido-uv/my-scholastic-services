package com.myscholastic.controllers.myprofile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myscholastic.controllers.AbstractBaseController;
import com.myscholastic.exception.InvalidResponseException;
import com.myscholastic.exception.InvalidSessionException;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.myprofile.*;
import com.myscholastic.models.myprofile.request.AdditionalOrg;
import com.myscholastic.models.myprofile.response.AdditionalOrgResponse;
import com.myscholastic.services.myprofile.UserService;
import com.myscholastic.utils.CookieUtil;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

@Slf4j
@RestController
@RequestMapping({"/my-profile", "/api"})
public class UserController extends AbstractBaseController {

    private static final String ROLES_TEACHER = "teacher";
    private static final String ROLES_ADMIN = "admin";
    
    @Autowired
    private IamClient idamClient;

    @Autowired
    private UserService userService;

    /**
     * Check session and return user details for the user in session
     * @param request Http request for resource
     * @param response Http response for the request
     * @return User as json object.
     * @throws InvalidSessionException  User session is invalid
     * @throws InvalidResponseException No response from API
     */
    @ApiOperation(value = "Get User Details", response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of user detail", response = User.class),
            @ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
            @ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class)
    })
    @GetMapping(path = "/user", produces = {"application/json"})
    public User getUser(@RequestParam(value = "sections", required = false) String sections, HttpServletRequest request, HttpServletResponse response) throws InvalidSessionException, InvalidResponseException {
        String userId = getUserId(request, response);
        if (!StringUtils.isEmpty(userId)) {
            log.info("Getting user info. userId: {}, sections: {}.", userId, sections);
            User user = getUserFromClient(userId, sections);
            return user;
        }
        return null;
    }

    private User getUserFromClient(String userId, String sections) throws InvalidResponseException {
        User user;
        try {
            if (StringUtils.isEmpty(sections)) {
                user = idamClient.getUser(userId);
            } else {
                user = idamClient.getUserWithSections(userId, sections);
            }
        } catch (FeignException e) {
            throw new InvalidResponseException("Error in getting service response",e);
        }
        return user;
    }
    
    /**
     * deletes credit card
     * @param response Http response for the request
     * @return null, No Response body - Only Status Code
     * @throws InvalidSessionException  User session is invalid
     * @throws InvalidResponseException No response from API
     */
    @ApiOperation(value = "DELETE credit card")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully delete credit card"),
            @ApiResponse(code = 400, message = "Error in getting service response", response = InvalidResponseException.class),
            @ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class)
    })
    @DeleteMapping(value = "/credit-card/{cardId}", produces = "application/json")
    public Wallet deleteCreditCard(@PathVariable String cardId, HttpServletRequest request, HttpServletResponse response) throws InvalidSessionException {
        String userId = getUserId(request, response);
        log.info("Delete tokenized credit card. userId: {}", userId);
        return idamClient.deleteCreditCard(userId, cardId);
    }

    /**
     * Deletes address
     * @param request Http request for resource
     * @param response Http response for the request
     * @return User as json object.
     * @throws InvalidSessionException  User session is invalid
     * @throws InvalidResponseException No response from API
     */
    @ApiOperation(value = "Deletes address")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful delete of address"),
            @ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
            @ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class)
    })
    @DeleteMapping(path = "/addresses/{addressId}", produces = {"application/json"})
    public ResponseEntity<Void> deleteUserAddress(@RequestParam(value = "sections", required = false) String sections, @PathVariable String addressId, HttpServletRequest request, HttpServletResponse response) throws InvalidSessionException, InvalidResponseException {
        String userId = getUserId(request, response);
        if (!StringUtils.isEmpty(userId)) {
            log.info("Delete Address. userId : {}, addressId: {}, sections: {}", userId, addressId, sections);
            return idamClient.deleteAddress(userId, addressId);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    
    
    /**
     * Deletes child
     * @param request Http request for resource
     * @param response Http response for the request
     * @return User as json object.
     * @throws InvalidSessionException  User session is invalid
     * @throws InvalidResponseException No response from API
     */
    @ApiOperation(value = "Deletes child")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful delete of child"),
            @ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
            @ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class)
    })
    @DeleteMapping(path = "/children/{childId}", produces = {"application/json"})
    public ResponseEntity<Void> deleteChild(@RequestParam(value = "sections", required = false) String sections, @PathVariable String childId, HttpServletRequest request, HttpServletResponse response) throws InvalidSessionException, InvalidResponseException {
        String userId = getUserId(request, response);
        if (!StringUtils.isEmpty(userId)) {
            log.info("Delete child. userId : {}, childId: {}, sections: {}", userId, childId, sections);
            return idamClient.deleteChild(userId, childId);       
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    
    
    /**
     * Deletes Role
     * @param request Http request for resource
     * @param response Http response for the request
     * @return User as json object.
     * @throws InvalidSessionException  User session is invalid
     * @throws InvalidResponseException No response from API
     * @throws JsonProcessingException 
     */
    @ApiOperation(value = "Deletes role")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful delete of child"),
            @ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
            @ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class)
    })
    @DeleteMapping(path = "/roles/{role}", produces = {"application/json"})
    public ResponseEntity<Void> deleteRole(@RequestParam(value = "sections", required = false) String sections, @PathVariable String role, HttpServletRequest request, HttpServletResponse response) throws InvalidSessionException, InvalidResponseException, JsonProcessingException {
        String userId = getUserId(request, response);
        log.info("Delete roles. userId : {}, role: {}, sections: {}", userId, role, sections);
        
        if(ROLES_TEACHER.equalsIgnoreCase(role) || ROLES_ADMIN.equalsIgnoreCase(role)) {
        	User user = getUserFromClient(userId, sections);
        	Educator e= user.getPersonas().getEducator();
        	if(ROLES_TEACHER.equalsIgnoreCase(role)) {
            	e.getSchool().setGradeRoles(new String[0]);
            	e.getSchool().setClasses(new HashMap<String, ClassSize>());
        	}else if(ROLES_ADMIN.equalsIgnoreCase(role)) {
        		e.getSchool().setPositions(new String[0]);
        	}
        	String body = new ObjectMapper().writeValueAsString(e);
        	idamClient.updateEducator(userId, body);
        	return new ResponseEntity<>(HttpStatus.OK);
    	}
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

	/**
	 * Add Address
	 * 
	 * @param request  Http request for resource
	 * @param response Http response for the request
	 * @return User as json object.
	 * @throws InvalidSessionException  User session is invalid
	 * @throws InvalidResponseException No response from API
	 */
	@ApiOperation(value = "Add User Address", response = Address.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful add of user address", response = Address.class),
			@ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
			@ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class) })
	@PostMapping(path = "/addresses", produces = { "application/json" })
	public Address addAddress(@RequestParam(value = "sections", required = false) String sections,
			HttpServletRequest request, HttpServletResponse response, @RequestBody Address address)
			throws InvalidSessionException, InvalidResponseException {
		String userId = getUserId(request, response);
		if (!StringUtils.isEmpty(userId)) {
            log.info("Add address. userId : {}, sections: {}, addressId: {},", userId, sections, address.getId());
			String nickName = address.getLastName() + System.nanoTime();
			address.setNickName(nickName);
			return idamClient.addAddress(userId, address);
		}
		return null;
	}
	
	
	/**
	 * Add Child
	 * 
	 * @param request  Http request for resource
	 * @param response Http response for the request
	 * @return User as json object.
	 * @throws InvalidSessionException  User session is invalid
	 * @throws InvalidResponseException No response from API
	 */
	@ApiOperation(value = "Add Child", response = Child.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful add of child", response = Child.class),
			@ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
			@ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class) })
	@PostMapping(path = "/children", produces = { "application/json" })
	public Child addChild(@RequestParam(value = "sections", required = false) String sections,
			HttpServletRequest request, HttpServletResponse response, @RequestBody ChildRequest child)
			throws InvalidSessionException, InvalidResponseException {
		String userId = getUserId(request, response);
		if (!StringUtils.isEmpty(userId)) {
            log.info("Add Child. userId : {}, sections: {}, childFName: {},", userId, sections, child.getFirstName());
			return idamClient.addChild(userId, child);
		}
		return null;
	}

    @ApiOperation(value = "Delete additional organization per user")
    @DeleteMapping(value = "/additional-orgs/{index}")
    public String deleteAdditionalOrg(@PathVariable String index, HttpServletRequest request) {
        String userId = CookieUtil.getSpsIdFromCookie(request);
        log.info("Delete additional organization by index. userId: {}, index: {}", userId, index);
        if (userId == null) throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is not authorized");
        return idamClient.deleteAdditionalOrg(userId, index);
    }

    @ApiOperation(value = "Create additional organization per user")
    @PostMapping(value = "/additional-orgs", consumes = "application/json", produces =  "application/json")
    public AdditionalOrgResponse createAdditionalOrg(@RequestBody AdditionalOrg additionalOrg, HttpServletRequest request) {
	    String userId = CookieUtil.getSpsIdFromCookie(request);
        log.info("Posting additional organization by user '{}'", userId);
        if (userId == null) throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is not authorized");
	    return idamClient.createAdditionalOrg(userId, additionalOrg);
    }

    /** JsonP!!?? */
    @ApiOperation(value = "Get Profile Links per user")
    @ApiResponse(code = 422, message = "ProfileLinks object couldn't be converted to Json")
    @GetMapping(value = "/profile-links", produces = "application/javascript;charset=UTF-8")
    public String getProfileLinks(HttpServletRequest request) {
	    String userId = CookieUtil.getSpsIdFromCookie(request);
	    log.info("Getting profile links for user '{}'", userId);
        if (userId == null) throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is not authorized");
        try {
            String profileLinksJson = userService.getProfileLinks(userId);
            return String.format("GetProfileLinksJSON(%s)", profileLinksJson);
        } catch (JsonProcessingException e) {
            throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Profile Links object couldn't be serialized");
        }
    }
}
