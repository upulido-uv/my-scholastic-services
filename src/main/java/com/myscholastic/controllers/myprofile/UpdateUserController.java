package com.myscholastic.controllers.myprofile;

import com.myscholastic.controllers.AbstractBaseController;
import com.myscholastic.exception.InvalidResponseException;
import com.myscholastic.exception.InvalidSessionException;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.myprofile.*;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping({"/my-profile", "/api"})
public class UpdateUserController extends AbstractBaseController {

    @Autowired
    private IamClient idamClient;

    /**
     * Check session and if valid update user with provided details.
     * return user details for the user
     *
     * @param request  Http request for resource
     * @param response Http response for the request
     * @return User as json object.
     * @throws InvalidSessionException  User session is invalid
     * @throws InvalidResponseException No response from API
     */
    @ApiOperation(value = "Update User Details", response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of user detail", response = User.class),
            @ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
            @ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class)
    })
    @PostMapping(path = "/user", produces = {"application/json"})
    public User updateUser(HttpServletRequest request, HttpServletResponse response,
                           @RequestParam("updateType") String updateType,
                           @RequestParam(value = "index", required = false) String index,
                           @RequestBody String updateParam) throws InvalidSessionException, InvalidResponseException {        String userId = getUserId(request, response);
        log.debug("UserID : {}", userId);
        if (!StringUtils.isEmpty(userId) && !StringUtils.isEmpty(updateType) && !StringUtils.isEmpty(updateParam)) {
            User user = updateUser(updateType, userId, index, updateParam);
            //Note, do not log the updateParam! This could potentially contain password information!
            log.info("User Update. userId: {}, updateType: {}, index: {}", user, updateType, index);
            return user;
        }
        return null;
    }

    private User updateUser(String type, String userId, String index, String body) throws InvalidResponseException {
        User user = null;
        Educator educator = null;
        try {
            switch (type) {
                case "CREDENTIALS":
                    Credentials credentials = idamClient.updateCredentials(userId, body);
                    user = getUser(userId);
                    user.setCredentials(credentials);
                    break;
                case "ROLES":
                case "READING_LEVEL":
                case "EDU_PERSONA":
                case "SPECIALITIES":
                    educator = idamClient.updateEducator(userId, body);
                    user = getUserFromEducator(userId, educator);
                    break;
                case "PRIMARY_SCHOOL":
                    School school = idamClient.updateSchool(userId, body);
                    user = getUserFromEducator(userId, new Educator());
                    user.getPersonas().getEducator().setSchool(school);
                    break;
                case "FIRST_YEAR_TEACHING":
                    Contexts contexts = idamClient.updateContexts(userId, body);
                    user = getUser(userId);
                    user.setContexts(contexts);
                    break;
                case "MYSCHL_appUserName":
                    user = idamClient.updateUserAppType(userId, body);
                    user.setId(userId);
                    break;
                case "ADDN_ORG":
                    user = idamClient.updateUserChangeOrganization(userId, index, body);
                    user.setId(userId);
                    break;
                case "ADDRESS":
                    Address address = idamClient.updateAddress(userId, index, body);
                    user = getUser(userId);
                    user.setBasicProfile(new BasicProfile());
                    Map<String, Address> addresses = new HashMap<>();
                    addresses.put(String.valueOf(address.getId()), address);
                    user.getBasicProfile().setAddresses(addresses);
                    break;
                case "UPDATE_CHILD":
                    Child child = idamClient.updateChild(userId, index, body);
                    Map<String, Child> children = new HashMap<>();
                    children.put(child.getId(), child);
                    Parent parent = new Parent();
                    parent.setChildren(children);
                    user = getUser(userId);
                    user.setPersonas(new Personas());
                    user.getPersonas().setParent(parent);
                    break;
                default: //Type=USER
                    BasicProfile profile = idamClient.updateUser(userId, body);
                    user = getUser(userId);
                    user.setBasicProfile(profile);
            }
        } catch (FeignException e) {
            log.error("Error in getting service response. IDAM Status : {} , with response: {}", e.status(), e.contentUTF8());
            throw new InvalidResponseException(e.getMessage(), e);
        }
        return user;
    }

    private User getUserFromEducator(String userId, Educator educator) {
        User user;
        user = getUser(userId);
        user.setPersonas(new Personas());
        user.getPersonas().setEducator(educator);
        return user;
    }

    private User getUser(String userId) {
        User user;
        user = new User();
        user.setId(userId);
        return user;
    }
}
