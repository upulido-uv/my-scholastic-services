package com.myscholastic.controllers.myprofile;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myscholastic.models.myprofile.spslookup.KeyName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.myscholastic.exception.InvalidResponseException;
import com.myscholastic.exception.InvalidSessionException;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.feignclients.idam.IamSpsLookupClient;
import com.myscholastic.feignclients.idam.IamSpsOrgClient;
import com.myscholastic.models.myprofile.response.AppUserTypeResponse;
import com.myscholastic.models.myprofile.response.LookupListReponse;
import com.myscholastic.models.myprofile.response.LookupResponse;
import com.myscholastic.models.myprofile.response.SchoolInfoResponse;
import com.myscholastic.models.myprofile.response.SchoolResponse;
import com.myscholastic.services.misc.LookupService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/lookup")
public class LookupController {

	private static final String LOOKUP_STATES = "states";
	private static final String LOOKUP_COUNTRIES = "countries";

	@Autowired
	private IamSpsLookupClient iamSpsLookupClient;

	@Autowired
	private IamClient idamClient;

	@Autowired
	private IamSpsOrgClient iamSpsOrgClient;

	@Autowired
	private LookupService lookupService;

	
	@ApiOperation(value = "Generic IDAM Lookup", notes="types={grades|teacher-title|teacher-specialities|reading-level-systems|teacher-grade-roles|teacher-role-groups|salutations}", response = LookupListReponse.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successful Generic IDAM Lookup", response = LookupListReponse.class),
							@ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
							@ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class) })
	@GetMapping(path = "", produces = { "application/json" })
	public LookupListReponse lookup(@RequestParam(value = "types", required = false) String types,
			HttpServletRequest request, HttpServletResponse response)
			throws InvalidSessionException, InvalidResponseException {
		return idamClient.lookUp(types);
	}
	
	
	
	@ApiOperation(value = "Get Schools", notes="forceClient={F(Face)|W(Weston woods Org)|null}",response =SchoolResponse.class, responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of school", response =SchoolResponse.class, responseContainer = "List"),
			@ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
			@ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class) })
	@GetMapping(path = "/schools", produces = { "application/json" })
	public List<SchoolResponse> getSchools(@RequestParam(value = "zipcode", required = false) String zipcode,
										   @RequestParam(value = "country", required = false) String country,
										   @RequestParam(value = "groupType", required = false) String groupType,
										   @RequestParam(value = "forceClient", required = false) String forceClient,
			HttpServletRequest request, HttpServletResponse response)
			throws InvalidSessionException, InvalidResponseException {

		if(country!=null || groupType!=null) {
			return iamSpsOrgClient.getSchools(zipcode, country, groupType);
		}else if(zipcode!=null) {
			if(forceClient==null) {
				return iamSpsLookupClient.getSchools(zipcode);
			}else {
				return iamSpsLookupClient.getSchools(zipcode, forceClient);
			}
		}
		return null;
	}

	
	@ApiOperation(value = "Get Organizations", response = SchoolResponse.class, responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of Organizations", response = SchoolResponse.class, responseContainer = "List"),
			@ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
			@ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class) })
	@GetMapping(path = "/orgs", produces = { "application/json" })
	public List<SchoolResponse> getOrgs(@RequestParam(value = "zipcode", required = false) String zipcode,
										@RequestParam(value = "forceClient", required = false) String forceClient,
			HttpServletRequest request, HttpServletResponse response)
			throws InvalidSessionException, InvalidResponseException {
		
		if(forceClient==null) {
			return iamSpsLookupClient.getOrgs(zipcode);
		}else {
			return iamSpsLookupClient.getOrgs(zipcode, forceClient);
		}
	}


	@ApiOperation(value = "Get States", response = LookupResponse.class, responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of states", response = LookupResponse.class, responseContainer = "List"),
			@ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
			@ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class) })
	@GetMapping(path = "/states", produces = { "application/json" })
	public List<LookupResponse> getStates(HttpServletRequest request, HttpServletResponse response)
			throws InvalidSessionException, InvalidResponseException {

		return iamSpsLookupClient.lookUp(LOOKUP_STATES);
	}


	@ApiOperation(value = "Get Countries", response = LookupResponse.class, responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of countries", response = LookupResponse.class, responseContainer = "List"),
			@ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
			@ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class) })
	@GetMapping(path = "/countries", produces = { "application/json" })
	public List<LookupResponse> getCountries(HttpServletRequest request, HttpServletResponse response)
			throws InvalidSessionException, InvalidResponseException {

		return iamSpsLookupClient.lookUp(LOOKUP_COUNTRIES);
	}
	
	
	
	@ApiOperation(value = "Get School Types", notes="locationTypes={domestic|military|home|intl}", response = LookupResponse.class, responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of School Types", response = LookupResponse.class, responseContainer = "List"),
			@ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
			@ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class) })
	@GetMapping(path = "/schoolTypes", produces = { "application/json" })
	public List<LookupResponse> getSchoolTypes(
			@RequestParam(value = "locationType", required = false) String locationType, HttpServletRequest request,
			HttpServletResponse response) throws InvalidSessionException, InvalidResponseException {
		
		return iamSpsLookupClient.getschoolTypes(locationType);
	}


	
	@ApiOperation(value = "Get School Info", response = SchoolInfoResponse.class, responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of School Info", response = SchoolInfoResponse.class, responseContainer = "List"),
			@ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
			@ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class) })
	@GetMapping(path = "/schoolInfo", produces = { "application/json" })
	public List<SchoolInfoResponse> getSchoolInfo(@RequestParam(value = "spsId", required = false) String spsId,
			HttpServletRequest request, HttpServletResponse response)
			throws InvalidSessionException, InvalidResponseException {
		
		return iamSpsOrgClient.getschoolInfo(spsId);
	}


	
	@ApiOperation(value = "Get Role Class Size", response = LookupResponse.class, responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of Role Class Size", response = LookupResponse.class, responseContainer = "List"),
			@ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
			@ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class) })
	@GetMapping(path = "/roleClassSize", produces = { "application/json" })
	public List<LookupResponse> getRoleClassSize(HttpServletRequest request, HttpServletResponse response)
			throws InvalidSessionException, InvalidResponseException {

		return lookupService.getRoleClassSize();
	}


	
	@ApiOperation(value = "Get grades by Ordinal", response = LookupResponse.class, responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of grades by Ordinal", response = LookupResponse.class, responseContainer = "List"),
			@ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
			@ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class) })
	@GetMapping(path = "/gradesByOrdinals", produces = { "application/json" })
	public List<LookupResponse> getGradesByOrdinals(HttpServletRequest request, HttpServletResponse response)
			throws InvalidSessionException, InvalidResponseException {

		return lookupService.getGradesByOrdinals();
	}
	
	
	
	@ApiOperation(value = "Get App User Type", response = AppUserTypeResponse.class, responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful retrieval of App User Type", response = AppUserTypeResponse.class, responseContainer = "List"),
			@ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
			@ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class) })
	@GetMapping(path = "/appUserTypes", produces = { "application/json" })
	public List<AppUserTypeResponse> getAppUserTypes(HttpServletRequest request, HttpServletResponse response)
			throws InvalidSessionException, InvalidResponseException {
		
		return idamClient.getAppUserTypes();
	}

	// Not sure if this should go under RegisterController
	@ApiOperation("Get cities by state")
	@ApiResponse(code = 500, message = "If input (state) is invalid, Idam generates a 500")
    @GetMapping(path = "/cities", produces = "application/json")
	public List<KeyName> getCitiesByState(@RequestParam String state) {
        return iamSpsLookupClient.getCitiesByState(state.toUpperCase());
	}
}
