package com.myscholastic.controllers;

import com.myscholastic.configurations.AppConfig;
import com.myscholastic.exception.InvalidSessionException;
import com.myscholastic.utils.CookieUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public abstract class AbstractBaseController implements BaseController {

    @Autowired
    private AppConfig appConfig;

    @Override
    public String getUserId(HttpServletRequest request, HttpServletResponse response) throws InvalidSessionException {
        String userId;
        userId = CookieUtil.getSpsIdFromCookie(request);
        log.debug("userId {}", userId);
        //TODO: Find a proper place this should be added. IE: In the SessionValidationInterceptor or WebMvcConfig
        response.setHeader("Access-Control-Allow-Credentials", "true");
        if (userId != null)
            return userId.trim();
        throw new InvalidSessionException("Session Expired");
    }

}
