package com.myscholastic.controllers.register;

import com.myscholastic.exception.SpsOrgHasInvalidLocationTypeException;
import com.myscholastic.exception.SpsOrgMissingFieldException;
import com.myscholastic.models.myprofile.request.SpsOrganization;
import com.myscholastic.models.myprofile.response.SpsOrganizationResponse;
import com.myscholastic.services.spsorg.SpsOrgService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@RestController
@RequestMapping({"/register", "/api"})
public class RegistrationController {

    @Autowired
    private SpsOrgService spsOrgService;

    // Implementation from AEM takes a handful of query params as input, here changed to requestbody
    // Not sure if this is where it belongs.
    @ApiOperation(value = "Post all the school data to SPS Organization API")
    @PostMapping(value = "/submit-school", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public SpsOrganizationResponse submitSchool(@RequestBody SpsOrganization spsOrganization) {
        try {
            return spsOrgService.submitSchool(spsOrganization);
        } catch (SpsOrgMissingFieldException | SpsOrgHasInvalidLocationTypeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
