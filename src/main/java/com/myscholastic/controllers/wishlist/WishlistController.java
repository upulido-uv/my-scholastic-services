package com.myscholastic.controllers.wishlist;

import com.myscholastic.models.wishlist.DeleteWishListResponse;
import com.myscholastic.services.wishlist.WishListService;
import com.myscholastic.utils.CookieUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping({"/api"})
public class WishlistController {

    @Autowired
    private WishListService wishListService;

    @ApiOperation("Get a list of wishlist items per user")
    @GetMapping(path = "/wishlist", produces = "application/json")
    public List<String> getWishList(HttpServletRequest request) {
        String userId = CookieUtil.getSpsIdFromCookie(request);
        log.info("Get wishlist. userId: {}", userId);
        return wishListService.getProductIds(userId);
    }

    @ApiOperation("Delete a wishlist item per its (Mdm) product ID")
    @DeleteMapping(path = "/wishlist/product/{productId}", produces = "application/json")
    public DeleteWishListResponse deleteWishListByProductId(HttpServletRequest request, @PathVariable String productId) {
        String userId = CookieUtil.getSpsIdFromCookie(request);
        log.info("Delete wishlist item. userId: {}, productId: {}", userId, productId);
        return wishListService.deleteItemByProductId(userId, productId);
    }

    @ApiOperation("Delete all wishlist items per user")
    @DeleteMapping(path = "/wishlist", produces = "application/json")
    public DeleteWishListResponse deleteAllWishList(HttpServletRequest request) {
        String userId = CookieUtil.getSpsIdFromCookie(request);
        log.info("Delete all wishlist items. userId: {}", userId);
        return wishListService.deleteAllWishList(userId);
    }

}
