package com.myscholastic.controllers.ewallet;

import static com.myscholastic.utils.CookieUtil.getBookFairId;
import static com.myscholastic.utils.CookieUtil.getSpsIdFromCookie;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.myscholastic.feignclients.bookfair.BookfairsClient;
import com.myscholastic.feignclients.idam.EWalletClient;
import com.myscholastic.models.BaseIamResponseModel;
import com.myscholastic.models.ewallet.EWalletId;
import com.myscholastic.models.ewallet.Fair;
import com.myscholastic.models.ewallet.FairIdList;
import com.myscholastic.models.ewallet.FairVouchersJson;
import com.myscholastic.models.ewallet.request.CreateVoucherRequest;
import com.myscholastic.models.ewallet.response.DeleteVoucherResponse;
import com.myscholastic.models.ewallet.response.PostVoucherTransactionResponse;
import com.myscholastic.services.ewallet.EWalletService;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping({"/ewallet", "/api"})
public class EWalletController {

    @Autowired
    private EWalletClient ewalletClient;

    @Autowired
    private BookfairsClient bookfairsClient;

    @Autowired
    private EWalletService eWalletService;
    
    @ApiOperation(value = "Get a list of e-Wallet ids by user")
    @GetMapping(value={"", "/ewallet"}, produces = "application/json")
    public List<EWalletId> getEWalletBySpsId(HttpServletRequest request) {
        String spsId = getSpsIdFromCookie(request);
        if (StringUtils.isEmpty(spsId)) throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is not authorized.");
        log.info("Getting EWallet info. userId: {}", spsId);
        return ewalletClient.getEWalletBySpsId(spsId);
    }

    @ApiOperation(value = "Get a list of fairs by fairId")
    @GetMapping(value = {"/fair-info", "/fair"})
    public List<Fair> getFairsInfoByFairId(HttpServletRequest request, @RequestParam(required = false) Long fairId) {
        Long fairIdParam = (fairId != null) ? fairId : getBookFairId(request);
        if (fairIdParam == null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "fairId is not present in cookie or request param.");
        log.info("Getting fair information by FairId. userId: {}, fairId: {}", getSpsIdFromCookie(request), fairIdParam);
        return bookfairsClient.getFairsInfo(new FairIdList(new HashSet<>(Collections.singletonList(fairIdParam))));
    }

    @ApiOperation(value = "Get a list of fair vouchers by walletId")
    @GetMapping(value = {"/fair-vouchers/{walletId}", "/vouchers/{walletId}"}, produces = "application/json")
    public FairVouchersJson getFairsAndVouchersByWalletId(@PathVariable String walletId, HttpServletRequest request) {
        log.info("Getting fair vouchers by walletId. userId: {}, walletId: {}", getSpsIdFromCookie(request), walletId);
        Long ewalletCookieId = getBookFairId(request);
        return eWalletService.getFairVouchersJson(walletId, ewalletCookieId);
    }

    @ApiOperation(value = "Create a eWallet AND Voucher with CyberSource tokenized information.")
    @PostMapping(value = {"/ewallet-and-voucher", "/voucher"})
    public BaseIamResponseModel createWalletAndVoucherByUserUcn(HttpServletRequest request, @RequestBody CreateVoucherRequest eWalletRequestBody){
        String spsId = getSpsIdFromCookie(request);
        log.info("Creating new eWallet and Voucher for user: {}", spsId);
        return eWalletService.createEWalletAndVoucher(eWalletRequestBody, spsId);
    }

    @PostMapping(value = {"/voucher/voucher-transaction", "/voucher/transaction"})
    public PostVoucherTransactionResponse postVoucherTransactionbyVoucherId(HttpServletRequest request,
                                                                            @RequestParam String voucherId,
                                                                            @RequestParam String amount,
                                                                            @RequestParam String transactionType) {
        log.info("Posting eWallet transaction. voucherId: {}, amount: {}, type: {}.", voucherId, amount, transactionType);
        validateUserHasVoucher(request, voucherId);
        return eWalletService.postVoucherTransaction(voucherId, amount, transactionType);
    }

    @DeleteMapping(value = {"/voucher/{voucherId}"})
    public DeleteVoucherResponse deleteVoucherByVoucherId(HttpServletRequest request, @PathVariable String voucherId) {
        log.info("Deleting voucher. userId: {}, voucherId: {}", getSpsIdFromCookie(request), voucherId);
        validateUserHasVoucher(request, voucherId);
        return ewalletClient.deleteVoucher(voucherId);
    }

    // This is to prevent any authenticated user from posting/deleting any voucher
    private void validateUserHasVoucher(HttpServletRequest request, String voucherId) {
        String spsId = getSpsIdFromCookie(request);
        if (StringUtils.isEmpty(spsId))
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "User is not authorized.");
        if (!eWalletService.hasVoucher(spsId, voucherId))
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, String.format("%s doesn't belong to user", voucherId));
    }
}
