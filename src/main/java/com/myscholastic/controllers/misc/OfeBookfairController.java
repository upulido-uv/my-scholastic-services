package com.myscholastic.controllers.misc;

import com.myscholastic.controllers.AbstractBaseController;
import com.myscholastic.exception.InvalidResponseException;
import com.myscholastic.exception.InvalidSessionException;
import com.myscholastic.feignclients.bookfair.BookfairsCachedClient;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.myprofile.BookfairOrganizationInfo;
import com.myscholastic.services.misc.OfeBookfairService;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

@Slf4j
@RestController
@RequestMapping({"/my-bookfairs", "/api/bookfairs"})
public class OfeBookfairController extends AbstractBaseController {

    @Autowired
    private OfeBookfairService ofeBookfairService;

    @Autowired
    private IamClient idamClient;

    @Autowired
    private BookfairsCachedClient bookfairsCachedClient;

    private static final String PUT_JSON_BODY = "{\"default\": true}";

    /**
     * Get Bookfair org information of a user
     * @param request Http request for resource
     * @param response Http response for the request
     * @return List<BookfairOrganizationInfo> as json object.
     * @throws InvalidSessionException  User session is invalid
     * @throws InvalidResponseException No response from API
     */
    @ApiOperation(value = "Get User Bookfair Organization Information. *Requires Cookie for Identification/Session Validation", response = BookfairOrganizationInfo.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of user detail. Can return null.", response = BookfairOrganizationInfo.class),
            @ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class)
    })
    @GetMapping(path = {"/bookfair-school-associations", "/schools"}, produces = {"application/json"})
    public List<BookfairOrganizationInfo> getBookfairOrgAssociations(HttpServletRequest request, HttpServletResponse response) throws InvalidSessionException {
        String userId = getUserId(request, response);
        if (!StringUtils.isEmpty(userId)) {
            List<BookfairOrganizationInfo> bfOrgInfo = ofeBookfairService.getBookfairOrgNames(userId);
            log.info("Get OFE Bookfair Info. bfOrgInfo: {}", bfOrgInfo);
            return bfOrgInfo;
        }
        return Collections.emptyList();
    }

    /**
     * TODO: What is the limit of the number of schools that can be added?
     * Add bookfair org to user account
     * @param schoolUcn String to add to account
     * @param request Http request for resource
     * @param response Http response for the request
     * @return null, No Response body - Only Status Code
     * @throws InvalidSessionException  User session is invalid
     * @throws InvalidResponseException No response from API
     */
    @ApiOperation(value = "Add User Bookfair Organization Association. *Requires Cookie for Identification/Session Validation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful update of user detail. Sets this school to default."),
            @ApiResponse(code = 201, message = "Successful creation of user detail. Sets this school to default."),
            @ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
            @ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class)
    })
    @PutMapping(path = {"/bookfair-school-associations/{schoolUcn}", "/schools/{schoolUcn}"})
    public void getBookfairOrgAssociations(@PathVariable String schoolUcn, HttpServletRequest request, HttpServletResponse response) throws InvalidSessionException, InvalidResponseException {
        String userId = getUserId(request, response);
        if (!StringUtils.isEmpty(userId)) {

            //Validate that the school UCN being added into the system is a valid UCN.
            try{
                List<BookfairOrganizationInfo> checkValidUcn = bookfairsCachedClient.getBookfairOrganizationInfo(schoolUcn);
            } catch (FeignException e){
                log.error("orgUcn does not exists in Bookfair System. OrgUcn: {}", userId);
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "orgUcn does not exists in Bookfair System. OrgUcn: " + schoolUcn, e);
            }

            ResponseEntity<Void> idamResponse = idamClient.setUserBFOrgUcn(userId, schoolUcn, PUT_JSON_BODY);

            log.debug("idamResponse : {}", idamResponse);
            if(idamResponse.getStatusCode().is2xxSuccessful()){
                log.info("Bookfair Org Successfully Added/Updated. UserId: {}. Status Code 200 Update, 201 Created. Status: {}. schoolUcn: {}", userId, idamResponse.getStatusCode(), schoolUcn);
                response.setStatus(idamResponse.getStatusCode().value());
            }else{
                //TODO: Determine proper error messaging. Also, will code ever reach here? Global FeignException might be thrown before this.
                log.error("Bookfair Org Add/Update Error! User: {}, schoolUcn: {}, Status: {}", userId, schoolUcn, idamResponse.getStatusCode());
                throw new InvalidResponseException("Bookfair Org Add/Update Error!", null);
            }
        }
    }

    /**
     * TODO: What is the limit of the number of schools that can be added?
     * Delete bookfair org to user account
     * @param schoolUcn String to delete from account
     * @param request Http request for resource
     * @param response Http response for the request
     * @return null, No Response body - Only Status Code
     * @throws InvalidSessionException  User session is invalid
     * @throws InvalidResponseException No response from API
     */
    @ApiOperation(value = "Delete User Bookfair Organization Association. *Requires Cookie for Identification/Session Validation")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful deletion of user detail, even if it didn't exist before."),
            @ApiResponse(code = 400, message = "Error in getting service response.", response = InvalidResponseException.class),
            @ApiResponse(code = 401, message = "Session Expired.", response = InvalidSessionException.class)
    })
    @DeleteMapping(path = {"/bookfair-school-associations/{schoolUcn}", "/schools/{schoolUcn}"})
    public void deleteBookfairOrgAssociations(@PathVariable String schoolUcn, HttpServletRequest request, HttpServletResponse response) throws InvalidSessionException, InvalidResponseException {
        String userId = getUserId(request, response);
        if (!StringUtils.isEmpty(userId)) {

            ResponseEntity<Void> idamResponse = idamClient.deleteUserBFOrgUcn(userId, schoolUcn);
            log.debug("idamResponse : {}", idamResponse);

            if(idamResponse.getStatusCode().is2xxSuccessful()){
                log.info("Bookfair Org Successfully deleted. UserId: {}, Status: {}, schoolUcn: {}", userId, idamResponse.getStatusCode(), schoolUcn);
                response.setStatus(idamResponse.getStatusCode().value());
            }else{
                //TODO: Determine proper error messaging. Also, will code ever reach here? FeignException might be thrown before this.
                log.debug("Bookfair Org delete Error! User: {}, Status: {}", userId, idamResponse.getStatusCode());
                throw new InvalidResponseException("Bookfair Org Delete Error!", null);
            }
        }
    }

}