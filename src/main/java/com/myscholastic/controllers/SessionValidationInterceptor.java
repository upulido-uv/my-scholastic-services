package com.myscholastic.controllers;

import com.myscholastic.utils.CookieUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Slf4j
public class SessionValidationInterceptor extends HandlerInterceptorAdapter {

    private final long timeout;
    public SessionValidationInterceptor(long timeout) {
        this.timeout = timeout;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        // Allow for Preflight Request
        if (HttpMethod.OPTIONS.matches(request.getMethod())) {
            return true;
        }

        log.debug("Validating session with secret keys");
        boolean isValidSession = CookieUtil.isValidSession(request, response, timeout);
        log.debug("Is valid session? {}", isValidSession);

        if (!isValidSession) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
            response.getWriter().write("{\"message\" : \"User is not authorized\"}");
        }
        return isValidSession;
    }

}
