package com.myscholastic.controllers;

import com.myscholastic.exception.InvalidSessionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface BaseController {

    String getUserId(HttpServletRequest request, HttpServletResponse response) throws InvalidSessionException;

}
