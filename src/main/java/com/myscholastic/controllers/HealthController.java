package com.myscholastic.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {
    @Value("${app.version}")
    private String appVersion;

    @GetMapping(path = "/health", produces = {"application/json"})
    public String healthCheck() {
        return "{\"version\":\""+ appVersion +"\"}";
    }
}
