package com.myscholastic.controllers.cybersource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myscholastic.configurations.ReCaptchaClientConfig;
import com.myscholastic.feignclients.recaptcha.ReCaptchaClient;
import com.myscholastic.models.cybersource.CyberSourceDto;
import com.myscholastic.models.cybersource.Signature;
import com.myscholastic.models.cybersource.Wallet;
import com.myscholastic.models.recaptcha.ReCaptchaRequestPayload;
import com.myscholastic.models.recaptcha.ReCaptchaResponsePayload;
import com.myscholastic.models.subscription.UpdateBillingResponse;
import com.myscholastic.services.cybersource.CreditCardService;
import com.myscholastic.services.cybersource.HtmlService;
import com.myscholastic.services.cybersource.SignatureService;
import com.myscholastic.utils.SignatureUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Base64;
import java.util.Map;

@Slf4j
@RestController
public class CyberSourceController {

    @Autowired
    private SignatureService signatureService;
    @Autowired
    private CreditCardService creditCardService;
    @Autowired
    private HtmlService htmlService;

    @Autowired
    private ReCaptchaClient reCaptchaClient;

    private final ReCaptchaClientConfig reCaptchaClientConfig = new ReCaptchaClientConfig();

    /* thread safe */
    private static final ObjectMapper JSON_MAPPER = new ObjectMapper();
    private static final String EMPTY = "";

    /**
     * All logic is taken from AEM with various improvements
     * @return a BASE64 encoded string of a hmac and its plaintext
     * @throws JsonProcessingException if signature contains malformed structure
     */
    @GetMapping(value = {"/cybersource/secure/sign", "/api/cybersource/secure/sign"}, produces = "application/json")
    public SignatureResponse getCyberSourceJson(@RequestParam String cardAction,
                                                @RequestParam String type,
                                                @RequestParam String captchaToken,
                                                @RequestParam(required = false) String ccToken) throws JsonProcessingException {

        log.info("Getting digital signature with cardAction '{}', type '{}', captchaToken '{}', ccToken '{}'", cardAction, type, captchaToken, ccToken);

        if (captchaToken == null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  "Captcha token is required");

        //Call google recaptcha validator and verify captcha token
        ReCaptchaRequestPayload reCaptchaRequestPayload = ReCaptchaRequestPayload.builder().secret(reCaptchaClientConfig.getSecretKey()).response(captchaToken).build();
        ReCaptchaResponsePayload recaptchaResponsePayload = reCaptchaClient.validateRecaptcha(reCaptchaRequestPayload);
        if (!recaptchaResponsePayload.isSuccess())
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,  "Captcha token invalid");

        return getSignatureResponse(cardAction, type, ccToken);
    }

    /**
     * All logic is taken from AEM with various improvements
     * @return a BASE64 encoded string of a hmac and its plaintext
     * @throws JsonProcessingException if signature contains malformed structure
     */
    @GetMapping(value = {"/cybersource/sign", "/api/cybersource/sign"}, produces = "application/json")
    public SignatureResponse getCyberSourceJson(@RequestParam String cardAction,
                                                @RequestParam String type,
                                                @RequestParam(required = false) String token) throws JsonProcessingException {

        log.info("Getting digital signature with cardAction '{}', type '{}'", cardAction, type);

        return getSignatureResponse(cardAction, type, token);
    }

    private SignatureResponse getSignatureResponse(String cardAction, String type, String ccToken) throws JsonProcessingException {
        Signature hmac = signatureService.sign(cardAction, type, ccToken);
        if (hmac == null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, cardAction + " is not a valid card action");

        String hmacJson = JSON_MAPPER.writeValueAsString(hmac);

        // Java 8 Basic Encoder does not add any line feed i.e \r\n
        String encoded = Base64.getEncoder().encodeToString(hmacJson.getBytes());
        return new SignatureResponse(encoded);
    }

    /**
     * Get signature for a new card authorization
     * @return a BASE64 encoded string of a hmac and its plaintext
     * @throws JsonProcessingException if signature contains malformed structure
     */
    @GetMapping(value = {"/api/sign/add"}, produces = "application/json")
    public SignatureResponse getAddCardSignature() throws JsonProcessingException {
        
        Signature hmac = signatureService.signAddCardAuthorization();
        if (hmac == null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "There was a problem processing your request.");
        String hmacJson = JSON_MAPPER.writeValueAsString(hmac);

        // Java 8 Basic Encoder does not add any line feed i.e \r\n
        String encoded = Base64.getEncoder().encodeToString(hmacJson.getBytes());
        return new SignatureResponse(encoded);
    }

    /**
     * Get signature for a editing an authorized card
     * @return a BASE64 encoded string of a hmac and its plaintext
     * @throws JsonProcessingException if signature contains malformed structure
     */
    @GetMapping(value = {"/api/sign/edit"}, produces = "application/json")
    public SignatureResponse getEditCardSignature(@RequestParam(required = true) String token) throws JsonProcessingException {
        
        Signature hmac = signatureService.signEditCardAuthorization(token);
        if (hmac == null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "There was a problem processing your request.");
        String hmacJson = JSON_MAPPER.writeValueAsString(hmac);

        // Java 8 Basic Encoder does not add any line feed i.e \r\n
        String encoded = Base64.getEncoder().encodeToString(hmacJson.getBytes());
        return new SignatureResponse(encoded);
    }
    
    /**
     * Get signature for a ewallet transaction with a new card
     * @return a BASE64 encoded string of a hmac and its plaintext
     * @throws JsonProcessingException if signature contains malformed structure
     */
    @GetMapping(value = {"/api/sign/ewallet"}, produces = "application/json")
    public SignatureResponse getEwalletTransactionSignature(@RequestParam(required = false) String token) throws JsonProcessingException {
        
        Signature hmac = signatureService.signEwalletTransaction(token);
        if (hmac == null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "There was a problem processing your request.");
        String hmacJson = JSON_MAPPER.writeValueAsString(hmac);

        // Java 8 Basic Encoder does not add any line feed i.e \r\n
        String encoded = Base64.getEncoder().encodeToString(hmacJson.getBytes());
        return new SignatureResponse(encoded);
    }

    /**
     * Get signature for a updating the card associated with a subscription
     * @return a BASE64 encoded string of a hmac and its plaintext
     * @throws JsonProcessingException if signature contains malformed structure
     */
    @GetMapping(value = {"/api/sign/subscription"}, produces = "application/json")
    public SignatureResponse getSubscriptionTransactionSignature(@RequestParam(required = true) String token) throws JsonProcessingException {
        
        Signature hmac = signatureService.signUpdateSubscriptionAuthorization(token);
        if (hmac == null) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "There was a problem processing your request.");
        String hmacJson = JSON_MAPPER.writeValueAsString(hmac);

        // Java 8 Basic Encoder does not add any line feed i.e \r\n
        String encoded = Base64.getEncoder().encodeToString(hmacJson.getBytes());
        return new SignatureResponse(encoded);
    }

    /**
     * All logic is taken from AEM except for responses (Existing AEM returns HTML with embedded Javascript)
     *
     * @param formData application/x-www-form-urlencoded
     * @return see {@link HtmlService#getHtml(int, HtmlService.Type)}
     */
    @PostMapping(path = "/bin/myschl/cspostback", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.TEXT_HTML_VALUE)
    public ResponseEntity<String> handleCyberSourceData(@RequestParam Map<String, String> formData) {
        CyberSourceDto cyberSourceDto = convertToCyberSourceDto(formData);

        String decision = cyberSourceDto.getDecision();
        String cardAction = cyberSourceDto.getCardAction();
        String otcParam = cyberSourceDto.getOtcParam();
        String ewallet = cyberSourceDto.getEwallet();
        String userId = cyberSourceDto.getUserId();

        log.info("Adding CyberSource data: [action:{}, decision:{}, otc:{}, ewallet:{}, userId:{}]",
                cardAction, decision, otcParam, ewallet, userId);

        if ("Accept".equalsIgnoreCase(decision)) {
            if (cardAction != null && StringUtils.isEmpty(otcParam)) { /* Not a subscription */

                if ("EWALLET".equalsIgnoreCase(ewallet)) {
                    validateSignature(formData, SignatureUtil.SecretKeyType.EWALLET);
                    // Add EWallet
                    Wallet wallet = creditCardService.addCreditCardForEWallet(cyberSourceDto, cardAction);
                    return (wallet != null)
                        ? ResponseEntity.ok().body(htmlService.getHtml(200, cyberSourceDto, HtmlService.Type.EWALLET))
                        : ResponseEntity.status(500).body(htmlService.getHtml(500, HtmlService.Type.EWALLET));
                } else {
                    validateSignature(formData, SignatureUtil.SecretKeyType.DEFAULT);
                    // Add or Edit credit card
                    Wallet wallet = creditCardService.addOrEditCreditCard(cyberSourceDto);
                    return (wallet != null)
                        ? ResponseEntity.ok().body(htmlService.getHtml(200, HtmlService.Type.DEFAULT))
                        : ResponseEntity.status(500).body(htmlService.getHtml(500, HtmlService.Type.DEFAULT));
                }

            } else {
                if ("OTC".equalsIgnoreCase(otcParam) && !StringUtils.isEmpty(userId)) {
                    validateSignature(formData, SignatureUtil.SecretKeyType.UPDATEBILLING);
                    UpdateBillingResponse resp = creditCardService.updateBilling(cyberSourceDto);
                    // Not sure if the person who wrote the service knows that JSON can have boolean properties..
                    if ("true".equalsIgnoreCase(resp.getIsSuccess())) {
                        return ResponseEntity.ok().body(htmlService.getHtml(200, HtmlService.Type.SUBSCRIPTION));
                    } else {
                        return ResponseEntity.status(500).body(htmlService.getHtml(500, HtmlService.Type.SUBSCRIPTION));
                    }
                }
            }
        }

        if (StringUtils.isEmpty(otcParam) && StringUtils.isEmpty(ewallet)) {
            return ResponseEntity.badRequest().body(htmlService.getHtml(400, HtmlService.Type.DEFAULT));
        } else if (!StringUtils.isEmpty(ewallet)) {
            return ResponseEntity.badRequest().body(htmlService.getHtml(400, HtmlService.Type.EWALLET));
        } else {
            return ResponseEntity.badRequest().body(htmlService.getHtml(400, HtmlService.Type.SUBSCRIPTION));
        }
    }

    private void validateSignature(Map<String, String> formData, SignatureUtil.SecretKeyType skt) {
        if (!signatureService.isSignatureValid(formData, skt)) {
            log.error("Signatures validation failed!");
            // Not sure what status code should be. Enlighten pls.
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Signatures don't match!");
        };
    }

    private CyberSourceDto convertToCyberSourceDto(Map<String, String> formData) {
        return CyberSourceDto.builder()
            .decision(formData.getOrDefault("decision", EMPTY))
            .cardType(formData.getOrDefault("req_card_type", EMPTY))
            .editPaymentToken(formData.getOrDefault("req_payment_token", EMPTY))
            .paymentToken(formData.getOrDefault("payment_token", EMPTY))
            .cardNumber(formData.getOrDefault("req_card_number", EMPTY))
            .expiryDate(formData.getOrDefault("req_card_expiry_date", EMPTY))
            .userId(formData.getOrDefault("req_merchant_defined_data12", EMPTY))
            .subId(formData.getOrDefault("req_merchant_defined_data19", EMPTY))
            .otcParam(formData.getOrDefault("req_merchant_defined_data20", EMPTY))
            .defaultCard(formData.getOrDefault("req_merchant_defined_data21", EMPTY))
            .saveToProfile(formData.getOrDefault("req_merchant_defined_data23", EMPTY))
            .cardAction(formData.getOrDefault("req_merchant_defined_data25", EMPTY))
            .ext(formData.getOrDefault("req_merchant_defined_data27", EMPTY))
            .ewallet(formData.getOrDefault("req_merchant_defined_data28", EMPTY))
            .firstName(formData.getOrDefault("req_bill_to_forename", EMPTY))
            .lastName(formData.getOrDefault("req_bill_to_surname", EMPTY))
            .address1(formData.getOrDefault("req_bill_to_address_line1", EMPTY))
            .address2(formData.getOrDefault("req_bill_to_address_line2", EMPTY))
            .city(formData.getOrDefault("req_bill_to_address_city", EMPTY))
            .postalCode(formData.getOrDefault("req_bill_to_address_postal_code", EMPTY))
            .state(formData.getOrDefault("req_bill_to_address_state", EMPTY))
            .country(formData.getOrDefault("req_bill_to_address_country", EMPTY))
            .phone(formData.getOrDefault("req_bill_to_phone", EMPTY))
            .build();
    }

    @Data
    @AllArgsConstructor
    private class SignatureResponse {
        private String key;
    }
}
