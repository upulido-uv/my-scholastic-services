package com.myscholastic.controllers.orderhistory;

import com.myscholastic.models.orderhistory.GenericOrder;
import com.myscholastic.models.orderhistory.orderdetails.OrderDetail;
import com.myscholastic.services.orderhistory.OrderHistoryService;
import com.myscholastic.utils.CookieUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping({"/order-history", "/api/orders"})
public class OrderHistoryController {

    @Autowired
    private OrderHistoryService orderHistoryService;

    @ApiOperation("Get order history per user")
    @GetMapping(path = "/history", produces = {"application/json"})
    public GenericOrder getOrderHistory(HttpServletRequest request,
                                        @RequestParam(defaultValue = "1950-01-01") String fromDate,
                                        @RequestParam String toDate) {
        String userId = CookieUtil.getSpsIdFromCookie(request);
        log.info("Get Order History. userId: {}", userId);
        return orderHistoryService.getOrderHistory(userId, fromDate, toDate);
    }

    @ApiOperation("Get order detail per query params")
    @GetMapping(path = "/detail", produces = {"application/json"})
    public OrderDetail getOrderDetail(HttpServletRequest request,
                                      @RequestParam String storeId,
                                      @RequestParam(required = false) String eventsNo,
                                      @RequestParam(required = false) String eventSubmittedYear,
                                      @RequestParam(required = false) String ucn,
                                      @RequestParam(required = false) String bcoe9) {
        String userId = CookieUtil.getSpsIdFromCookie(request);
        log.info("Get Order History Details. userId: {}", userId);
        return orderHistoryService.getOrderDetails(userId, storeId, eventsNo, eventSubmittedYear, ucn, bcoe9);
    }
}
