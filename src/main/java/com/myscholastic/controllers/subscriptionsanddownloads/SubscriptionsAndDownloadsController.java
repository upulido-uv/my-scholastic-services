package com.myscholastic.controllers.subscriptionsanddownloads;

import com.myscholastic.feignclients.digitaldownloads.DigitalDownloadsClient;
import com.myscholastic.feignclients.subscription.QtcUpdateBillingClient;
import com.myscholastic.feignclients.subscription.SubscriptionHistoryClient;
import com.myscholastic.feignclients.subscription.QtcSubscriptionHistoryClient;
import com.myscholastic.models.BaseResponse;
import com.myscholastic.models.digitaldownloads.DigitalDownloads;
import com.myscholastic.models.subscription.QtcSubscriptions;
import com.myscholastic.models.subscription.QtcUpdateBillingRequest;
import com.myscholastic.models.subscription.Subscriptions;
import com.myscholastic.models.subscription.orderdetail.OrderDetails;
import com.myscholastic.services.myprofile.UserService;
import com.myscholastic.utils.CookieUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping({"/subscriptions-and-downloads", "/api"})
public class SubscriptionsAndDownloadsController {

    @Autowired
    private DigitalDownloadsClient downloadsClient;
    @Autowired
    private SubscriptionHistoryClient subscriptionClient;
    @Autowired
    private QtcSubscriptionHistoryClient qtcSubscriptionHistoryClient;
    @Autowired
    private QtcUpdateBillingClient qtcUpdateBillingClient;
    @Autowired
    private UserService userService;

    private static final String UNAUTHORIZED_MESSAGE = "User is not authorized.";

    @ApiOperation(value = "Get subscription history per user using new SOAR api")
    @GetMapping(value = {"/qtc-subscription-history"}, produces = "application/json")
    public QtcSubscriptions getQtcSubscriptions(HttpServletRequest request) {
        // get cookies from request
        String spsCookies = CookieUtil.getSpsRelatedCookies(request);
        if (spsCookies == null) throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, UNAUTHORIZED_MESSAGE);

        // get customer ucn from user id from cookie
        String userId = CookieUtil.getSpsIdFromCookie(request);
        log.info("Getting customerUCN for userId: {}", userId);
        String customerUCN = userService.getCustomerUCN(userId);
        log.info("customer UCN is " + customerUCN);

        // get qtc subscriptions
        return qtcSubscriptionHistoryClient.getQtcSubscriptions(customerUCN);
    }

    @ApiOperation(value = "Sends an update billing request using the new SOAR api")
    @PostMapping(value = {"/qtc-update-billing"}, produces = "application/json")
    public BaseResponse updateQtcBilling(HttpServletRequest request, @RequestBody QtcUpdateBillingRequest requestBody) {
        // get cookies from request
        String spsCookies = CookieUtil.getSpsRelatedCookies(request);
        if (spsCookies == null) throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, UNAUTHORIZED_MESSAGE);

        String userId = CookieUtil.getSpsIdFromCookie(request);
        log.info("Update billing for: {}", userId);
        log.info("Getting customerUCN for userId: {}", userId);
        String customerUCN = userService.getCustomerUCN(userId);
        log.info("customer UCN is " + customerUCN);

        // no need to null check because QTC handles missing ucn on their end
        requestBody.setUcn(customerUCN);

        // pass payload to feign client
        return qtcUpdateBillingClient.updateBilling(requestBody);
    }

    @ApiOperation(value = "Get users digital download details", response = DigitalDownloads.class)
    @GetMapping(path = {"/digital-downloads", "/downloads"}, produces = "application/json")
    public DigitalDownloads getDigitalDownloads(HttpServletRequest request) {
        String spsCookies = CookieUtil.getSpsRelatedCookies(request);
        if (StringUtils.isEmpty(spsCookies)) throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, UNAUTHORIZED_MESSAGE);
        log.info("Get digital downloads. userId: {}", CookieUtil.getSpsIdFromCookie(request));
        return downloadsClient.getDigitalDownloadsList(spsCookies);
    }

    @ApiOperation(value = "Get subscription-history per user")
    @GetMapping(value = {"/subscription-history", "/subscriptions"}, produces = "application/json")
    public Subscriptions getSubscriptions(HttpServletRequest request) {
        String spsCookies = CookieUtil.getSpsRelatedCookies(request);
        if (spsCookies == null) throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, UNAUTHORIZED_MESSAGE);
        log.info("Get subscription history. userId: {}", CookieUtil.getSpsIdFromCookie(request));
        return subscriptionClient.getSubscriptions(spsCookies);
    }

    @ApiOperation(value = "Get subscription order detail by Subscription ID and Order ID")
    @GetMapping(value = {"/subscription-order-detail/{subId}/{orderId}", "/subscriptions/{subId}/{orderId}"}, produces = "application/json")
    public OrderDetails getOrderDetails(@PathVariable String subId, @PathVariable String orderId, HttpServletRequest request) {
        String spsCookies = CookieUtil.getSpsRelatedCookies(request);
        if (spsCookies == null) throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, UNAUTHORIZED_MESSAGE);
        log.info("Get subscription detail. userId: {}, subId: {}, orderId: {}", CookieUtil.getSpsIdFromCookie(request), subId, orderId);
        return subscriptionClient.getSubscriptionByOrderId(subId, orderId, spsCookies);
    }

}