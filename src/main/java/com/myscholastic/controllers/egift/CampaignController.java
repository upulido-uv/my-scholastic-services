package com.myscholastic.controllers.egift;

import com.myscholastic.feignclients.egift.EGiftClient;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.egift.*;
import com.myscholastic.utils.CookieUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.function.Function;

@RestController
@Slf4j
@RequestMapping("/api")
@RequiredArgsConstructor
public class CampaignController {

    private static Function<String, String> extractSpsId = spsUd -> spsUd.split("\\|")[0]; // Not pretty

    private final EGiftClient eGiftClient;
    private final IamClient iamClient;

    @GetMapping("/campaigns")
    public List<Campaign> getCampaignsByUser(HttpServletRequest request) {
        String spsId = CookieUtil.getSpsIdFromCookie(request);
        log.info("Getting campaigns (spsId={})", spsId);
        return eGiftClient.getCampaigns(spsId);
    }

    @GetMapping("/campaigns/{campaignId}")
    public Campaign getCampaignById(HttpServletRequest request,
                                    @PathVariable String campaignId) {
        log.info("Getting campaign (spsId={}, id={})", CookieUtil.getSpsIdFromCookie(request), campaignId);
        return eGiftClient.getCampaignById(campaignId);
    }

    @PostMapping("/campaigns")
    @ResponseStatus(HttpStatus.CREATED)
    public Campaign createCampaign(HttpServletRequest request,
                            @RequestBody CampaignRequest campaignRequest) {
        String spsId = CookieUtil.getSpsIdFromCookie(request);
        log.info("Creating campaign (spsId={})", spsId);
        val ucn = iamClient.getUserWithSections(spsId, "identifiers").getIdentifiers().getUcn();
        campaignRequest.setUcn(ucn);
        campaignRequest.setSpsid(spsId);
        return eGiftClient.postCampaign(campaignRequest);
    }

    @RequestMapping(method = {RequestMethod.PATCH, RequestMethod.PUT}, value = "/campaigns/{campaignId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCampaign(@PathVariable String campaignId, @RequestBody CampaignRequest campaignRequest) {
        log.info("Updating campaign (id={})", campaignId);
        eGiftClient.putCampaign(campaignId, campaignRequest);
    }

    @GetMapping("/campaigns/{campaignId}/contributions")
    public ContributionsResponse getContributionsByCampaign(HttpServletRequest request,
                                                            @PathVariable String campaignId) {
        log.info("Getting contributions (spsId={}, campaignId={})", CookieUtil.getSpsIdFromCookie(request), campaignId);
        return eGiftClient.getContributionsByCampaign(campaignId);
    }

    @GetMapping("/campaigns/{campaignId}/contributions/{contributionId}")
    public Contribution getContribution(HttpServletRequest request,
                                        @PathVariable String campaignId, @PathVariable String contributionId) {
        log.info("Getting contribution (spsId={}, campaignId={}, id={})", CookieUtil.getSpsIdFromCookie(request), campaignId, contributionId);
        return eGiftClient.getContribution(campaignId, contributionId);
    }

    @PostMapping("/campaigns/{campaignId}/contributions")
    @ResponseStatus(HttpStatus.CREATED)
    public ContributionResponse createContribution(HttpServletRequest request,
                                                   @PathVariable String campaignId,
                                                   @RequestBody ContributionRequest contributionReq) {
        log.info("Creating contribution (spsId={}, campaignId={})", CookieUtil.getSpsIdFromCookie(request), campaignId);
        return eGiftClient.createContribution(campaignId, contributionReq);
    }

    @RequestMapping(method = {RequestMethod.PATCH, RequestMethod.PUT}, value = "/campaigns/{campaignId}/contributions/{contributionId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Contribution updateContribution(HttpServletRequest request,
                                           @PathVariable String campaignId, @PathVariable String contributionId,
                                           @RequestBody ContributionRequest contributionRequest) {
        log.info("Updating contribution (spsId={}, campaignId={}, id={})", CookieUtil.getSpsIdFromCookie(request), campaignId, contributionId);
        return eGiftClient.updateContribution(campaignId, contributionId, contributionRequest);
    }

}
