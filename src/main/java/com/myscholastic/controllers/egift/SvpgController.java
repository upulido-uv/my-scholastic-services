package com.myscholastic.controllers.egift;

import com.myscholastic.feignclients.egift.EGiftClient;
import com.myscholastic.feignclients.egift.SVPGClient;
import com.myscholastic.models.egift.*;
import com.myscholastic.utils.CookieUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.function.Function;

@RestController
@Slf4j
@RequestMapping("/api/svpg")
public class SvpgController {

    private static Function<String, String> extractSpsId = spsUd -> spsUd.split("\\|")[0]; // Not pretty

    private final SVPGClient svpgClient;
    public SvpgController(SVPGClient svpgClient) {
        this.svpgClient = svpgClient;
    }

    @GetMapping("/transactions-history")
    public List<Transactions> getTransactionHistory(HttpServletRequest request, TransactionsRequest params) {
        String spsId = CookieUtil.getSpsIdFromCookie(request);
        log.info("Getting Transaction History for (spsId={})", spsId);
        return svpgClient.getTransactionsHistory(spsId, params);
    }

    @GetMapping("/balances")
    public List<BalancesResponse> getBalances(HttpServletRequest request) {
        String spsId = CookieUtil.getSpsIdFromCookie(request);
        log.info("Getting Balances for (spsId={})", spsId);
        return svpgClient.getBalances(spsId);
    }
}
