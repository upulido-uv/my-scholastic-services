package com.myscholastic.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MissingSecretKeyException extends RuntimeException {
    public MissingSecretKeyException(String err) {
        super(err);
    }
}
