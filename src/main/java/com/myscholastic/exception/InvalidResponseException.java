package com.myscholastic.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class InvalidResponseException extends Exception {

    public InvalidResponseException(String message, Throwable cause) {
        super(message, cause);
    }
}
