package com.myscholastic.exception;

import feign.FeignException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

/**
 *  Centralize exception handling instead of duplicating them everywhere, especially for ${@link FeignException}
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * TODO add CustomErrorDecoder instead of catching FeignException
     * Note there is a bug within OpenFeign.
     * https://github.com/OpenFeign/feign/commit/6b8ed38ef5e8310bc5d93e7b2293d15762312d09#diff-7a6604a8e471f865454cc533dc1fcff3
     * {@link FeignException#contentUTF8()} throws a NPE even though FeignException is not null
     * This will be fixed in the new OpenFeign version 10.2.0
     */
    @ExceptionHandler(FeignException.class)
    public ResponseEntity<ErrorResponse> handleFeignException(FeignException ex, WebRequest webRequest) {

        // Below logic will be removed in new OpenFeign version update.
        String errFromIdam;
        try {
            errFromIdam = ex.contentUTF8();
        } catch (Exception e) {
            errFromIdam = "";
        }

        if(ex.status() == HttpStatus.NOT_FOUND.value()){
            log.error("404 Error Intercepted. ErrorMessage: {}, Status: {}, idamMessage: {}", ex.getMessage(), ex.status(), errFromIdam);
            ErrorResponse errorResponse = new ErrorResponse(
                    ex.status(), ex.toString(), errFromIdam, webRequest.getDescription(false)
            );
            return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .body(errorResponse);
        }

        log.warn("FeignException encountered [cause: {}, message: {}, status: {}]", ex.getMessage(), errFromIdam, ex.status());
        ErrorResponse errorResponse = new ErrorResponse(
                ex.status(), ex.toString(), errFromIdam, webRequest.getDescription(false)
        );
        return ResponseEntity.status(ex.status())
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .body(errorResponse);
    }

    @Data
    @AllArgsConstructor
    private class ErrorResponse {
        private int status;
        private String cause;
        private String message;
        private String context;
    }
}
