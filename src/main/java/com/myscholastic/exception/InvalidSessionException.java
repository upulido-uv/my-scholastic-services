package com.myscholastic.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED)
public class InvalidSessionException extends Exception {

    public InvalidSessionException(String message) {
        super(message);
    }

}
