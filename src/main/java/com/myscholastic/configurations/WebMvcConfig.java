package com.myscholastic.configurations;

import com.myscholastic.controllers.SessionValidationInterceptor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.stream.Stream;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "cors-origins")
@ConditionalOnProperty(name = "com.myscholastic.configurations.WebMvcConfig", matchIfMissing = true, havingValue = "enabled")
public class WebMvcConfig implements WebMvcConfigurer {

    private String[] aemOrigins;

    private String[] sfccOrigins;

    private String[] s3Origins;

    @Value("${api-gateway.sessionTimeout:1800000}")
    private long sessionTimeout;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        String[] allowedMethods = {"GET", "POST", "PUT", "DELETE", "OPTIONS", "HEAD", "PATCH"};
        String[] listOfAllOrigins = Stream.of(aemOrigins, sfccOrigins, s3Origins).flatMap(Stream::of).toArray(String[]::new);

        registry.addMapping("/**").allowedOrigins(listOfAllOrigins).allowedMethods(allowedMethods).allowCredentials(true);
    }

    // `/api/**` would've been way better
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(handlerInterceptor())
            .addPathPatterns("/my-profile/**", "/ewallet", "/ewallet/**", "/subscriptions-and-downloads/**",
                "/my-bookfairs/**", "/cybersource/**", "/bin/myschl/cspostback", "/wishlist/**",
                "/order-history/**", "/register/**", "/api/**"
            )
            .excludePathPatterns("/health", "/api/login");
    }

    @Bean
    public HandlerInterceptor handlerInterceptor() {
        return new SessionValidationInterceptor(sessionTimeout);
    }

}
