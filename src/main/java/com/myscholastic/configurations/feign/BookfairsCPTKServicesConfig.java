package com.myscholastic.configurations.feign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import com.myscholastic.configurations.AppConfig;

import feign.RequestInterceptor;

public class BookfairsCPTKServicesConfig {
	
    @Autowired
    private AppConfig appConfig;
    
    @Bean
    public RequestInterceptor iamRequestInterceptor() {
        return template -> template.header("Authorization", "Bearer " + appConfig.getApiGatewayConfig().getBookfairsCPTKRouteBearerToken());
    }
    
}
