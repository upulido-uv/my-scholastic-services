package com.myscholastic.configurations.feign;

import com.myscholastic.configurations.AppConfig;
import feign.Logger;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

public class BookfairServicesConfig {

    @Autowired
    private AppConfig appConfig;

    @Bean
    public RequestInterceptor iamRequestInterceptor() {
        return template -> template.header("Authorization", "Bearer " + appConfig.getApiGatewayConfig().getBookfairsServiceBearerToken());
    }

    //TODO: Is this needed with the @slf4j annotation/yaml config options?
    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

}
