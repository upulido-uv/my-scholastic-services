package com.myscholastic.configurations.feign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import com.myscholastic.configurations.AppConfig;

import feign.RequestInterceptor;

public class BriteEmailVerifyConfig {

	@Autowired
	private AppConfig appConfig;

	@Bean
	public RequestInterceptor briteEmailVerifyInterceptor() {
		return template -> template.query("apikey", appConfig.getApiGatewayConfig().getBriteVerifyEmailApiKey());
	}
}
