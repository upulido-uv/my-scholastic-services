package com.myscholastic.configurations.feign;

import com.myscholastic.configurations.AppConfig;
import feign.Logger;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

public class IamSpsLookupConfig {

    @Autowired
    private AppConfig appConfig;

    @Bean
    public RequestInterceptor TestKongClientInterceptor() {
        return template -> template.header("Authorization", "Bearer " + appConfig.getApiGatewayConfig().getIamSpsLookupBearerToken());
    }
    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

}
