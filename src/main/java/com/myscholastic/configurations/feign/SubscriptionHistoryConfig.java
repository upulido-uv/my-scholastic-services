package com.myscholastic.configurations.feign;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.myscholastic.configurations.AppConfig;
import feign.Logger;
import feign.RequestInterceptor;
import feign.codec.Decoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

public class SubscriptionHistoryConfig {

    @Autowired
    private AppConfig appConfig;

    @Bean
    public RequestInterceptor TestKongClientInterceptor() {
        return template -> template.header("Authorization", "Bearer " + appConfig.getApiGatewayConfig().getSubHistoryBearerToken());
    }

    @Bean
    public Decoder xmlDecoder() {
        XmlMapper xmlMapper = new XmlMapper();
        HttpMessageConverter jacksonConverter = new MappingJackson2HttpMessageConverter(xmlMapper);
        ObjectFactory<HttpMessageConverters> objectFactory = () -> new HttpMessageConverters(jacksonConverter);
        return new SpringDecoder(objectFactory);
    }

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }
}
