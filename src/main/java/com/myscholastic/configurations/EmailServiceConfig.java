package com.myscholastic.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "mail-service")
public class EmailServiceConfig {
	
	private String eventApiUrl;
	private String eventApiUser;
	private String eventApiPwd;
	private String soapAction;
	private String sessionToken;
	private String eventApiEwalletEvent;

}
