package com.myscholastic.configurations;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "cybersource")
public class CyberSourceConfigProperties {
    private String updateBillingAccessKey;
    private String updateBillingProfileId;
    private String ewalletAccessKey;
    private String ewalletProfileId;
    private String accessKey;
    private String profileId;

    private String updateBillingMerchantData35;
    private String oneTimeMerchantData35;
    private String addMerchantData35;
    private String editMerchantData35;

    private String addSignedFieldNames;
    private String editSignedFieldNames;

    private String oneTimeeditBillingUnSignedFieldNames;
    private String addUnSignedFieldNames;
    private String editBillingUnSignedFieldNames;
    private String editUnSignedFieldNames;

    private String addActionUrl;
    private String editActionUrl;
}
