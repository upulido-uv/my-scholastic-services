package com.myscholastic.configurations;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "api-gateway")
public class ApiGatewayConfig {

    private Long sessionTimeout;
    private String baseUrl;
    private String iamRoute;
    private String iamBearerToken;
    private String iamSecretsManagerRoute;
    private String iamSecretsManagerBearerToken;
    private String iamSpsLookupRoute;
    private String iamSpsLookupBearerToken;
    private String iamSpsOrgRoute;
    private String iamSpsOrgBearerToken;
    private String updateBillingRoute;
    private String updateBillingBearerToken;
    private String subHistoryRoute;
    private String subHistoryBearerToken;
    private String subscriptionHistoryRoute;
    private String subscriptionHistoryBearerToken;
    private String eWalletServicsRoute;
    private String eWalletServicsBearerToken;
    private String bookfairsServiceRoute;
    private String bookfairsServiceBearerToken;
    private String briteVerifyEmailRoute;
    private String briteVerifyEmailApiKey;    
    private String bookfairsCPTKRoute;
    private String bookfairsCPTKRouteBearerToken;
    private String wishlistServiceRoute;
    private String wishlistServiceBearerToken;
    private String orderHistoryServiceRoute;
    private String orderHistoryServiceBearerToken;
    private String egiftServiceRoute;
    private String egiftServiceBearerToken;
    private String svpgServiceRoute;
    private String svpgServiceBearerToken;

}
