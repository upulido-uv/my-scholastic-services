package com.myscholastic.services.spsorg;

import com.myscholastic.exception.SpsOrgHasInvalidLocationTypeException;
import com.myscholastic.exception.SpsOrgMissingFieldException;
import com.myscholastic.feignclients.idam.IamSpsOrgClient;
import com.myscholastic.models.myprofile.request.SpsOrganization;
import com.myscholastic.models.myprofile.response.SpsOrganizationResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.stream.Stream;

@Slf4j
@Service
public class SpsOrgService {

    private final IamSpsOrgClient iamSpsOrgClient;
    public SpsOrgService(IamSpsOrgClient iamSpsOrgClient) {
        this.iamSpsOrgClient = iamSpsOrgClient;
    }

    public SpsOrganizationResponse submitSchool(@NonNull SpsOrganization spsOrganization)
        throws SpsOrgMissingFieldException, SpsOrgHasInvalidLocationTypeException {
        if (hasEmptyRequiredFields(spsOrganization))
            throw new SpsOrgMissingFieldException("All required fields must be present for spsOrg");

        String locationType = mapLocationType(spsOrganization.getLocationType());
        spsOrganization.setReportingSchoolType(mapIfSchoolTypeIsHS(spsOrganization.getReportingSchoolType()));

        return iamSpsOrgClient.submitSchool(locationType, spsOrganization);
    }

    private boolean hasEmptyRequiredFields(SpsOrganization spsOrganization) {
        if (spsOrganization == null) return false;
        return Stream.of(spsOrganization.getSchoolName(), spsOrganization.getAddress1(), spsOrganization.getCity(),
            spsOrganization.getZipcode(), spsOrganization.getReportingSchoolType()).anyMatch(StringUtils::isEmpty);
    }

    // I am so confused.. this field is never valid from spsOrg endpoint
    // No val from ['HOS', 'AP', '0', 'USI/ISI'] works..
    // On idam conflunce it accepts following enum
    // PS - Manual school type
    // HOS - Home school type
    // USI/ISI - International Manual
    // AP - Military
    // 0 - US/domestic
    private String mapIfSchoolTypeIsHS(@NonNull String reportingSchoolType) {
        if ("HS".equals(reportingSchoolType)) {
            return "HOS";
        } else {
            return reportingSchoolType;
        }
    }

    // I see locationType being hardcoded as 'US' as part of request...
    private String mapLocationType(@NonNull String locationType) throws SpsOrgHasInvalidLocationTypeException {
        switch (locationType) {
            case "US":
                return "domestic";
            case "MS":
                return "military";
            case "HS":
                return "home";
            case "NUS":
                return "intl";
            default:
                throw new SpsOrgHasInvalidLocationTypeException(locationType + " isn't a valid location type");
        }
    }
}
