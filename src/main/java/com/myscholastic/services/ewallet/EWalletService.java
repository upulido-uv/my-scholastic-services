package com.myscholastic.services.ewallet;

import java.math.BigDecimal;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ResponseStatusException;

import com.myscholastic.feignclients.bookfair.BookfairsClient;
import com.myscholastic.feignclients.idam.EWalletClient;
import com.myscholastic.models.BaseIamResponseModel;
import com.myscholastic.models.ewallet.EWalletId;
import com.myscholastic.models.ewallet.Fair;
import com.myscholastic.models.ewallet.FairIdList;
import com.myscholastic.models.ewallet.FairVouchers;
import com.myscholastic.models.ewallet.FairVouchersJson;
import com.myscholastic.models.ewallet.FundingSource;
import com.myscholastic.models.ewallet.Voucher;
import com.myscholastic.models.ewallet.VoucherTransaction;
import com.myscholastic.models.ewallet.request.CreateEWalletRequest;
import com.myscholastic.models.ewallet.request.CreateVoucherRequest;
import com.myscholastic.models.ewallet.response.PostVoucherTransactionResponse;
import com.myscholastic.services.mail.MailService;

import feign.FeignException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EWalletService {

    @Autowired
    private MailService mailService;
    
    private final EWalletClient eWalletClient;
    private final BookfairsClient bookfairsClient;
    public EWalletService(EWalletClient eWalletClient, BookfairsClient bookfairsClient) {
        this.eWalletClient = eWalletClient;
        this.bookfairsClient = bookfairsClient;
    }

    private static final DateTimeFormatter ISO_INSTANT_PATTERN = DateTimeFormatter.ISO_INSTANT; /* 2019-03-06T16:52:10Z */
    private static final DateTimeFormatter ISO_LOCAL_DATE_PATTERN = DateTimeFormatter.ISO_LOCAL_DATE;   /* 2019-03-06 */
    private static final String VOUCHER_NAME = "MYSCHOLASTIC";
    private static final String VOUCHER_TYPE = "BOOKFAIR_VOUCHER";
    private static final String VOUCHER_FUND_TYPE = "CC_PREAUTH";
    private static final String VOUCHER_FUND_SOURCE = "CYBERSOURCE";


    // It's a shame that client has to do this
    public boolean hasVoucher(String spsId, String voucherId) {
        List<EWalletId> eWalletIds = eWalletClient.getEWalletBySpsId(spsId);
        List<Voucher> vouchers = eWalletIds.stream()
            .map(eWalletId -> eWalletClient.getVouchersByWalletId(String.valueOf(eWalletId.getId())))
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
        return vouchers.stream().anyMatch(voucher -> voucher.getId().equals(Integer.valueOf(voucherId)));
    }

    public PostVoucherTransactionResponse postVoucherTransaction(String voucherId, String amount, String transactionType) {

        VoucherTransaction voucherTransaction = new VoucherTransaction();
        voucherTransaction.setAmount(new BigDecimal(amount));
        voucherTransaction.setType(transactionType);
        voucherTransaction.setReference("MYSCHOLASTIC_" + UUID.randomUUID());
        voucherTransaction.setTimestamp(ISO_INSTANT_PATTERN.format(Instant.now().truncatedTo(ChronoUnit.SECONDS)));

        return eWalletClient.postVoucherTransaction(voucherId, voucherTransaction);
    }


    // All logic is taken from My Scholastic, with minor data type changes
    public FairVouchersJson getFairVouchersJson(String walletId, Long eWalletCookieId) {
        List<Voucher> vouchers =  eWalletClient.getVouchersByWalletId(walletId);

        // We extract fairId, aka source from each voucher, and insert them into fairIdsMap
        // We organize Vouchers by fairId
        Map<Long, List<Voucher>> fairVoucherMap = new HashMap<>();/* fairId : vouchers (with source same as fairId) */
        if (eWalletCookieId != null) fairVoucherMap.put(eWalletCookieId, new ArrayList<>());

        vouchers.stream()
            .map(this::setDisplayCard)
            .map(this::sortVoucherTransactions)
            .forEach(voucher -> addFairIdFromVoucher(voucher, fairVoucherMap));

        FairIdList fairIdList = FairIdList.builder().ids(fairVoucherMap.keySet()).build();
        List<Fair> fairs = bookfairsClient.getFairsInfo(fairIdList);
        List<FairVouchers> fairVouchersList = fairs.stream()
            .filter(this::isFairActive)
            .map(fair -> convertToFairVouchers(fair, eWalletCookieId, fairVoucherMap))
            .sorted((f1, f2) -> sortFairVouchers(eWalletCookieId, f1, f2))
            .collect(Collectors.toList());

        FairVouchersJson fairVouchersJson = new FairVouchersJson();
        fairVouchersJson.setFairVouchersList(fairVouchersList);
        return fairVouchersJson;
    }

    /** VOUCHER **/

    private Voucher setDisplayCard(Voucher voucher) {
        if (voucher.getFundingSource() == null) return voucher;

        FundingSource fundingSource = voucher.getFundingSource();
        String displayCard = mapCardType(fundingSource.getCardType())
            + "...."
            + fundingSource.getMaskedPan().substring(fundingSource.getMaskedPan().lastIndexOf("x") + 1);
        fundingSource.setDisplayCard(displayCard);
        return voucher;
    }

    private static String mapCardType(String cardType) {
        if (cardType == null) return "";
        switch (cardType) {
            case "001":
                return "Visa";
            case "002":
                return "MasterCard";
            case "003":
                return "AmericanExpress";
            case "004":
                return "Discover";
            default:
                return "";
        }
    }

    private Voucher sortVoucherTransactions(Voucher voucher) {
        List<VoucherTransaction> transactions = voucher.getTransactions();
        transactions.sort((t1, t2) -> {
            String createdOn1 = t1.getCreatedOn();
            String createdOn2 = t2.getCreatedOn();
            if (createdOn1 == null || createdOn2 == null) return 0;
            try {
                Instant i1 = Instant.from(ISO_INSTANT_PATTERN.parse(createdOn1));
                Instant i2 = Instant.from(ISO_INSTANT_PATTERN.parse(createdOn2));
                // Descending order
                return i2.compareTo(i1);
            } catch (DateTimeException e) {
                return 0;
            }
        });
        return voucher;
    }

    private void addFairIdFromVoucher(Voucher voucher, Map<Long, List<Voucher>> fairVoucherMap) {
        Long fairId = Long.valueOf(voucher.getSource());
        fairVoucherMap.putIfAbsent(fairId, new ArrayList<>());
        fairVoucherMap.get(fairId).add(voucher);
    }


    /** Fairs **/

    private boolean isFairActive(Fair fair) {
        return fair.getId() != null && !"CANCELLED".equalsIgnoreCase(fair.getStatus());
    }

    private int sortFairVouchers(Long eWalletCookieId, FairVouchers fairVouchers1, FairVouchers fairVouchers2) {
        // Edge Case: FairVouchers with source equals to cookieId, always come at top
        if (fairVouchers1.getId().equals(eWalletCookieId)) return Integer.MIN_VALUE;
        if (fairVouchers2.getId().equals(eWalletCookieId)) return Integer.MAX_VALUE;

        String sd1 = fairVouchers1.getStartDate();
        String sd2 = fairVouchers2.getStartDate();
        if (sd1 == null || sd2 == null) return 0;
        try {
            Instant i1 = Instant.from(ISO_LOCAL_DATE_PATTERN.parse(sd1));
            Instant i2 = Instant.from(ISO_LOCAL_DATE_PATTERN.parse(sd2));
            // Descending order
            return i2.compareTo(i1);
        } catch (DateTimeException e) {
            return 0;
        }

    }

    private FairVouchers convertToFairVouchers(Fair fair, Long ewalletCookieId, Map<Long, List<Voucher>> fairVoucherMap) {
        return FairVouchers.builder()
            .expandedView(setExpandedViewOfFairVouchers(fair, ewalletCookieId))
            .id(fair.getId())
            .url(fair.getUrl())
            .endDate(fair.getEndDate())
            .startDate(fair.getStartDate())
            .status(fair.getStatus())
            .name(fair.getName())
            .walletFlag(fair.getEWalletFlag())
            .moved17DaysFlag(fair.getMoved17DaysFlag())
            .vouchers(getSortedVouchersByFairId(fair, fairVoucherMap))
            .build();
    }

    private List<Voucher> getSortedVouchersByFairId(Fair fair, Map<Long, List<Voucher>> fairVoucherMap) {
        List<Voucher> vouchers = fairVoucherMap.get(fair.getId());
        if (vouchers != null && !vouchers.isEmpty()) {
            vouchers.sort((v1, v2) -> {
                String firstName1 = v1.getAttributes().getStudentFirstName();
                String firstName2 = v2.getAttributes().getStudentFirstName();
                return firstName1.compareTo(firstName2);
            });
            return vouchers;
        } else {
            return Collections.emptyList();
        }
    }

    private boolean setExpandedViewOfFairVouchers(Fair fair, Long eWalletCookieId) {
        if (eWalletCookieId != null && eWalletCookieId.equals(fair.getId())) return true;
        return eWalletCookieId == null
            && ("COMING SOON".equalsIgnoreCase(fair.getStatus()) || "ACTIVE".equalsIgnoreCase(fair.getStatus()));
    }

    public BaseIamResponseModel createEWalletAndVoucher(CreateVoucherRequest eWalletRequest, String spsId) {

        BaseIamResponseModel iamEWalletResponse = new BaseIamResponseModel();
        BaseIamResponseModel createVoucherResponse = new BaseIamResponseModel();
        Long eWalletId;

        //Legacy Logic Check - Logic existed in old code, including it to ensure behavior doesn't happen
        if (eWalletRequest != null && eWalletRequest.getFundingSource() != null && eWalletRequest.getFundingSource().getMaskedPan() != null
         && !eWalletRequest.getFundingSource().getMaskedPan().matches( "\\d{6}[x,X]{1,6}\\d{1,4}" )) {
            log.error("ERROR! Someone is passing unmasked card data to this service! Aborted Transaction!");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Creating voucher with invalid data.");
        }

        // get fair data from fair id
        Set<Long> fairIds = new HashSet<Long>();
        Long fairId = null;
        if (eWalletRequest != null && eWalletRequest.getAttributes() != null && eWalletRequest.getAttributes().getFairId() != null) {
            fairId = Long.parseLong(eWalletRequest.getAttributes().getFairId());
            fairIds.add(fairId);
        }
        List<Fair> fairs = bookfairsClient.getFairsInfo(new FairIdList(fairIds));

        // if the size of fairs is 0, then we did not find any fairs associated with this fairId. Throw an error
        if (fairs.size() == 0) {
            log.error("Fair not found for fairId: " + fairId + " when trying to create a voucher. Check Request body for `fairId` inside `attributes`.");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Creating voucher for missing fair. Check your request body.");
        }

        // get the fair start and end date windows
        Fair fair = fairs.get(0);
        LocalDate startWindow = LocalDate.parse(fair.getStartDate()).minusWeeks(2);
        LocalDate endWindow = LocalDate.parse(fair.getEndDate());
        LocalDate currentDate = LocalDate.now();

        // check to see if current date outside the active fair window
        if (currentDate.isAfter(endWindow) || currentDate.isBefore(startWindow)) {
            log.error("ERROR! Someone is trying to create an eWallet voucher outside of the ACTIVE/COMING SOON period for fairId: " + fairId);
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Creating voucher outside of active period");
        }

        try {
            List<EWalletId> eWalletIds = eWalletClient.getEWalletBySpsId(spsId);
            //NOTE: IAM only supports 1 walletId per user so this is hardcoded as the first wallet
            eWalletId = eWalletIds.get(0).getId();
        } catch (FeignException e) {
            if(HttpStatus.NOT_FOUND.equals(HttpStatus.resolve(e.status()))){
                log.debug("User does not have a eWallet. Create it!");
                iamEWalletResponse = eWalletClient.createEWalletBySpsId(new CreateEWalletRequest(spsId));
                eWalletId = iamEWalletResponse.getId();
            } else {
                HttpStatus status = HttpStatus.resolve(e.status()) != null ? HttpStatus.resolve(e.status()) : HttpStatus.INTERNAL_SERVER_ERROR;
                throw new ResponseStatusException(status, "IAM Get eWallet call failed unexpectedly!", e);
            }
        }

        if (isValidRequest(eWalletRequest, iamEWalletResponse)){
            eWalletRequest.setName(VOUCHER_NAME);
            eWalletRequest.setType(VOUCHER_TYPE);
            eWalletRequest.setFundType(VOUCHER_FUND_TYPE);
            eWalletRequest.setSource(eWalletRequest.getAttributes().getFairId());
            eWalletRequest.getFundingSource().setFundSource(VOUCHER_FUND_SOURCE);

            createVoucherResponse = eWalletClient.createVoucherByWalletId(eWalletId, eWalletRequest);
        }
        if (!StringUtils.isEmpty(createVoucherResponse.getId())) {
        	mailService.sendVoucherCreateConfirmationMail(eWalletRequest, spsId);
            log.debug("Send email to user of a successful transaction/addition to their vouchers.");
        }

        return createVoucherResponse;
    }

    private boolean isValidRequest(CreateVoucherRequest eWalletRequest, BaseIamResponseModel iamEWalletResponse) {
        return null != eWalletRequest && eWalletRequest.getFundingSource() != null;
    }


}
