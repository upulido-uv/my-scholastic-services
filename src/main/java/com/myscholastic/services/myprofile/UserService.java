package com.myscholastic.services.myprofile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myscholastic.feignclients.idam.EWalletClient;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.ewallet.EWalletId;
import com.myscholastic.models.myprofile.*;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@Slf4j
@Service
public class UserService {

    private static final ObjectMapper jsonMapper = new ObjectMapper();

    private final IamClient iamClient;
    private final EWalletClient eWalletClient;
    public UserService(IamClient iamClient, EWalletClient eWalletClient) {
        this.iamClient = iamClient;
        this.eWalletClient = eWalletClient;
    }

    // AEM returns a javascript callback for JsonP...
    public String getProfileLinks(String userId) throws JsonProcessingException {
        ProfileLinks profileLinks = new ProfileLinks();
        profileLinks.setMyProfile("/my-scholastic/profile/my-profile");
        profileLinks.setOrderHistory("/my-scholastic/profile/order-history");

        boolean isParent = isParent(userId);
        boolean hasEWallet = hasEWallet(userId);

        if (!isParent) {
            profileLinks.setSubscriptionsDownloads("/my-scholastic/profile/subscriptions-downloads");
            profileLinks.setWishList("/my-scholastic/profile/my-wishlist");
            profileLinks.setCampaigns("/my-scholastic/profile/manage-campaigns");
        }
        if (hasEWallet) {
            profileLinks.setEwallet("/my-scholastic/profile/ewallet");
        }

        return jsonMapper.writeValueAsString(profileLinks);
    }

    private boolean hasEWallet(String userId) {
        try {
            List<EWalletId> eWalletList = eWalletClient.getEWalletBySpsId(userId);
            return !eWalletList.isEmpty();
        } catch (FeignException e) {
            // Bad API design
            // They should really return an empty list if user doesn't have any ewallet..
            return false;
        }
    }

    // Weirdest logic
    private boolean isParent(String userId) {
        User user = getUser(userId);
        Educator educator = Optional.ofNullable(user)
            .map(User::getPersonas)
            .map(Personas::getEducator)
            .orElseGet(() -> null);
        return educator == null;

        // Below is the logic from AEM, which always evaluate to 'true'...
        /*
        Parent parent = (personas != null) ? personas.getParent() : null;
        String[] appUserType = Optional.of(user)
            .map(User::getBasicProfile)
            .map(BasicProfile::getAppUserType)
            .orElseGet(() -> null);

        boolean condition1 = (parent != null);
        boolean condition2 = (appUserType != null);
        boolean condition3 = (!condition1 && !condition2);
        return (condition1 || condition2 || condition3);
         */
    }

    public User getUser(String userId) {
        return iamClient.getUser(userId);
    }

    public String getCustomerUCN(String userId) {
        User user = getUser(userId);
        return Optional.ofNullable(user)
                .map(User::getIdentifiers)
                .map(Identifiers::getUcn)
                .orElseThrow(() -> {
                    log.error("CustomerUCN was not found for UserId: " + userId);
                    return new ResponseStatusException(HttpStatus.BAD_REQUEST, "CustomerUCN not found for user");
                });
    }

}
