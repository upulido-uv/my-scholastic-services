package com.myscholastic.services.aws;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagement;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagementClientBuilder;
import com.amazonaws.services.simplesystemsmanagement.model.GetParameterRequest;
import com.amazonaws.services.simplesystemsmanagement.model.GetParameterResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Slf4j
@Component
public class SSMService {

    @Value("${amazon.accessKeyId}")
    private String accessKeyId = "";

    @Value("${amazon.secretAccessKey}")
    private String secretAccessKey = "";

    AWSSimpleSystemsManagement awsSimpleSystemsManagement;

    public SSMService() {
    }

    @PostConstruct
    private void postConstruct() {
        log.info("Making System Management Client");

        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKeyId, secretAccessKey);

        awsSimpleSystemsManagement = AWSSimpleSystemsManagementClientBuilder.standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .build();
    }

    /**
     * Get parameter from SSM, with or without encryption (use IAM role for decryption)
     * Throws {@Link com.amazonaws.services.simplesystemsmanagement.model.ParameterNotFoundException} if not found
     *
     * @param key
     * @param encryption
     * @return value
     */
    public String getParameter(String key, boolean encryption) {
        log.debug("Getting key: {} ", key);

        GetParameterRequest getParameterRequest = new GetParameterRequest().withName(key).withWithDecryption(encryption);
        final GetParameterResult result = awsSimpleSystemsManagement.getParameter(getParameterRequest);

        return result.getParameter().getValue();
    }
}
