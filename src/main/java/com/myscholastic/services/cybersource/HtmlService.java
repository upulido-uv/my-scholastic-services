package com.myscholastic.services.cybersource;

import com.myscholastic.controllers.cybersource.CyberSourceController;
import com.myscholastic.models.cybersource.CyberSourceDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/* This class is very evil */
@Slf4j
@Service
public class HtmlService {

    public enum Type {
        DEFAULT, EWALLET, SUBSCRIPTION
    }

    private static String getInlineJavascript(int status, CyberSourceDto cyberSourceDto, Type type) {
        switch (type) {
            case EWALLET:
                String params = (status == 200)
                    ? String.format("%d, '%s', '%s', '%s'",
                        status, cyberSourceDto.getCardType(), cyberSourceDto.getCardNumber(), cyberSourceDto.getPaymentToken())
                    : String.valueOf(status);
                return "processEwalletRequest(" + params + ")";
            case SUBSCRIPTION:
                return "processSubscriptionRequest(" + status + ")";
            default:
                return "processRequest(" + status + ")";
        }
    }

    public String getHtml(int status, Type type) {
        return this.getHtml(status, null, type);
    }

    // Simply baffled
    public String getHtml(int status, CyberSourceDto cyberSourceDto, Type type) {
        return "<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\">" +
            "<head><script type=\"text/javascript\">function ready(){window.parent." +
            getInlineJavascript(status, cyberSourceDto, type) +
            ";}</script></head>" +
            "<body onload=\"ready();\"></body>" +
            "</html>";
    }
}
