package com.myscholastic.services.cybersource;

import com.myscholastic.configurations.CyberSourceConfigProperties;
import com.myscholastic.exception.MissingSecretKeyException;
import com.myscholastic.exception.SignatureValidationException;
import com.myscholastic.models.cybersource.Signature;
import com.myscholastic.services.aws.SSMService;
import com.myscholastic.utils.SignatureUtil;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class SignatureService {

    private final CyberSourceConfigProperties props;
    private final SignatureUtil signatureUtil;
    private final SSMService ssmService;

    @Value("${spring.profiles.active}")
    private String activeProfile;

    SignatureService(CyberSourceConfigProperties props, SignatureUtil signatureUtil, SSMService ssmService) {
        this.props = props;
        this.signatureUtil = signatureUtil;
        this.ssmService = ssmService;
    }

    private static final List<String> ADD_KEY_LIST = Collections.unmodifiableList(Arrays.asList("access_key",
            "profile_id", "transaction_type", "locale", "payment_method", "currency", "signed_field_names",
            "unsigned_field_names", "transaction_uuid", "reference_number", "signed_date_time"));

    private static final List<String> EDIT_KEY_LIST = Collections
            .unmodifiableList(Arrays.asList("amount", "payment_token"));

    private enum Action {
        ADD, EDIT
    }

    private enum Type {
        ADD_ONE_TIME, EWALLET, UPDATE_BILLING, DEFAULT
    }

    private static final String COMMA = ",";
    private static final int SECRET_KEY = 0;
    private static final int EWALLET_SECRET = 1;
    private static final int UPDATE_BILLING_SECRET = 2;

    /**
     *
     * @param cardAction either "ADD" or "EDIT"
     * @param type       "ADD" --> "ADD_ONE_TIME", "EWALLET", else default "EDIT"
     *                   --> "UPDATE_BILLING", else default
     * @param token      payment token only valid for Action "EDIT"
     * @return {@code null} if action is other than "ADD" or "EDIT"
     */
    public Signature sign(String cardAction, String type, String token) {
        String[] keys = getSecretKeys();
        if (Arrays.stream(keys).anyMatch(Objects::isNull))
            throw new MissingSecretKeyException("All secret keys must be present!");

        if ("ADD".equalsIgnoreCase(cardAction)) {

            if ("ADD_ONE_TIME".equalsIgnoreCase(type)) {
                return getSignature(Action.ADD, Type.ADD_ONE_TIME, keys[UPDATE_BILLING_SECRET],
                        props.getUpdateBillingAccessKey(), props.getUpdateBillingProfileId());
            } else if ("EWALLET".equalsIgnoreCase(type)) {
                return getSignature(Action.ADD, Type.EWALLET, keys[EWALLET_SECRET], props.getEwalletAccessKey(),
                        props.getEwalletProfileId());
            } else {
                return getSignature(Action.ADD, Type.DEFAULT, keys[SECRET_KEY], props.getAccessKey(),
                        props.getProfileId());
            }

        } else if ("EDIT".equalsIgnoreCase(cardAction)) {

            if ("UPDATE_BILLING".equalsIgnoreCase(type)) {
                return getSignature(Action.EDIT, Type.UPDATE_BILLING, keys[UPDATE_BILLING_SECRET], token,
                        props.getUpdateBillingAccessKey(), props.getUpdateBillingProfileId());
            } else {
                return getSignature(Action.EDIT, Type.DEFAULT, keys[SECRET_KEY], token, props.getAccessKey(),
                        props.getProfileId());
            }

        } else {
            return null;
        }

    }

    public Signature signAddCardAuthorization() {
        String key = getSecretKeyByType(SignatureUtil.SecretKeyType.DEFAULT);
        if (key == null)
            throw new MissingSecretKeyException("Default Secret Key Missing");

        return getNewCardSignature(Type.DEFAULT, key, props.getAccessKey(), props.getProfileId());
    }

    public Signature signEditCardAuthorization(String token) {
        String key = getSecretKeyByType(SignatureUtil.SecretKeyType.DEFAULT);
        if (key == null)
            throw new MissingSecretKeyException("Default Secret Key Missing");

        return getEditCardSignature(Type.DEFAULT, key, props.getAccessKey(), props.getProfileId(), token);
    }

    public Signature signEwalletTransaction(String token) {
        String key = getSecretKeyByType(SignatureUtil.SecretKeyType.EWALLET);
        if (key == null) throw new MissingSecretKeyException("Ewallet Secret Key Missing");

        if (token == null)
            return getNewCardSignature(Type.EWALLET, key, props.getEwalletAccessKey(), props.getEwalletProfileId());
        else
            return getExistingCardSignature(Type.EWALLET, key, props.getEwalletAccessKey(), 
                props.getEwalletProfileId(), token);
    }

    public Signature signUpdateSubscriptionAuthorization(String token) {
        String key = getSecretKeyByType(SignatureUtil.SecretKeyType.UPDATEBILLING);
        if (key == null)
            throw new MissingSecretKeyException("Billing Secret Key Missing");

        return getEditCardSignature(Type.UPDATE_BILLING, key, props.getUpdateBillingAccessKey(), props.getUpdateBillingProfileId(), token);
    }

    // Not sure if secret keys change periodically.
    private String[] getSecretKeys() {
        final String profilePath = "/MyScholastic/Cybersource/" + activeProfile + "/";

        final String secretKey = ssmService.getParameter( profilePath + "secretkey", true);
        final String eWalletSecretKey = ssmService.getParameter(profilePath + "ewalletSecretKey", true);
        final String updateBillingSecretKey = ssmService.getParameter(profilePath + "updateBillingSecretKey", true);
        return new String[] { secretKey, eWalletSecretKey, updateBillingSecretKey };
    }

    private Signature getSignature(@NonNull Action action, @NonNull Type type, String secretKey, String accessKey,
            String profileId) {
        return getSignature(action, type, secretKey, null, accessKey, profileId);
    }

    private Signature getSignature(@NonNull Action action, @NonNull Type type, String secretKey, String token,
            String accessKey, String profileId) {
        String referenceNumber = signatureUtil.getReferenceNumber();
        String transactionUUID = signatureUtil.getTransactionUUID();
        String signedDateTime = signatureUtil.getSignedDateTime();
        String transactionType = getTransactionType(action);

        List<String> pairs = (action == Action.ADD)
                ? Stream.of(getKV(ADD_KEY_LIST.get(0), accessKey), getKV(ADD_KEY_LIST.get(1), profileId),
                        getKV(ADD_KEY_LIST.get(2), transactionType), getKV(ADD_KEY_LIST.get(3), "en"),
                        getKV(ADD_KEY_LIST.get(4), "card"), getKV(ADD_KEY_LIST.get(5), "USD"),
                        getKV(ADD_KEY_LIST.get(6), getSignedFieldNames(action, type)),
                        getKV(ADD_KEY_LIST.get(7), getUnsignedFieldNames(action, type)),
                        getKV(ADD_KEY_LIST.get(8), transactionUUID), getKV(ADD_KEY_LIST.get(9), referenceNumber),
                        getKV(ADD_KEY_LIST.get(10), signedDateTime)).collect(Collectors.toList())
                : Stream.of(getKV(ADD_KEY_LIST.get(0), accessKey), getKV(ADD_KEY_LIST.get(1), profileId),
                        getKV(ADD_KEY_LIST.get(2), transactionType), getKV(ADD_KEY_LIST.get(3), "en"),
                        getKV(ADD_KEY_LIST.get(4), "card"), getKV(ADD_KEY_LIST.get(5), "USD"),
                        getKV(EDIT_KEY_LIST.get(0), "0.00"),
                        getKV(ADD_KEY_LIST.get(6), getSignedFieldNames(action, type)),
                        getKV(ADD_KEY_LIST.get(7), getUnsignedFieldNames(action, type)),
                        getKV(ADD_KEY_LIST.get(8), transactionUUID), getKV(ADD_KEY_LIST.get(9), referenceNumber),
                        getKV(EDIT_KEY_LIST.get(1), token), getKV(ADD_KEY_LIST.get(10), signedDateTime))
                        .collect(Collectors.toList());
        String message = String.join(COMMA, pairs);
        String hmac = SignatureUtil.getHmac(message, secretKey);

        return Signature.builder().signature(hmac).accessKey(accessKey).profileId(profileId).currency("USD")
                .locale("en").paymentMethod("card").merchant_defined_data35(getMechantData35(action, type))
                .signed_field_names(getSignedFieldNames(action, type))
                .unsigned_field_names(getUnsignedFieldNames(action, type)).actionUrl(getActionUrl(action))
                .reference_number(referenceNumber).transaction_type(transactionType).transaction_uuid(transactionUUID)
                .country("US").signed_date_time(signedDateTime).amount(getAmount(action))
                .merchant_defined_data24("MyScholastic").build();
    }

    // Create signature for new card authorizations
    private Signature getNewCardSignature(Type type, String secretKey, String accessKey, String profileId) {
        String referenceNumber = signatureUtil.getReferenceNumber();
        String transactionUUID = signatureUtil.getTransactionUUID();
        String signedDateTime = signatureUtil.getSignedDateTime();
        String transactionType = getTransactionType(Action.ADD);

        List<String> pairs = Stream.of(getKV(ADD_KEY_LIST.get(0), accessKey), getKV(ADD_KEY_LIST.get(1), profileId),
                getKV(ADD_KEY_LIST.get(2), transactionType), getKV(ADD_KEY_LIST.get(3), "en"),
                getKV(ADD_KEY_LIST.get(4), "card"), getKV(ADD_KEY_LIST.get(5), "USD"),
                getKV(ADD_KEY_LIST.get(6), getSignedFieldNames(Action.ADD, type)),
                getKV(ADD_KEY_LIST.get(7), getUnsignedFieldNames(Action.ADD, type)),
                getKV(ADD_KEY_LIST.get(8), transactionUUID), getKV(ADD_KEY_LIST.get(9), referenceNumber),
                getKV(ADD_KEY_LIST.get(10), signedDateTime)).collect(Collectors.toList());
        String message = String.join(COMMA, pairs);
        String hmac = SignatureUtil.getHmac(message, secretKey);

        return Signature.builder().signature(hmac).accessKey(accessKey).profileId(profileId).currency("USD")
                .locale("en").paymentMethod("card").merchant_defined_data35(getMechantData35(Action.ADD, type))
                .signed_field_names(getSignedFieldNames(Action.ADD, type))
                .unsigned_field_names(getUnsignedFieldNames(Action.ADD, type)).actionUrl(getActionUrl(Action.ADD))
                .reference_number(referenceNumber).transaction_type(transactionType).transaction_uuid(transactionUUID)
                .country("US").signed_date_time(signedDateTime).amount(getAmount(Action.ADD))
                .merchant_defined_data24("MyScholastic").build();
    }

    private Signature getEditCardSignature(Type type, String secretKey, String accessKey, String profileId,
            String token) {
        String referenceNumber = signatureUtil.getReferenceNumber();
        String transactionUUID = signatureUtil.getTransactionUUID();
        String signedDateTime = signatureUtil.getSignedDateTime();
        String transactionType = getTransactionType(Action.EDIT);

        List<String> pairs = Stream.of(getKV(ADD_KEY_LIST.get(0), accessKey), getKV(ADD_KEY_LIST.get(1), profileId),
                getKV(ADD_KEY_LIST.get(2), transactionType), getKV(ADD_KEY_LIST.get(3), "en"),
                getKV(ADD_KEY_LIST.get(4), "card"), getKV(ADD_KEY_LIST.get(5), "USD"),
                getKV(EDIT_KEY_LIST.get(0), "0.00"), getKV(ADD_KEY_LIST.get(6), getSignedFieldNames(Action.EDIT, type)),
                getKV(ADD_KEY_LIST.get(7), getUnsignedFieldNames(Action.EDIT, type)),
                getKV(ADD_KEY_LIST.get(8), transactionUUID), getKV(ADD_KEY_LIST.get(9), referenceNumber),
                getKV(EDIT_KEY_LIST.get(1), token), getKV(ADD_KEY_LIST.get(10), signedDateTime))
                .collect(Collectors.toList());
        String message = String.join(COMMA, pairs);
        String hmac = SignatureUtil.getHmac(message, secretKey);

        return Signature.builder().signature(hmac).accessKey(accessKey).profileId(profileId).currency("USD")
                .locale("en").paymentMethod("card").merchant_defined_data35(getMechantData35(Action.EDIT, type))
                .signed_field_names(getSignedFieldNames(Action.EDIT, type))
                .unsigned_field_names(getUnsignedFieldNames(Action.EDIT, type)).actionUrl(getActionUrl(Action.EDIT))
                .reference_number(referenceNumber).transaction_type(transactionType).transaction_uuid(transactionUUID)
                .country("US").signed_date_time(signedDateTime).amount(getAmount(Action.EDIT))
                .merchant_defined_data24("MyScholastic").build();
    }

    // Create authorization for existing cards
    // TODO: Add authorization action for new ewallet transactions from user's
    // wallet
    private Signature getExistingCardSignature(Type type, String secretKey, String accessKey, String profileId,
            String token) {
        throw new NotImplementedException("This action is not currently supported.");
    }

    private String getActionUrl(Action action) {
        switch (action) {
        case ADD:
            return props.getAddActionUrl();
        case EDIT:
            return props.getEditActionUrl();
        default:
            return "";
        }
    }

    private String getUnsignedFieldNames(Action action, Type type) {
        switch (type) {
        case ADD_ONE_TIME:
            return props.getOneTimeeditBillingUnSignedFieldNames();
        case EWALLET:
            return props.getAddUnSignedFieldNames();
        case UPDATE_BILLING:
            return props.getEditBillingUnSignedFieldNames();
        default:
            return action == Action.ADD ? props.getAddUnSignedFieldNames() : props.getEditUnSignedFieldNames();
        }
    }

    private String getSignedFieldNames(Action action, Type type) {
        switch (type) {
        case ADD_ONE_TIME:
        case EWALLET:
            return props.getAddSignedFieldNames();
        case UPDATE_BILLING:
            return props.getEditSignedFieldNames();
        default:
            return action == Action.ADD ? props.getAddSignedFieldNames() : props.getEditSignedFieldNames();
        }
    }

    private String getMechantData35(Action action, Type type) {
        switch (type) {
        case ADD_ONE_TIME:
            return props.getOneTimeMerchantData35();
        case EWALLET:
            return props.getAddMerchantData35();
        case UPDATE_BILLING:
            return props.getUpdateBillingMerchantData35();
        default:
            return action == Action.ADD ? props.getAddMerchantData35() : props.getEditMerchantData35();
        }
    }

    private String getAmount(Action action) {
        return action == Action.ADD ? null : "0.00";
    }

    private String getTransactionType(Action action) {
        switch (action) {
        case ADD:
            return "create_payment_token";
        case EDIT:
            return "update_payment_token";
        default:
            return "";
        }
    }

    private String getKV(String key, String val) {
        return key + "=" + val;
    }

    /**
     * Reproduce the signature from CyberSource (with the pre-shared secret key) to
     * determine if any fields had been tampered with.
     * 
     * @param formData submitted by front end
     * @throws SignatureValidationException with error message
     */
    public boolean isSignatureValid(Map<String, String> formData, @NonNull SignatureUtil.SecretKeyType keyType) {
        // Further validation pending..
        Assert.notNull(formData, "Form data must be present!");
        String signedFieldNames = formData.get("signed_field_names");
        String signatureFromCyberSource = formData.get("signature");

        if (signedFieldNames == null)
            throw new SignatureValidationException("'signed_field_names' is missing");
        if (signatureFromCyberSource == null)
            throw new SignatureValidationException("'signature' is missing");

        // [f1,f2,...] ---> [f1=v1,f2=v2,...]
        String message = Arrays.stream(signedFieldNames.split(COMMA))
                .map(val -> String.format("%s=%s", val, formData.getOrDefault(val, "")))
                .collect(Collectors.joining(COMMA));
        String secretKey = getSecretKeyByType(keyType);
        String hmac = SignatureUtil.getHmac(message, secretKey);

        return signatureFromCyberSource.equals(hmac);
    }

    private @NonNull String getSecretKeyByType(@NonNull SignatureUtil.SecretKeyType keyType) {
        String[] keys = getSecretKeys();
        switch (keyType) {
        case EWALLET:
            return keys[EWALLET_SECRET];
        case UPDATEBILLING:
            return keys[UPDATE_BILLING_SECRET];
        default:
            return keys[SECRET_KEY];
        }
    }
}
