package com.myscholastic.services.cybersource;

import com.myscholastic.feignclients.idam.CreditCardClient;
import com.myscholastic.feignclients.subscription.UpdateBillingClient;
import com.myscholastic.models.cybersource.Address;
import com.myscholastic.models.cybersource.Wallet;
import com.myscholastic.models.cybersource.IDMDate;
import com.myscholastic.models.cybersource.CyberSourceDto;
import com.myscholastic.models.subscription.UpdateBillingResponse;
import com.myscholastic.utils.CreditCardUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;

@Slf4j
@Service
public class CreditCardService {

    private final CreditCardClient creditCardClient;
    private final UpdateBillingClient updateBillingClient;
    public CreditCardService(CreditCardClient creditCardClient, UpdateBillingClient updateBillingClient) {
        this.creditCardClient = creditCardClient;
        this.updateBillingClient = updateBillingClient;
    }

    /* Add */
    public Wallet addOrEditCreditCard(CyberSourceDto cyberSourceDto) {
        Wallet reqWallet = setCreditCard(cyberSourceDto);
        return creditCardClient.addCreditCard(cyberSourceDto.getUserId(), reqWallet.getCbsToken(), reqWallet);
    }

    /* EWallet */
    public Wallet addCreditCardForEWallet(CyberSourceDto cyberSourceDto, String cardAction) {
        String saveToProfile = cyberSourceDto.getSaveToProfile();

        if ("Add".equalsIgnoreCase(cardAction)) {
            if ("true".equalsIgnoreCase(saveToProfile)) {
                Wallet reqWallet = setCreditCard(cyberSourceDto);
                return creditCardClient.addCreditCard(cyberSourceDto.getUserId(), reqWallet.getCbsToken(), reqWallet);
            } else if ("false".equalsIgnoreCase(saveToProfile)) {
                return Wallet.builder().build();
            }
        }

        // I think this is a bug from AEM. Depending on the input, it is possible to return a null if none of above conditions
        // is satisfied, yet AEM doesn't handle such condition as it just writes null in the response payload.
        return null;
    }

    /* UpdateBilling */
    public UpdateBillingResponse updateBilling(CyberSourceDto cyberSourceDto) {
        String cookie = "SPS_UD=" + cyberSourceDto.getUserId();
        String subId = cyberSourceDto.getSubId();
        return updateBillingClient.updateBilling(cookie, subId);
    }

    private Wallet setCreditCard(CyberSourceDto cyberSourceDto) {
        return Wallet.builder()
            .cbsToken(getCbsToken(cyberSourceDto))
            .billingAddress(getBillingAddress(cyberSourceDto))
            .source("customer")
            .cardType(CreditCardUtil.mapCardType(cyberSourceDto.getCardType()))
            .maskedCardNumber(cyberSourceDto.getCardNumber())
            .primary(Boolean.valueOf(cyberSourceDto.getDefaultCard()))
            .phoneNumber(cyberSourceDto.getPhone())
            .phoneExtension(cyberSourceDto.getExt())
            .expiration(getIDMDate(cyberSourceDto))
            .build();
    }

    private IDMDate getIDMDate(CyberSourceDto cyberSourceDto) {
        String expirationDate = cyberSourceDto.getExpiryDate();
        if (expirationDate == null) return null;

        String[] arr = expirationDate.split("-");
        if (arr.length != 2) {
            log.warn("ExpirationDate '{}' is not valid", expirationDate);
            return null;
        }

        int month = Integer.parseInt(arr[0]);
        int year = Integer.parseInt(arr[1]);

        return new IDMDate(month, year);
    }

    private Address getBillingAddress(CyberSourceDto cyberSourceDto) {
        return Address.builder()
            .firstName(cyberSourceDto.getFirstName())
            .lastName(cyberSourceDto.getLastName())
            .address1(cyberSourceDto.getAddress1())
            .address2(cyberSourceDto.getAddress2())
            .postalCode(cyberSourceDto.getPostalCode())
            .city(cyberSourceDto.getCity())
            .state(cyberSourceDto.getState())
            .country(cyberSourceDto.getCountry())
            .build();
    }

    private String getCbsToken(CyberSourceDto cyberSourceDto) {
        String action = cyberSourceDto.getCardAction();
        if ("ADD".equalsIgnoreCase(action)) return cyberSourceDto.getPaymentToken(); /* AEM My Scholastic passes an empty string */
        else if ("EDIT".equalsIgnoreCase(action)) return cyberSourceDto.getEditPaymentToken();
        else return null;
    }

}
