package com.myscholastic.services.rotatingsecret;

import com.myscholastic.feignclients.idam.IamSecretsManagerClient;
import com.myscholastic.models.authentication.response.SecretResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Slf4j
@Service
public class RotatingSecretService {

    private IamSecretsManagerClient iamSecretsManagerClient;

    private ReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);
    private Lock writeLock = readWriteLock.writeLock();
    private Lock readLock = readWriteLock.readLock();

    private SecretResponse cachedKeys;

    public RotatingSecretService(IamSecretsManagerClient iamSecretsManagerClient) {
        this.iamSecretsManagerClient = iamSecretsManagerClient;
    }


    public SecretResponse getKeys() {
        SecretResponse readOnlyCopy = null;
        boolean hasKeys = true;

        if (cachedKeys == null) {
            log.debug("No keys are present, updating keys from secrets manager.");
            hasKeys = updateKeys();
        }

        if (hasKeys) {
            try {
                readLock.lock();
                readOnlyCopy = new SecretResponse(cachedKeys);
                log.debug("Reading keys from cache, currentKey: {}", readOnlyCopy.getCurrentKey());
            } finally {
                readLock.unlock();
            }
        }

        return readOnlyCopy;
    }

//    @Scheduled(fixedDelayString = "${secret-key.polling.fixedDelayMs}", initialDelayString = "${secret-key.polling.initialDelayMs}")
    public boolean updateKeys() {
        SecretResponse updatedKeys;
        try {
            updatedKeys = iamSecretsManagerClient.getKeys();
        } catch (Exception e) {
            log.error("Couldn't retrieve updated secret key from iam secrets manager.");
            log.error("Trace: {}", e.getStackTrace());
            return false;
        }

        try {
            writeLock.lock();
            if (updatedKeys != null) {
                this.cachedKeys = updatedKeys;
                log.debug("Updated cache with new keys, currentKey: {}", this.cachedKeys.getCurrentKey());
            }
        } finally {
            writeLock.unlock();
        }
        return true;
    }

}
