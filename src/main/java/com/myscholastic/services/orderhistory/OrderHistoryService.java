package com.myscholastic.services.orderhistory;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.feignclients.orderhistory.OrderHistoryClient;
import com.myscholastic.models.myprofile.*;
import com.myscholastic.models.orderhistory.GenericOrder;
import com.myscholastic.models.orderhistory.OrderHistory;
import com.myscholastic.models.orderhistory.orderdetails.*;
import com.myscholastic.models.orderhistory.orderdetails.xml.*;
import com.myscholastic.models.subscription.Attribute;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

@Slf4j
@Service
public class OrderHistoryService {

    private final IamClient iamClient;
    private final OrderHistoryClient orderHistoryClient;
    public OrderHistoryService(IamClient iamClient, OrderHistoryClient orderHistoryClient) {
        this.iamClient = iamClient;
        this.orderHistoryClient = orderHistoryClient;
    }

    /**
     * <quote>
     * Mapper instances are fully thread-safe provided that ALL configuration of the
     * instance occurs before ANY read or write calls.
     * </quote>
     */
    private static final ObjectMapper xmlMapper = new XmlMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    private static final String THUMBNAIL_ROOT = "https://shop.scholastic.com/content/stores/";
    private static final String READING_CLUB_URL = "https://clubs.scholastic.com/myreadingclubaccount?orderhistory=true";
    private static final String PRODUCT_URL = "https://shop.scholastic.com/content/teachers-ecommerce/en/search-results.html?search=1&filters=&text=";

    public @NonNull GenericOrder getOrderHistory(String userId, String fromDate, String toDate) {
        User user = iamClient.getUser(userId);
        GenericOrder genericOrder = orderHistoryClient.getOrderHistory(userId, toDate, fromDate, getUcn(user), getBcoe9(user));

        if (genericOrder == null || genericOrder.getGenericOrder() == null) return new GenericOrder();
        OrderHistory[] orderHistories = Arrays.stream(genericOrder.getGenericOrder())
            .filter(OrderHistoryService::isValidOrder)
            .toArray(OrderHistory[]::new);
        genericOrder.setGenericOrder(orderHistories);
        return genericOrder;
    }

    public OrderDetail getOrderDetails(String spsId, String storeId,
                                       String eventsNo, String eventSubmittedYear,
                                       String ucn, String bcoe9) {
        String xml = orderHistoryClient.getOrderDetail(spsId, storeId, eventsNo, eventSubmittedYear, ucn, bcoe9);
        OrderDetail orderDetail = deserializeToOrderDetail(xml);
        this.setByStoreId(orderDetail, storeId);

        return orderDetail;
    }

    private static boolean isValidOrder(OrderHistory orderHistory) {
        if (StringUtils.isEmpty(orderHistory.getClickStore())) return false;
        switch (orderHistory.getClickStore().toLowerCase()) {
            case "pcool":
            case "as400pcool":
            case "as400":
                return false;
            default:
                return true;
        }
    }

    private String getBcoe9(User user) {
        return Optional.of(user)
            .map(User::getPersonas)
            .map(Personas::getEducator)
            .map(Educator::getSchool)
            .map(School::getBcoe)
            .map(OrderHistoryService::getBcoe9)
            .orElseGet(() -> null);
    }

    private static String getBcoe9(String bcoe) {
        return bcoe.length() > 9 ? bcoe.substring(0, 9) : bcoe;
    }

    private static String getUcn(User user) {
        return Optional.of(user)
            .map(User::getIdentifiers)
            .map(Identifiers::getUcn)
            .orElseGet(() -> null);
    }

    private OrderDetail deserializeToOrderDetail(String xml) {
        try {
            OrderDetailsXml orderDetailsXml = xmlMapper.readValue(xml, OrderDetailsXml.class);
            return OrderDetail.builder()
                .orderTotals(XmlToJsonHelper.getOrderTotals(orderDetailsXml.getOrderTotals()))
                .shippingInfo(XmlToJsonHelper.getShippingInfo(orderDetailsXml.getShippingInfo()))
                .billingInfo(XmlToJsonHelper.getBillingInfo(orderDetailsXml.getBillingInfo()))
                .orderItem(XmlToJsonHelper.getOrderItems(orderDetailsXml.getOrderItem()))
                .build();
        } catch (IOException e) {
            log.error("Xml couldn't be parsed [{}], cause: [{}]", xml, e.getMessage());
            return new OrderDetail();
        }
    }


    private void setByStoreId(@NonNull OrderDetail orderDetail, @NonNull String storeId) {
        switch (storeId.toLowerCase()) {
            case "pcool":
            case "as400pcool":
            case "as400":
                orderDetail.setPcool(true);
                orderDetail.setImageRootPath(THUMBNAIL_ROOT); // same val across all envs
                orderDetail.setReadingClubUrl(READING_CLUB_URL);
                break;
            case "tso7":
            case "sso7":
            case "dw_tso":
            case "dw_sso":
            case "otc_sso":
            case "otc_tso":
                orderDetail.setIndividualTracking(true);
                orderDetail.setProductUrl(PRODUCT_URL);
                break;
            default:
                orderDetail.setBookFair(true);
                orderDetail.setImageRootPath(THUMBNAIL_ROOT);
                break;
        }
    }

    private static class XmlToJsonHelper {

        private static OrderDetailsTotal getOrderTotals(OrderDetailsTotalXml orderTotalsXml) {
            if (orderTotalsXml == null) return null;
            Attribute[] attributes = orderTotalsXml.getAttributes();
            if (attributes == null) return null;

            OrderDetailsTotal orderDetailsTotal = new OrderDetailsTotal();
            Arrays.stream(attributes).forEach(attr -> {
                String name = attr.getName();
                String value = attr.getValue();

                if (name == null) return;
                switch (name.toLowerCase()) {
                    case "order_id":
                        orderDetailsTotal.setOrderId(value);
                        break;
                    case "order_date":
                        orderDetailsTotal.setOrderDate(value);
                        break;
                    case "order_status":
                        orderDetailsTotal.setOrderStatus(value);
                        break;
                    case "detail_url":
                        orderDetailsTotal.setDetailURL(value);
                        break;
                    case "store_label":
                        orderDetailsTotal.setStoreLabel(value);
                        break;
                    case "item_total":
                        orderDetailsTotal.setItemTotal(value);
                        break;
                    case "discount":
                        orderDetailsTotal.setDiscount(value);
                        break;
                    case "shipping_total":
                        orderDetailsTotal.setShippingTotal(value);
                        break;
                    case "tax_total":
                        orderDetailsTotal.setTaxTotal(value);
                        break;
                    case "grand_total":
                        orderDetailsTotal.setGrandTotal(value);
                        break;
                    default:
                }
            });
            return orderDetailsTotal;
        }

        private static ShippingInfo getShippingInfo(ShippingInfoXml shippingInfoXml) {
            if (shippingInfoXml == null) return null;
            Attribute[] attrs = shippingInfoXml.getAttributes();
            if (attrs == null) return null;

            ShippingInfo shippingInfo = new ShippingInfo();
            Arrays.stream(attrs).forEach(attr -> {
                String name = attr.getName();
                String value = attr.getValue();

                if (name == null) return;
                switch (name.toLowerCase()) {
                    case "name":
                        shippingInfo.setName(value);
                        break;
                    case "address1":
                        shippingInfo.setAddress1(value);
                        break;
                    case "address2":
                        shippingInfo.setAddress2(value);
                        break;
                    case "city":
                        shippingInfo.setCity(value);
                        break;
                    case "state":
                        shippingInfo.setState(value);
                        break;
                    case "zip":
                        shippingInfo.setZip(value);
                        break;
                    case "ship_to_phone":
                        shippingInfo.setShip_to_phone(value);
                        break;
                    case "ship_type":
                        shippingInfo.setShip_type(value);
                        break;
                    default:
                }
            });
            return shippingInfo;
        }

        private static BillingInformation getBillingInfo(BillingInformationXml billingInformationXml) {
            if (billingInformationXml == null) return null;
            Attribute[] attributes = billingInformationXml.getAttributes();
            if (attributes == null) return null;

            BillingInformation billingInfo = new BillingInformation();
            Arrays.stream(attributes).forEach(attr -> {
                String name = attr.getName();
                String value = attr.getValue();

                if (name == null) return;
                switch (name.toLowerCase()) {
                    case "name":
                        billingInfo.setName(value); break;
                    case "address1":
                        billingInfo.setAddress1(value); break;
                    case "address2":
                        billingInfo.setAddress2(value); break;
                    case "city":
                        billingInfo.setCity(value); break;
                    case "state":
                        billingInfo.setState(value); break;
                    case "zip":
                        billingInfo.setZip(value); break;
                    case "bill_to_phone":
                        billingInfo.setBill_to_phone(value); break;
                    case "bill_to_email":
                        billingInfo.setBill_to_email(value); break;
                    case "card_brand":
                        billingInfo.setCard_brand(value); break;
                    case "card_number":
                        billingInfo.setCard_number(value); break;
                    default:
                }
            });

            return billingInfo;
        }

        private static OrderItem[] getOrderItems(OrderItemXml[] orderItemXml) {
            if (orderItemXml == null) return new OrderItem[0]; // Empty array
            return Arrays.stream(orderItemXml).map(XmlToJsonHelper::getOrderItem).toArray(OrderItem[]::new);
        }

        private static OrderItem getOrderItem(OrderItemXml itemXml) {
            if (itemXml == null) return null;
            Attribute[] attrs = itemXml.getAttributes();
            if (attrs == null) return null;

            OrderItem orderItem = new OrderItem();
            Arrays.stream(attrs).forEach(attr -> {
                String name = attr.getName();
                String value = attr.getValue();

                if (name == null || value == null) return;
                switch (name.toLowerCase()) {
                    case "order_item_id":
                        orderItem.setOrder_item_id(value);
                        break;
                    case "title":
                        orderItem.setTitle(value);
                        break;
                    case "nickname":
                        orderItem.setNickname(value);
                        break;
                    case "shipping_amt":
                        orderItem.setShipping_amt(value);
                        break;
                    case "isbn":
                        orderItem.setIsbn(value);
                        break;
                    case "partnum":
                        orderItem.setPartNum(value);
                        break;
                    case "quantity":
                        orderItem.setQuantity(value);
                        break;
                    case "list_price":
                        orderItem.setList_price(value);
                        break;
                    case "tax_dollar_amt":
                        orderItem.setTax_dollar_amt(value);
                        break;
                    case "total_items_price":
                        orderItem.setTotal_items_price(value);
                        break;
                    case "item_adjust":
                        orderItem.setItem_adjust(value);
                        break;
                    case "item_image":
                        orderItem.setItem_image(value);
                        break;
                    case "product_type":
                        orderItem.setProduct_type(value);
                        break;
                    case "tracking_href":
                        orderItem.setTracking_href(value);
                        break;
                    default:
                }
            });

            return orderItem;
        }
    }
}
