package com.myscholastic.services.mail;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;

public class SOAPUtil {

    private SOAPUtil() {

    }

    public static SoapResponse sendPostSoap( String targetUrl, String user, String password, String strSoapAction,
            String body ) throws IOException {
        // Prepare HTTP post
        PostMethod post = new PostMethod( targetUrl );
        // Request content will be retrieved directly
        // from the input stream
        RequestEntity entity = new StringRequestEntity( body, "text/xml", "ISO-8859-1" );
        post.setRequestEntity( entity );
        // consult documentation for your web service
        post.setRequestHeader( "SOAPAction", strSoapAction );

        // Get HTTP client
        HttpClient httpclient = new HttpClient();

        httpclient.getState().setCredentials( AuthScope.ANY, new UsernamePasswordCredentials( user, password ) );

        SoapResponse response = new SoapResponse();

        try {
            int result = httpclient.executeMethod( post );
            response.setResponseCode( result );
            response.setResponseBody( post.getResponseBodyAsString() );
        } finally {
            // Release current connection to the connection pool once you are
            // done
            post.releaseConnection();
        }
        return response;
    }

}