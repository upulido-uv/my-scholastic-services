package com.myscholastic.services.mail;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.springframework.stereotype.Service;

import com.myscholastic.configurations.AppConfig;
import com.myscholastic.feignclients.bookfair.BookfairsClient;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.ewallet.Fair;
import com.myscholastic.models.ewallet.FairIdList;
import com.myscholastic.models.ewallet.request.CreateVoucherRequest;
import com.myscholastic.models.myprofile.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MailService {
	
    private AppConfig appConfig;
    
    private IamClient idamClient;
    
    private BookfairsClient bookfairsClient;
    
    public MailService(AppConfig appConfig, IamClient idamClient, BookfairsClient bookfairsClient) {
        this.appConfig = appConfig;
    	this.idamClient = idamClient;
        this.bookfairsClient = bookfairsClient;
    }
    
    
	public SoapResponse sendVoucherCreateConfirmationMail(CreateVoucherRequest eWalletRequest, String spsId){
		SoapResponse response = null;
		
		User user = idamClient.getUser(spsId);
		
		Long fairId = Long.parseLong(eWalletRequest.getAttributes().getFairId());
		List<Fair> fairs = bookfairsClient.getFairsInfo(new FairIdList(new HashSet<Long>(Arrays.asList(fairId))));
		Fair bookFair = fairs.get(0);
		
		if(user!=null && bookFair!=null) {
			Date startDate =null;
			Date endDate = null;
			
			try {
				startDate = new SimpleDateFormat("yyyy-MM-dd").parse(bookFair.getStartDate());
				endDate = new SimpleDateFormat("yyyy-MM-dd").parse(bookFair.getEndDate());
			} catch (ParseException e) {
				log.error("ParseException - fairId :{}, errorMessage:{}.",bookFair.getId(), e.getMessage());
			}
					
			Map<String, Object> params = new HashMap<String, Object>();
			params.put(ConstantUtils.AMOUNT, new DecimalFormat("#.00").format(eWalletRequest.getAmount()));
			params.put(ConstantUtils.STUDENT_FIRST_NAME, eWalletRequest.getAttributes().getStudentFirstName());
			params.put(ConstantUtils.AUTHORIZATION_DATE, new SimpleDateFormat("MM/dd/yy").format(Calendar.getInstance().getTime()));
			params.put(ConstantUtils.SCHOOL_NAME, bookFair.getName());
			params.put(ConstantUtils.START_DATE, (startDate!=null)?new SimpleDateFormat("MM/dd").format(startDate):"");
			params.put(ConstantUtils.END_DATE, (endDate!=null)?new SimpleDateFormat("MM/dd").format(endDate):"");
			params.put(ConstantUtils.SESSION_TOKEN_KEY, appConfig.getEmailServiceConfig().getSessionToken());
			params.put(ConstantUtils.EVENT_TYPE_KEY, appConfig.getEmailServiceConfig().getEventApiEwalletEvent());
			params.put(ConstantUtils.EMAIL_KEY, user.getBasicProfile().getEmail());
	
			try {
				ACMMessage messageBuilder = MessageFactory.getMessageBuilder(MessageFactory.EWALLET_ORDER_CONFIRMATION);
				String body = messageBuilder.getMessageBody(params);

				response = SOAPUtil.sendPostSoap(appConfig.getEmailServiceConfig().getEventApiUrl(),
						 						 appConfig.getEmailServiceConfig().getEventApiUser(),
						 						 appConfig.getEmailServiceConfig().getEventApiPwd(),
						 						 appConfig.getEmailServiceConfig().getSoapAction(),
						 						 body);
			} catch (TransformerException | ParserConfigurationException | IOException e) {
				log.error("ParseError - spsId:{}, fairId:{}, errorMessage:{}.",spsId, bookFair.getId(), e.getMessage());
			}
		}

		return response;

	}

}
