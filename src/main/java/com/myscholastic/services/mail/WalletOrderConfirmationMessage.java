package com.myscholastic.services.mail;

import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;



/**
 * The Class WalletOrderConfirmationMessage.
 */
public class WalletOrderConfirmationMessage extends ACMMessage {

    /*
     * (non-Javadoc)
     * 
     * @see com.sch.myaccounts.utils.soap.ACMMessage#getCtxElement(org.w3c.dom.
     * Document, java.util.Map)
     */
    @Override
    public Element getCtxElement( Document doc, Map< String, Object > params ) {
        Element ctxElement = doc.createElement( "ctx" );

        Element emailElement = doc.createElement( "email" );
        emailElement.appendChild( doc.createTextNode( ( String ) params.get( "email" ) ) );

        Element itemsElement = doc.createElement( "bookfairitems" );

        if ( params.containsKey( ConstantUtils.AMOUNT ) ) {
            Element element = doc.createElement( ConstantUtils.AMOUNT );
            element.appendChild( doc.createTextNode( ( String ) params.get( ConstantUtils.AMOUNT ) ) );
            itemsElement.appendChild( element );
        }

        if ( params.containsKey( ConstantUtils.STUDENT_FIRST_NAME ) ) {
            Element element = doc.createElement( ConstantUtils.STUDENT_FIRST_NAME );
            element.appendChild( doc.createTextNode( ( String ) params.get( ConstantUtils.STUDENT_FIRST_NAME ) ) );
            itemsElement.appendChild( element );
        }

        if ( params.containsKey( ConstantUtils.AUTHORIZATION_DATE ) ) {
            Element element = doc.createElement( ConstantUtils.AUTHORIZATION_DATE );
            element.appendChild( doc.createTextNode( ( String ) params.get( ConstantUtils.AUTHORIZATION_DATE ) ) );
            itemsElement.appendChild( element );
        }

        if ( params.containsKey( ConstantUtils.SCHOOL_NAME ) ) {
            Element element = doc.createElement( ConstantUtils.SCHOOL_NAME );
            element.appendChild( doc.createTextNode( ( String ) params.get( ConstantUtils.SCHOOL_NAME ) ) );
            itemsElement.appendChild( element );
        }

        if ( params.containsKey( ConstantUtils.END_DATE ) ) {
            Element element = doc.createElement( ConstantUtils.END_DATE );
            element.appendChild( doc.createTextNode( ( String ) params.get( ConstantUtils.END_DATE ) ) );
            itemsElement.appendChild( element );
        }

        if ( params.containsKey( ConstantUtils.START_DATE ) ) {
            Element element = doc.createElement( ConstantUtils.START_DATE );
            element.appendChild( doc.createTextNode( ( String ) params.get( ConstantUtils.START_DATE ) ) );
            itemsElement.appendChild( element );
        }

        if ( params.containsKey( ConstantUtils.EWALLET_LINK ) ) {
            Element element = doc.createElement( ConstantUtils.EWALLET_LINK );
            element.appendChild( doc.createTextNode( ( String ) params.get( ConstantUtils.EWALLET_LINK ) ) );
            itemsElement.appendChild( element );
        }

        ctxElement.appendChild( emailElement );
        ctxElement.appendChild( itemsElement );

        return ctxElement;
    }
}
