package com.myscholastic.services.mail;

/**
 * Factory class to return the required message from the available messages
 *
 */
public class MessageFactory {
    public static final String EWALLET_ORDER_CONFIRMATION = "orderConfirmation";

    private MessageFactory() {

    }

    /**
     * MessageBuilder returns appropriate Message
     * 
     * @param msg
     * @return
     */
    public static ACMMessage getMessageBuilder( String msg ) {
    	if ( EWALLET_ORDER_CONFIRMATION.equals( msg ) ) {
            return new WalletOrderConfirmationMessage();
        } else {
            throw new UnsupportedOperationException();
        }
    }
}
