package com.myscholastic.services.mail;

/**
 * ConstantUtils class
 * 
 * @author nbejjanki
 * 
 */
public final class ConstantUtils {

    public static final String AMOUNT = "amount";
    public static final String ATTRIBUTES = "attributes";
    public static final String STUDENT_FIRST_NAME = "studentFirstName";
    public static final String BOOKFAIR_DETAILS = "bookfairdetails";
    public static final String START_DATE = "startDate";
    public static final String END_DATE = "endDate";
    public static final String AUTHORIZATION_DATE = "authorizationDate";
    public static final String FAIR_ID = "fairId";
    public static final String EWALLET_LINK = "ewalletLink";
    public static final String SCHOOL_NAME = "name";
    public static final String SESSION_TOKEN_KEY = "sessionToken";
    public static final String EVENT_TYPE_KEY = "eventType";
    public static final String EMAIL_KEY = "email";
    
    private ConstantUtils() {}

}
