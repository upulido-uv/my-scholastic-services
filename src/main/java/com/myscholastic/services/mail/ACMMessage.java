package com.myscholastic.services.mail;

import java.io.StringWriter;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public abstract class ACMMessage {
    
    public abstract Element getCtxElement(Document doc, Map<String, Object> params);

    public String getMessageBody(Map<String, Object> params) throws TransformerException, ParserConfigurationException {

        if (params == null){
            throw new IllegalArgumentException("missing message parameters");
        }
        
        if (StringUtils.isEmpty((String)params.get("sessionToken"))) {
            throw new IllegalArgumentException("missing session token to create acm soap message");
        }
        
        if (StringUtils.isEmpty((String)params.get("eventType"))) {
            throw new IllegalArgumentException("missing event type to create acm soap message");
        }
        
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("soapenv:Envelope");
        rootElement.setAttribute("xmlns:soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
        rootElement.setAttribute("xmlns:urn", "urn:nms:rtEvent");

        Element headerElement = doc.createElement("soapenv:Header");
        rootElement.appendChild(headerElement);

        Element bodyElement = doc.createElement("soapenv:Body");
        Element pushEventElement = doc.createElement("urn:PushEvent");

        Element sessionTokenElement = doc.createElement("urn:sessiontoken");
        sessionTokenElement.appendChild(doc.createTextNode((String)params.get("sessionToken")));

        pushEventElement.appendChild(sessionTokenElement);

        Element domEventElement = doc.createElement("urn:domEvent");

        Element rtEventElement = doc.createElement("rtEvent");
        rtEventElement.setAttribute("type", (String)params.get("eventType"));
        rtEventElement.setAttribute("email", StringUtils.defaultIfBlank( (String)params.get("email"), StringUtils.EMPTY));

        rtEventElement.appendChild(getCtxElement(doc, params));

        domEventElement.appendChild(rtEventElement);
        pushEventElement.appendChild(domEventElement);

        bodyElement.appendChild(pushEventElement);

        rootElement.appendChild(bodyElement);

        doc.appendChild(rootElement);

        // write the content into xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);

        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);

        transformer.transform(source, result);

        return writer.getBuffer().toString();
    }

}
