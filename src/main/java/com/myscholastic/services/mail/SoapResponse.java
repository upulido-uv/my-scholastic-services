package com.myscholastic.services.mail;

/**
 * The Class SoapResponse.
 */
public class SoapResponse {

    /** The response code. */
    private int responseCode;

    /** The response body. */
    private String responseBody;

    /**
     * Instantiates a new soap response.
     */
    public SoapResponse() {

    }

    /**
     * Instantiates a new soap response.
     *
     * @param code
     *            the code
     * @param body
     *            the body
     */
    public SoapResponse( int code, String body ) {
        this.responseBody = body;
        this.responseCode = code;
    }

    /**
     * Gets the response code.
     *
     * @return the response code
     */
    public int getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the response code.
     *
     * @param responseCode
     *            the new response code
     */
    public void setResponseCode( int responseCode ) {
        this.responseCode = responseCode;
    }

    /**
     * Gets the response body.
     *
     * @return the response body
     */
    public String getResponseBody() {
        return responseBody;
    }

    /**
     * Sets the response body.
     *
     * @param responseBody
     *            the new response body
     */
    public void setResponseBody( String responseBody ) {
        this.responseBody = responseBody;
    }

}
