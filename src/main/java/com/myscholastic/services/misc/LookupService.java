package com.myscholastic.services.misc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.myscholastic.models.myprofile.response.LookupResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class LookupService {
	
	
	private String[] roleClassSize = new String[] {"pkt=pre_k","kt=kg","fgt=first","sgt=second","tgt=third","fogt=fourth","figt=fifth","sigt=sixth","segt=seventh","egt=eighth","ngt=ninth","tegt=tenth","elgt=eleventh","twgt=twelfth"};
	
	private String[] gradesByOrdinals = new String[] {"pre_k=20","kg=30","first=40","second=50","third=60","fourth=70","fifth=80","sixth=90","seventh=100","eighth=110","ninth=120","tenth=130","eleventh=140","twelfth=150"};
	
	public List<LookupResponse> getRoleClassSize() {
		
		List<LookupResponse>  roleClassSizeList = new ArrayList<LookupResponse>();		
		for(String r :roleClassSize ) {
			String[] pair = r.split("=");
			LookupResponse lookupResponse = new LookupResponse(pair[0],pair[1]);
			roleClassSizeList.add(lookupResponse);
		}
		return roleClassSizeList;
	}

public List<LookupResponse> getGradesByOrdinals() {
	List<LookupResponse>  gradesByOrdinalsList = new ArrayList<LookupResponse>();		
	for(String g :gradesByOrdinals ) {
		String[] pair = g.split("=");
		LookupResponse lookupResponse = new LookupResponse(pair[0],pair[1]);
		gradesByOrdinalsList.add(lookupResponse);
	}
	return gradesByOrdinalsList;
}

}
