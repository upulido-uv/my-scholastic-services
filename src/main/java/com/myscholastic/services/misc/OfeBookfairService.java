package com.myscholastic.services.misc;

import com.google.common.collect.Lists;
import com.myscholastic.feignclients.bookfair.BookfairsCachedClient;
import com.myscholastic.feignclients.idam.IamClient;
import com.myscholastic.models.myprofile.BookfairOrganizationInfo;
import com.myscholastic.models.myprofile.IdamBookfairOrganizations;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class OfeBookfairService {

    private final IamClient idamClient;
    private final BookfairsCachedClient bookfairClient;

    /**
     * @param userId (spsId)
     * @return a list of school information, empty otherwise
     */
    public List<BookfairOrganizationInfo> getBookfairOrgNames(String userId) {
        val idamOrgs = idamClient.getUserBFOrgUcns(userId);
        if (idamOrgs.isEmpty()) {
            log.warn("No school found in idam (userId={})", userId);
            return Collections.emptyList();
        }

        val defaultOrg = getDefaultSchool(idamOrgs);
        val orgIds = idamOrgs.stream().map(IdamBookfairOrganizations::getOrgId).collect(Collectors.toList());
        val bookFairOrgs = getSchoolInfo(orgIds);

        if (defaultOrg != null)
            bookFairOrgs.stream()
                    .filter(org -> isSameOrgId(org, defaultOrg))
                    .findFirst()
                    .ifPresent(bfOrgInfo -> bfOrgInfo.setDefaultOrg(true));

        return bookFairOrgs;
    }

    private boolean isSameOrgId(@NonNull BookfairOrganizationInfo org,
                                @NonNull String defaultOrg) {
        return defaultOrg.equalsIgnoreCase(org.getIdamOrgUcn());
    }

    private String getDefaultSchool(List<IdamBookfairOrganizations> orgs) {
        return orgs.stream()
                .filter(IdamBookfairOrganizations::isDefaultOrg)
                .findFirst()
                .map(IdamBookfairOrganizations::getOrgId)
                .orElseGet(() -> orgs.get(0).getOrgId());
    }

    private List<BookfairOrganizationInfo> getSchoolInfo(List<String> orgIds){
        return Lists.partition(orgIds, 10)
                .stream()
                .map(subOrgIds -> String.join(",", subOrgIds))
                .map(bookfairClient::getBookfairOrganizationInfo)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

}