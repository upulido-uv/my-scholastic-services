package com.myscholastic.services.wishlist;

import com.myscholastic.controllers.wishlist.WishlistController;
import com.myscholastic.feignclients.idam.WishListClient;
import com.myscholastic.models.wishlist.DeleteWishListResponse;
import com.myscholastic.models.wishlist.Item;
import com.myscholastic.models.wishlist.WishList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.DateTimeException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class WishListService {

    private final WishListClient wishListClient;
    public WishListService(WishListClient wishListClient) {
        this.wishListClient = wishListClient;
    }

    // 2018-11-07T07:31:24Z
    private static final DateTimeFormatter ISO_INSTANT_PATTERN = DateTimeFormatter.ISO_INSTANT;

    public List<String> getProductIds(String userId) {
        WishList wishList = wishListClient.getAllWishListItems(userId);
        List<Item> items = wishList.getItems();

        // Order items from latest to oldest
        items.sort((item1, item2) -> {
            String updated1 = item1.getUpdatedOn();
            String updated2 = item2.getUpdatedOn();

            // If either field is null, then item(s) sit at the bottom (higher up in index)
            if (updated1 == null && updated2 == null) return 0;
            if (updated1 == null) return 1;
            if (updated2 == null) return -1;

            try {
                Instant i1 = Instant.from(ISO_INSTANT_PATTERN.parse(updated1));
                Instant i2 = Instant.from(ISO_INSTANT_PATTERN.parse(updated2));
                return i2.compareTo(i1);
            } catch (DateTimeException e) {
                log.warn("'{}' and '{}' couldn't be parsed", updated1, updated2);
                return 0;
            }

        });

        return items.stream().map(Item::getProductId).collect(Collectors.toList());
    }

    public DeleteWishListResponse deleteItemByProductId(String userId, String productId) {
        return wishListClient.deleteWishListItemByProductId(userId, productId);
    }

    public DeleteWishListResponse deleteAllWishList(String userId) {
        return wishListClient.deleteAllWishListItems(userId);
    }
}
